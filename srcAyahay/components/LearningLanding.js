import React, { Component } from "react";
import { StyleSheet, View } from "react-native";

function LearningLanding(props) {
  return <View style={[styles.container, props.style]}></View>;
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "rgba(6,5,138,1)"
  }
});

export default LearningLanding;
