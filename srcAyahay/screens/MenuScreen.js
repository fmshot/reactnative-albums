import React, { Component } from "react";
import { StyleSheet, View, Image, Text } from "react-native";

function MenuScreen(props) {
  return (
    <View style={styles.container}>
      <View style={styles.rectStack}>
        <View style={styles.rect}>
          <Image
            source={require("../assets/images/reactlogo1.png")}
            resizeMode="contain"
            style={styles.image2}
          ></Image>
        </View>
        <Image
          source={require("../assets/images/sidebar-2.jpg")}
          resizeMode="contain"
          style={styles.image}
        ></Image>
        <View style={styles.group2}>
          <View style={styles.rect2}>
            <View style={styles.group}>
              <Text style={styles.logClassData}>LOG CLASS DATA</Text>
            </View>
          </View>
        </View>
        <View style={styles.rect4}>
          <View style={styles.rect5}>
            <Text style={styles.studentData}>STUDENT DATA</Text>
          </View>
        </View>
        <View style={styles.rect6}>
          <View style={styles.rect7}>
            <Text style={styles.curriculum}>CURRICULUM</Text>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  rect: {
    top: 102,
    left: 533,
    width: 214,
    backgroundColor: "rgba(6,5,138,1)",
    position: "absolute",
    bottom: 152
  },
  image2: {
    width: 111,
    height: 112,
    marginTop: 38,
    marginLeft: 67
  },
  image: {
    top: 0,
    left: 0,
    width: 559,
    position: "absolute",
    bottom: 0
  },
  group2: {
    top: 270,
    left: 493,
    width: 276,
    height: 104,
    position: "absolute"
  },
  rect2: {
    width: 276,
    height: 104,
    backgroundColor: "rgba(21,113,204,1)",
    borderRadius: 22,
    borderColor: "#000000",
    borderWidth: 0
  },
  group: {
    width: 183,
    height: 48,
    marginTop: 28,
    marginLeft: 47
  },
  logClassData: {
    color: "rgba(253,250,250,1)",
    fontSize: 24,
    fontFamily: "roboto-regular",
    textAlign: "center"
  },
  rect4: {
    top: 404,
    left: 493,
    width: 276,
    height: 104,
    position: "absolute"
  },
  rect5: {
    width: 276,
    height: 104,
    backgroundColor: "rgba(21,113,204,1)",
    borderRadius: 22,
    borderColor: "#000000",
    borderWidth: 0
  },
  studentData: {
    color: "rgba(253,250,250,1)",
    fontSize: 24,
    fontFamily: "roboto-regular",
    textAlign: "left",
    marginTop: 40,
    marginLeft: 65
  },
  rect6: {
    top: 546,
    left: 493,
    width: 276,
    height: 104,
    position: "absolute"
  },
  rect7: {
    width: 276,
    height: 104,
    backgroundColor: "rgba(21,113,204,1)",
    borderRadius: 22,
    borderColor: "#000000",
    borderWidth: 0
  },
  curriculum: {
    color: "rgba(253,250,250,1)",
    fontSize: 24,
    fontFamily: "roboto-regular",
    marginTop: 40,
    marginLeft: 73
  },
  rectStack: {
    width: 769,
    flex: 1,
    marginBottom: -152,
    marginTop: -102,
    marginLeft: -371
  }
});

export default MenuScreen;
