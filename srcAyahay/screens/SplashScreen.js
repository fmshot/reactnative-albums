import React, { Component } from "react";
import { StyleSheet, View, Image, Text } from "react-native";
import LearningLanding from "../components/LearningLanding";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen'

function SplashScreen(props) {
  return (
    <View style={styles.container}>
      <View style={styles.learningLandingStack}>
        {/* <LearningLanding style={styles.learningLanding}></LearningLanding> */}
        <View style={styles.responsiveLogo}>
          <Image
          source={require("../assets/images/reactlogo.png")}
          // resizeMode="contain"
          style={styles.image}
        ></Image>
        <Text style={styles.loremIpsum}>Learning Cluster{"\n"}Programme</Text>
        <Text style={styles.tutorApp}>Tutor App</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(6,5,138,1)",
    flexDirection: 'row',
    alignItems: 'center'
  },
  learningLanding: {
    top: 0,
    left: 0,
    backgroundColor: "rgba(6,5,138,1)",
    position: "absolute",
    right: 0,
    bottom: 0
  },
  image: {
    width: wp('40%'),
        height: hp('30%'),
        alignSelf: 'center'
    // top: 268,
    // left: 129,
    // width: 105,
    // height: 100,
    // position: "absolute"
  },
  loremIpsum: {
    // top: 381,
    // left: 86,
    color: "rgba(245,241,241,1)",
    // position: "absolute",
    fontSize: hp('3.3%'),
    fontFamily: "roboto-700",
    textAlign: "center"
  },
  tutorApp: {
    // top: 446,
    // left: 143,
    color: "rgba(245,241,241,1)",
    // position: "absolute",
    fontSize: hp('2%'),
    fontFamily: "roboto-regular",
    textAlign: "center"
  },
  learningLandingStack: {
    flex: 1
  },
  responsiveLogo: {
    // width: wp('70%'),
    //     height: hp('30%'),
    //     alignSelf: 'center',
        // resizeMode: 'contain',
  }
});

export default SplashScreen;
