import React, { Component } from "react";
import { StyleSheet, View, SafeAreaView, ScrollView, Text, Image } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen'

function StudentReviewScreen(props) {
  return (
    <View style={styles.container}>
      <ScrollView>
      {/* <View style={styles.rect30Stack}> */}
        {/* <View style={styles.rect30}>
          <View style={styles.rect29}>
            <View style={styles.loremIpsum2Stack}>
              <Text style={styles.loremIpsum2}>
                I noticed Khadija has a keen{"\n"} interest in mathematics. She
                {"\n"}is always punctual and arrives before all students.
              </Text>
              <Text style={styles.text}>
                I noticed Khadija has a keen{"\n"} interest in mathematics. She
                {"\n"}is always punctual and arrives before all students.
              </Text>
            </View>
          </View>
        </View> */}
        {/* <View style={styles.rect31}> */}
          <Image
            source={require("../assets/images/reactlogo2.png")}
            resizeMode="contain"
            style={styles.image}
          ></Image>
          <Text style={styles.khadijabubaMusa}>KHADIJABUBA MUSA</Text>
          <Text style={styles.typeOfReview}>Type Of Review</Text>
          {/* <View style={styles.group2}> */}
            {/* <View style={styles.group3}> */}
              <View style={styles.rect210}>
                <Text style={styles.positive}>Positive</Text>
              </View>
            {/* </View> */}
          {/* </View> */}
          <View style={styles.rect25}>
            <Text style={styles.negative}>Negative</Text>
          </View>
          <View style={styles.rect28}>
            <Text style={styles.neutral}>Neutral</Text>
          </View>
          <Text style={styles.loremIpsum}>Notes About Student Review</Text>
          <View style={styles.rect2102}>
          <ScrollView>
          <Text style={styles.comments}>
     lorem Ipsum,  that is the lorem that was is there to lorem.Lorem Ipsum 2 Stack. lorem Ipsum,  that is the lorem that was is there to lorem.Lorem Ipsum 2 Stack
          . lorem Ipsum,  that is the lorem that was is there to lorem.Lorem Ipsum 2 Stack. lorem Ipsum,  that is the lorem that was is there to lorem.Lorem Ipsum 2 Stack
          lorem Ipsum,  that is the lorem that was is there to lorem.Lorem Ipsum 2 Stack.
          lorem Ipsum,  that is the lorem that was is there to lorem.Lorem Ipsum 2 Stack.      
          </Text>
          </ScrollView>
          </View>
          <View style={styles.rect213}>
            <Text style={styles.submit}>Submit</Text>
          </View>
        {/* </View> */}
      {/* </View> */}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(6,5,138,1)",
  },
  rect31: {
    backgroundColor: "rgba(6,5,138,1)",
    width: wp('100%'),
    height: hp('100%'),
    alignSelf: 'center'

  },
  image: {
    width: wp('40%'),
    height: hp('30%'),
    alignSelf: 'center'
  },
  khadijabubaMusa: {
    color: "rgba(255,255,255,1)",
    fontSize: hp('3.6%'),
    fontFamily: "roboto-700",
    textAlign: 'center',
    marginBottom: hp('5.5%'),
  
  },
  typeOfReview: {
    color: "rgba(255,255,255,1)",
    fontSize: hp('2.6%'),
    fontFamily: "roboto-regular",
    textAlign: 'center',
    marginBottom: hp('1.5%'),


  },
  // group2: {
  //   width: 272,
  //   height: 45,
  //   marginTop: 23,
  //   marginLeft: 55
  // },
  // group3: {
  //   width: 272,
  //   height: 45
  // },
  rect210: {
    width: wp('70%'),
    height: hp('6%'),
    alignSelf: 'center',
    backgroundColor: "rgba(21,204,201,1)",
    borderRadius: 12,
    borderColor: "#000000",
  },
  positive: {
    color: "rgba(10,10,10,1)",
    // fontSize: 24,
    fontSize:hp('3.3%'),
    fontFamily: "roboto-regular",
    textAlign: "center",
    marginTop: hp('0.5%'),
    // marginLeft: 94
  },
  rect25: {
    backgroundColor: "rgba(204,21,71,1)",
    width: wp('70%'),
    height: hp('6%'),
    alignSelf: 'center',
    borderRadius: 12,
    borderColor: "#000000",
    borderWidth: 0,
    marginTop: hp('1%'),
  },
  negative: {
    color: "rgba(10,10,10,1)",
    fontSize:hp('3.3%'),
    fontFamily: "roboto-regular",
    textAlign: "center",
    marginTop: hp('0.5%'),
  },
  rect28: {
    backgroundColor: "rgba(252,255,255,1)",
    width: wp('70%'),
    height: hp('6%'),
    alignSelf: 'center',
    borderRadius: 12,
    borderColor: "#000000",
    borderWidth: 0,
    marginTop: hp('1%'),
  },
  neutral: {
    color: "rgba(10,10,10,1)",
    fontSize:hp('3.3%'),
    fontFamily: "roboto-regular",
    textAlign: "center",
    marginTop: hp('0.5%'),
  },
  loremIpsum: {
    color: "rgba(247,239,239,1)",
    fontSize:hp('3.3%'),
    fontFamily: "roboto-regular",
    textAlign: "center",
    marginTop: hp('8%'),
  },
  comments: {
    // color: "rgba(247,239,239,1)",
    fontSize:hp('2.3%'),
    // fontFamily: "roboto-regular",
    textAlign: "justify",
    padding:  hp('1%'),
    // marginTop: hp('8%'),
  },
  rect2102: {
    width: wp('70%'),
    height: hp('30%'),
    backgroundColor: "rgba(250,253,253,1)",
    borderRadius: hp('1%'),
    borderColor: "#000000",
    // borderWidth: 0,
    marginTop: hp('2%'),
    alignSelf: 'center'
  },
  rect213: {
    width: wp('70%'),
    height: hp('6%'),
    backgroundColor: "rgba(21,113,204,1)",
    alignSelf: 'center',
    borderRadius: 12,
    borderColor: "#000000",
    borderWidth: 0,
    marginTop: hp('10%'),
  },
  submit: {
    color: "rgba(249,245,245,1)",
    fontSize: hp('2%'),
    fontFamily: "roboto-700",
    textAlign: "center",
    marginTop: hp('1%'),
  },
  loremIpsum2: {
    top: 0,
    left: 0,
    color: "rgba(8,8,8,1)",
    // position: "absolute",
    alignSelf: 'center',
    fontSize:hp('1%'),
    fontFamily: "roboto-regular"
  },
  rect30: {
    top: 0,
    left: 4,
    width: 375,
    height: 812,
    backgroundColor: "rgba(6,5,138,1)",
    position: "absolute"
  },
  rect29: {
    width: 375,
    height: 812,
    backgroundColor: "rgba(6,5,138,1)"
  },
 
  text: {
    top: 0,
    left: 0,
    color: "rgba(8,8,8,1)",
    position: "absolute",
    fontSize: 20,
    fontFamily: "roboto-regular"
  },
  loremIpsum2Stack: {
    width: 324,
    height: 80,
    marginTop: 546,
    marginLeft: 51
  },
 
  // rect30Stack: {
  //   width: 379,
  //   height: 812,
  //   marginLeft: -4
  // }
});

export default StudentReviewScreen;
