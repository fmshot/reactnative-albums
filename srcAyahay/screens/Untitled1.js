import React, { Component } from "react";
import { StyleSheet, View, Image, Text } from "react-native";
import LearningLanding from "../components/LearningLanding";

function Untitled1(props) {
  return (
    <View style={styles.container}>
      <View style={styles.learningLandingStack}>
        <LearningLanding style={styles.learningLanding}></LearningLanding>
        <Image
          source={require("../assets/images/reactlogo.png")}
          resizeMode="contain"
          style={styles.image}
        ></Image>
        <Text style={styles.loremIpsum}>Learning Cluster{"\n"}Programme</Text>
        <Text style={styles.tutorApp}>Tutor App</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  learningLanding: {
    top: 0,
    left: 0,
    backgroundColor: "rgba(6,5,138,1)",
    position: "absolute",
    right: 0,
    bottom: 0
  },
  image: {
    top: 268,
    left: 129,
    width: 105,
    height: 100,
    position: "absolute"
  },
  loremIpsum: {
    top: 381,
    left: 86,
    color: "rgba(245,241,241,1)",
    position: "absolute",
    fontSize: 25,
    fontFamily: "roboto-700",
    textAlign: "center"
  },
  tutorApp: {
    top: 446,
    left: 143,
    color: "rgba(245,241,241,1)",
    position: "absolute",
    fontSize: 18,
    fontFamily: "roboto-regular",
    textAlign: "center"
  },
  learningLandingStack: {
    flex: 1
  }
});

export default Untitled1;
