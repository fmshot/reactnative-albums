import React, { Component } from "react";
import { StyleSheet, View, Text, Image, ScrollView } from "react-native";

function Untitled2(props) {
  return (
    <View style={styles.container}>
      <View style={styles.rect2}>
        <View style={styles.clusterA412ColumnRow}>
          <View style={styles.clusterA412Column}>
            <Text style={styles.clusterA412}>CLUSTER A412</Text>
            <Text style={styles.classroom12}>CLASSROOM 12</Text>
          </View>
          <Image
            source={require("../assets/images/reactlogo1.png")}
            resizeMode="contain"
            style={styles.image3}
          ></Image>
        </View>
        <Text style={styles.studentList}>Student List</Text>
        <View style={styles.scrollArea}>
          <ScrollView
            horizontal={false}
            contentContainerStyle={styles.scrollArea_contentContainerStyle}
          >
            <View style={styles.rect3}>
              <View style={styles.image2Row}>
                <Image
                  source={require("../assets/images/ayahay_logo.png")}
                  resizeMode="contain"
                  style={styles.image2}
                ></Image>
                <Text style={styles.loremIpsum}>Lorem Ipsum</Text>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  rect2: {
    width: 375,
    height: 812,
    backgroundColor: "rgba(6,5,138,1)"
  },
  clusterA412: {
    color: "rgba(245,241,241,1)",
    fontSize: 24,
    fontFamily: "roboto-700"
  },
  classroom12: {
    color: "rgba(245,241,241,1)",
    fontSize: 16,
    fontFamily: "roboto-regular",
    marginLeft: 19
  },
  clusterA412Column: {
    width: 166,
    marginTop: 34,
    marginBottom: 25
  },
  image3: {
    width: 97,
    height: 99,
    marginLeft: 88
  },
  clusterA412ColumnRow: {
    height: 99,
    flexDirection: "row",
    marginTop: 14,
    marginLeft: 21,
    marginRight: 3
  },
  studentList: {
    color: "rgba(224,220,220,1)",
    fontSize: 23,
    fontFamily: "roboto-regular",
    marginTop: 39,
    marginLeft: 126
  },
  scrollArea: {
    width: 273,
    height: 70,
    marginTop: 24,
    marginLeft: 51
  },
  scrollArea_contentContainerStyle: {
    width: 273,
    height: 70,
    flexDirection: "column"
  },
  rect3: {
    width: 273,
    height: 70,
    backgroundColor: "rgba(251,246,246,1)",
    borderRadius: 19,
    borderColor: "#000000",
    borderWidth: 0,
    flexDirection: "row"
  },
  image2: {
    width: 48,
    height: 49
  },
  loremIpsum: {
    color: "#121212",
    fontFamily: "roboto-regular",
    marginLeft: 100,
    marginTop: 11
  },
  image2Row: {
    height: 49,
    flexDirection: "row",
    flex: 1,
    marginRight: 24,
    marginLeft: 19,
    marginTop: 10
  }
});

export default Untitled2;
