import React, { Component } from "react";
import { StyleSheet, View, Text, Image, ScrollView } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen'

function StudentListScreen(props) {
  return (
    <View style={styles.container}>
      {/* <View style={styles.rect2}> */}
          <View style={styles.clusterA412Column}>
          <View style={styles.clusterA412ColumnRow}>
            <Text style={styles.clusterA412}>CLUSTER A412</Text>
            <Text style={styles.classroom12}>CLASSROOM 12</Text>
          </View>
          <Image
            source={require("../assets/images/reactlogo1.png")}
            resizeMode="contain"
            style={styles.image3}
          ></Image>
        </View>
        {/* </View> */}
        <Text style={styles.studentList}>Student List</Text>
        <View style={styles.scrollArea}>
          <ScrollView
            horizontal={false}
            contentContainerStyle={styles.scrollArea_contentContainerStyle}
          >
            <View style={styles.rect3}>
              <View style={styles.image2Row}>
                <Image
                  source={require("../assets/images/ayahay_logo.png")}
                  resizeMode="contain"
                  style={styles.image2}
                ></Image>
                <Text style={styles.loremIpsum}>Lorem Ipsum</Text>
              </View>
            </View>
          </ScrollView>
        </View>
      {/* </View> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(6,5,138,1)",
    // alignItems: 'center'
    // justifyContent: 'center'
  },
  rect2: {
    width: wp('100%'),
  //   //     height: hp('100%'),
  //       flex: 1,
  //   //     alignSelf: 'flex-end',
  //   //     resizeMode: 'contain',
  //   backgroundColor: "rgba(6,5,138,1)"
  },
  clusterA412Column: {
    // width: 166,
    // width: wp('100%'),
    // alignItems: 'baseline',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 34,
    marginBottom: 25
  },
  clusterA412ColumnRow: {
    width: wp('40%'),
    justifyContent: 'center',
    // height: 99,
    // flexDirection: "column",
    // marginTop: 14,
    // marginLeft: 21,
    // marginRight: 3
  },
  image3: {
    width: wp('10%'),
    height: hp('18%'),
    alignSelf: 'flex-end',
    resizeMode: 'contain',
    // marginTop: -40,
    // alignSelf: 'flex-end',
  },
  clusterA412: {
    color: "rgba(245,241,241,1)",
    fontSize: 24,
    fontFamily: "roboto-700",
    alignSelf: 'flex-start',
    textAlign: 'center',
  },
  classroom12: {
    color: "rgba(245,241,241,1)",
    fontSize: 9,
    fontFamily: "roboto-regular",
    // alignItems: 'center',
    textAlign: 'center',
    // alignSelf: 'flex-start',
    marginTop: 0,
    // marginLeft: 21,
    // marginLeft: 24
  },
  
  studentList: {
    color: "rgba(224,220,220,1)",
    fontSize: 23,
    fontFamily: "roboto-regular",
    // marginTop: 39,
    // marginLeft: 126
    alignSelf: 'center',
  },
  scrollArea: {
    // width: wp('70%'),
    // height: hp('40%'),
    // alignSelf: 'center',
  },
  scrollArea_contentContainerStyle: {
    width: wp('70%'),
    height: hp('100%'),
    flexDirection: "column",
    alignSelf: 'center',
    alignItems: 'center'
  },
  rect3: {
    width: wp('50%'),
    height: hp('20%'),
    backgroundColor: "rgba(251,246,246,1)",
    borderRadius: 8,
    borderColor: "#000000",
    borderWidth: 0,
    flexDirection: "row"
  },
  image2: {
    width: wp('7%'),
    height: hp('16%'),
  },
  loremIpsum: {
    color: "#121212",
    fontFamily: "roboto-regular",
    // marginLeft: 100,
    marginTop: 11,
    // alignItems: 'flex-end',
  },
  image2Row: {
    height: 49,
    flexDirection: "row",
    flex: 1,
    margin: 10,
    // marginRight: 24,
    // marginLeft: 19,
    // marginTop: 10,
    justifyContent: 'space-between',
  }
});

export default StudentListScreen;
