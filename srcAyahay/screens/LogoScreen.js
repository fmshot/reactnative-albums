import React, { Component } from "react";
import { StyleSheet, View, SafeAreaView, ScrollView, Image } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen'

function LogoScreen(props) {
  return (
    <View style={styles.container}>
      <Image
        source={require("../assets/images/reactlogo.png")}
        resizeMode="contain"
        style={styles.image}
      ></Image>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(6,5,138,1)",
    // flexDirection: 'row',
    // alignItems: 'center'
  },
  // learningLanding: {
  //   top: 0,
  //   left: 0,
  //   backgroundColor: "rgba(6,5,138,1)",
  //   position: "absolute",
  //   right: 0,
  //   bottom: 0
  // },
  image: {
    width: wp('40%'),
        height: hp('30%'),
        alignSelf: 'center'
    // top: 268,
    // left: 129,
    // width: 105,
    // height: 100,
    // position: "absolute"
  },
});

export default LogoScreen;
