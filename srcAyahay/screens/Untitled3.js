import React, { Component } from "react";
import { StyleSheet, View, Image } from "react-native";

function Untitled3(props) {
  return (
    <View style={styles.container}>
      <Image
        source={require("../assets/images/reactlogo.png")}
        resizeMode="contain"
        style={styles.image}
      ></Image>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(6,5,138,1)"
  },
  image: {
    width: 132,
    height: 151,
    marginTop: 36,
    marginLeft: 122
  }
});

export default Untitled3;
