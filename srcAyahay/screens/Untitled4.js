import React, { Component } from "react";
import { StyleSheet, View, Text, Image } from "react-native";

function Untitled4(props) {
  return (
    <View style={styles.container}>
      <View style={styles.rect30Stack}>
        <View style={styles.rect30}>
          <View style={styles.rect29}>
            <View style={styles.loremIpsum2Stack}>
              <Text style={styles.loremIpsum2}>
                I noticed Khadija has a keen{"\n"} interest in mathematics. She
                {"\n"}is always punctual and arrives before all students.
              </Text>
              <Text style={styles.text}>
                I noticed Khadija has a keen{"\n"} interest in mathematics. She
                {"\n"}is always punctual and arrives before all students.
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.rect31}>
          <Image
            source={require("../assets/images/reactlogo2.png")}
            resizeMode="contain"
            style={styles.image}
          ></Image>
          <Text style={styles.khadijabubaMusa}>KHADIJABUBA MUSA</Text>
          <Text style={styles.typeOfReview}>Type Of Review</Text>
          <View style={styles.group2}>
            <View style={styles.group3}>
              <View style={styles.rect210}>
                <Text style={styles.positive}>Positive</Text>
              </View>
            </View>
          </View>
          <View style={styles.rect25}>
            <Text style={styles.negative}>Negative</Text>
          </View>
          <View style={styles.rect28}>
            <Text style={styles.neutral}>Neutral</Text>
          </View>
          <Text style={styles.loremIpsum}>Notes About Student Review</Text>
          <View style={styles.rect2102}></View>
          <View style={styles.rect213}>
            <Text style={styles.submit}>Submit</Text>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  rect30: {
    top: 0,
    left: 4,
    width: 375,
    height: 812,
    backgroundColor: "rgba(6,5,138,1)",
    position: "absolute"
  },
  rect29: {
    width: 375,
    height: 812,
    backgroundColor: "rgba(6,5,138,1)"
  },
  loremIpsum2: {
    top: 0,
    left: 0,
    color: "rgba(8,8,8,1)",
    position: "absolute",
    fontSize: 20,
    fontFamily: "roboto-regular"
  },
  text: {
    top: 0,
    left: 0,
    color: "rgba(8,8,8,1)",
    position: "absolute",
    fontSize: 20,
    fontFamily: "roboto-regular"
  },
  loremIpsum2Stack: {
    width: 324,
    height: 80,
    marginTop: 546,
    marginLeft: 51
  },
  rect31: {
    top: 0,
    left: 0,
    width: 375,
    height: 812,
    backgroundColor: "rgba(6,5,138,1)",
    position: "absolute"
  },
  image: {
    width: 186,
    height: 139,
    marginTop: 25,
    marginLeft: 91
  },
  khadijabubaMusa: {
    color: "rgba(255,255,255,1)",
    fontSize: 24,
    fontFamily: "roboto-700",
    textAlign: "justify",
    marginTop: 34,
    marginLeft: 66
  },
  typeOfReview: {
    color: "rgba(255,255,255,1)",
    fontSize: 24,
    fontFamily: "roboto-regular",
    marginTop: 28,
    marginLeft: 110
  },
  group2: {
    width: 272,
    height: 45,
    marginTop: 23,
    marginLeft: 55
  },
  group3: {
    width: 272,
    height: 45
  },
  rect210: {
    width: 272,
    height: 45,
    backgroundColor: "rgba(21,204,201,1)",
    borderRadius: 12,
    borderColor: "#000000",
    borderWidth: 0
  },
  positive: {
    color: "rgba(10,10,10,1)",
    fontSize: 24,
    fontFamily: "roboto-regular",
    textAlign: "center",
    marginTop: 10,
    marginLeft: 94
  },
  rect25: {
    width: 272,
    height: 45,
    backgroundColor: "rgba(204,21,71,1)",
    borderRadius: 12,
    borderColor: "#000000",
    borderWidth: 0,
    marginTop: 19,
    marginLeft: 55
  },
  negative: {
    color: "rgba(10,10,10,1)",
    fontSize: 24,
    fontFamily: "roboto-regular",
    textAlign: "center",
    marginTop: 8,
    marginLeft: 94
  },
  rect28: {
    width: 272,
    height: 45,
    backgroundColor: "rgba(252,255,255,1)",
    borderRadius: 12,
    borderColor: "#000000",
    borderWidth: 0,
    marginTop: 18,
    marginLeft: 55
  },
  neutral: {
    color: "rgba(10,10,10,1)",
    fontSize: 24,
    fontFamily: "roboto-regular",
    textAlign: "center",
    marginTop: 10,
    marginLeft: 94
  },
  loremIpsum: {
    color: "rgba(247,239,239,1)",
    fontSize: 22,
    fontFamily: "roboto-regular",
    textAlign: "center",
    marginTop: 25,
    marginLeft: 47
  },
  rect2102: {
    width: 272,
    height: 117,
    backgroundColor: "rgba(250,253,253,1)",
    borderRadius: 12,
    borderColor: "#000000",
    borderWidth: 0,
    marginTop: 15,
    marginLeft: 53
  },
  rect213: {
    width: 272,
    height: 45,
    backgroundColor: "rgba(21,113,204,1)",
    borderRadius: 12,
    borderColor: "#000000",
    borderWidth: 0,
    marginTop: 97,
    marginLeft: 55
  },
  submit: {
    color: "rgba(249,245,245,1)",
    fontSize: 24,
    fontFamily: "roboto-700",
    textAlign: "center",
    marginTop: 10,
    marginLeft: 94
  },
  rect30Stack: {
    width: 379,
    height: 812,
    marginLeft: -4
  }
});

export default Untitled4;
