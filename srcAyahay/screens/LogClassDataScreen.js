import React, { Component } from "react";
import { StyleSheet, View, ScrollView, Text, Image } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen'
function LogClassDataScreen(props) {
  return (
    <View style={styles.container}>
      <ScrollView>

          <Image
            source={require("../assets/images/reactlogo2.png")}
            resizeMode="contain"
            style={styles.image}
          ></Image>
          {/* <Text style={styles.khadijabubaMusa}>KHADIJABUBA MUSA</Text>
          <Text style={styles.typeOfReview}>Type Of Review</Text>
         
              <View style={styles.rect210}>
                <Text style={styles.positive}>Positive</Text>
              </View>
           
          <View style={styles.rect25}>
            <Text style={styles.negative}>Negative</Text>
          </View>
          <View style={styles.rect28}>
            <Text style={styles.neutral}>Neutral</Text>
          </View> */}
          <Text style={styles.loremIpsum}>Notes About Student Review</Text>
          <View style={styles.rect2102}>
          <ScrollView>
          <Text style={styles.comments}>
     We had some problems being able to teach some lessons so 
     we improvised using learning sticks.      
          </Text>
          </ScrollView>
          </View>

          <Text style={styles.loremIpsum}>CLASS PHOTO OF THE DAY</Text>
          <View style={styles.rect2102}>
          <Text style={styles.comments}>
                    TAKE PHOTO      
          </Text>
          </View>
          <Text style={styles.attendance}>ATTENDANCE</Text>
        <View style={styles.group3}>
          <View style={styles.rect5}>
            <Text style={styles.aishaIlyasu}>AISHA ILYASU</Text>
          </View>
        </View>
          <View style={styles.rect213}>
            <Text style={styles.submit}>Submit</Text>
          </View>
     
      </ScrollView>
      {/* {/* <View style={styles.rect}>
        <Image
          source={require("../assets/images/reactlogo1.png")}
          // source={require("../assets/images/reac")}
          resizeMode="contain"
          style={styles.image}
        ></Image>
        <View style={styles.group}>
          <Text style={styles.loremIpsum}>
            NOTES ABOUT THE DAY&#39;S CLASS IF ANY
          </Text>
        </View>
        <View style={styles.group2Stack}>
          <View style={styles.group2}>
            <View style={styles.rect2}>
              <Text style={styles.loremIpsum4}></Text>
            </View>
          </View>
          <Text style={styles.loremIpsum3}>
            We had some problems being{"\n"}able to teach some lessons so{"\n"}
            we improvised using learning{"\n"} sticks
          </Text>
        </View>
        <Text style={styles.loremIpsum5}>CLASS PHOTO OF THE DAY</Text>
        <View style={styles.rect3}>
          <View style={styles.rect4}>
            <Text style={styles.takePhoto}>TAKE PHOTO</Text>
          </View>
        </View>
        <Text style={styles.attendance}>ATTENDANCE</Text>
        <View style={styles.group3}>
          <View style={styles.rect5}>
            <Text style={styles.aishaIlyasu}>AISHA ILYASU</Text>
          </View>
        </View>
        <View style={styles.rect6}>
          <Text style={styles.submit}>SUBMIT</Text>
        </View>
      </View>
      */}
    </View>
  );
}




const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(6,5,138,1)",
  },
  rect31: {
    backgroundColor: "rgba(6,5,138,1)",
    width: wp('100%'),
    height: hp('100%'),
    alignSelf: 'center'

  },
  image: {
    width: wp('40%'),
    height: hp('30%'),
    alignSelf: 'center'
  },
  khadijabubaMusa: {
    color: "rgba(255,255,255,1)",
    fontSize: hp('3.6%'),
    fontFamily: "roboto-700",
    textAlign: 'center',
    marginBottom: hp('5.5%'),
  
  },
  typeOfReview: {
    color: "rgba(255,255,255,1)",
    fontSize: hp('2.6%'),
    fontFamily: "roboto-regular",
    textAlign: 'center',
    marginBottom: hp('1.5%'),


  },
  // group2: {
  //   width: 272,
  //   height: 45,
  //   marginTop: 23,
  //   marginLeft: 55
  // },
  // group3: {
  //   width: 272,
  //   height: 45
  // },
  rect210: {
    width: wp('70%'),
    height: hp('6%'),
    alignSelf: 'center',
    backgroundColor: "rgba(21,204,201,1)",
    borderRadius: 12,
    borderColor: "#000000",
  },
  positive: {
    color: "rgba(10,10,10,1)",
    // fontSize: 24,
    fontSize:hp('3.3%'),
    fontFamily: "roboto-regular",
    textAlign: "center",
    marginTop: hp('0.5%'),
    // marginLeft: 94
  },
  rect25: {
    backgroundColor: "rgba(204,21,71,1)",
    width: wp('70%'),
    height: hp('6%'),
    alignSelf: 'center',
    borderRadius: 12,
    borderColor: "#000000",
    borderWidth: 0,
    marginTop: hp('1%'),
  },
  negative: {
    color: "rgba(10,10,10,1)",
    fontSize:hp('3.3%'),
    fontFamily: "roboto-regular",
    textAlign: "center",
    marginTop: hp('0.5%'),
  },
  rect28: {
    backgroundColor: "rgba(252,255,255,1)",
    width: wp('70%'),
    height: hp('6%'),
    alignSelf: 'center',
    borderRadius: 12,
    borderColor: "#000000",
    borderWidth: 0,
    marginTop: hp('1%'),
  },
  neutral: {
    color: "rgba(10,10,10,1)",
    fontSize:hp('3.3%'),
    fontFamily: "roboto-regular",
    textAlign: "center",
    marginTop: hp('0.5%'),
  },
  loremIpsum: {
    color: "rgba(247,239,239,1)",
    fontSize:hp('3.3%'),
    fontFamily: "roboto-regular",
    textAlign: "center",
    marginTop: hp('8%'),
  },
  comments: {
    // color: "rgba(247,239,239,1)",
    fontSize:hp('3.8%'),
    // fontFamily: "roboto-regular",
    textAlign: "center",
    padding:  hp('1%'),
    // marginTop: hp('8%'),
  },
  rect2102: {
    width: wp('50%'),
    height: hp('20%'),
    backgroundColor: "rgba(230, 230, 230,1)",
    // borderRadius: hp('1%'),
    borderColor: "#000000",
    // borderWidth: 0,
    marginTop: hp('2%'),
    alignSelf: 'center'
  },
  rect213: {
    width: wp('30%'),
    height: hp('7%'),
    backgroundColor: "rgba(21,113,204,1)",
    alignSelf: 'center',
    borderRadius: hp('1%'),
    borderColor: "#000000",
    // borderWidth: 0,
    marginTop: hp('10%'),
  },
  attendance: {
    color: "rgba(253,250,250,1)",
    fontSize: hp('4%'),
        fontFamily: "roboto-regular",
    marginTop: hp('3%'),
    // marginLeft: 118,
    alignSelf: 'center'
  },
  rect5: {
          width: wp('40%'),
          height: hp('6%'),
    backgroundColor: "rgba(230, 230, 230,1)",
    borderRadius: 15,
    borderColor: "#000000",
    // borderWidth: 0
    alignSelf: 'center'
  },
    aishaIlyasu: {
    color: "rgba(12,12,12,1)",
    // fontSize: 21,
    fontFamily: "roboto-regular",
    fontSize:hp('2.3%'),
    // fontFamily: "roboto-regular",
    textAlign: "center",
    padding:  hp('1%'),
  },
  submit: {
    color: "rgba(249,245,245,1)",
    fontSize: hp('2%'),
    fontFamily: "roboto-700",
    textAlign: "center",
    marginTop: hp('1%'),
  },
  loremIpsum2: {
    top: 0,
    left: 0,
    color: "rgba(8,8,8,1)",
    // position: "absolute",
    alignSelf: 'center',
    fontSize:hp('1%'),
    fontFamily: "roboto-regular"
  },
  rect30: {
    top: 0,
    left: 4,
    width: 375,
    height: 812,
    backgroundColor: "rgba(6,5,138,1)",
    position: "absolute"
  },
  rect29: {
    width: 375,
    height: 812,
    backgroundColor: "rgba(6,5,138,1)"
  },
 
  text: {
    top: 0,
    left: 0,
    color: "rgba(8,8,8,1)",
    position: "absolute",
    fontSize: 20,
    fontFamily: "roboto-regular"
  },
  loremIpsum2Stack: {
    width: 324,
    height: 80,
    marginTop: 546,
    marginLeft: 51
  },
});




// const styles = StyleSheet.create({
//   container: {
//     width: 375,
//     height: 812
//   },
//   rect: {
//     backgroundColor: "rgba(6,5,138,1)",
//     flex: 1
//   },
//   image: {
//     width: 130,
//     height: 110,
//     marginTop: 46,
//     marginLeft: 123
//   },
//   group: {
//     width: 326,
//     height: 18,
//     marginTop: 12,
//     marginLeft: 27
//   },
//   loremIpsum: {
//     color: "rgba(255,252,252,1)",
//     fontSize: 18,
//     fontFamily: "roboto-regular",
//     textAlign: "center"
//   },
//   group2: {
//     top: 0,
//     left: 2,
//     width: 260,
//     height: 130,
//     position: "absolute"
//   },
//   rect2: {
//     width: 260,
//     height: 130,
//     backgroundColor: "rgba(230, 230, 230,1)",
//     borderRadius: 13,
//     borderColor: "#000000",
//     borderWidth: 0
//   },
//   loremIpsum4: {
//     color: "rgba(6,6,6,1)",
//     fontSize: 20,
//     fontFamily: "roboto-regular",
//     marginTop: 35,
//     marginLeft: 237
//   },
//   loremIpsum3: {
//     top: 27,
//     left: 0,
//     color: "rgba(6,6,6,1)",
//     position: "absolute",
//     fontSize: 19,
//     fontFamily: "roboto-regular",
//     textAlign: "center"
//   },
//   group2Stack: {
//     width: 262,
//     height: 130,
//     marginTop: 29,
//     marginLeft: 58
//   },
//   loremIpsum5: {
//     color: "rgba(249,246,246,1)",
//     fontSize: 21,
//     fontFamily: "roboto-regular",
//     marginTop: 29,
//     marginLeft: 59
//   },
//   rect3: {
//     width: 260,
//     height: 130,
//     marginTop: 11,
//     marginLeft: 60
//   },
//   rect4: {
//     width: 260,
//     height: 130,
//     backgroundColor: "rgba(230, 230, 230,1)",
//     borderRadius: 13,
//     borderColor: "#000000",
//     borderWidth: 0
//   },
//   takePhoto: {
//     color: "rgba(10,4,4,1)",
//     fontSize: 21,
//     fontFamily: "roboto-regular",
//     marginTop: 44,
//     marginLeft: 65
//   },
  
//   group3: {
//     width: 250,
//     height: 40,
//     marginTop: 23,
//     marginLeft: 69
//   },
//   rect5: {
//     width: 250,
//     height: 40,
//     backgroundColor: "rgba(230, 230, 230,1)",
//     borderRadius: 15,
//     borderColor: "#000000",
//     borderWidth: 0
//   },
//   aishaIlyasu: {
//     color: "rgba(12,12,12,1)",
//     fontSize: 21,
//     fontFamily: "roboto-regular",
//     marginTop: 10,
//     marginLeft: 16
//   },
//   rect6: {
//     width: 264,
//     height: 40,
//     backgroundColor: "rgba(21,113,204,1)",
//     borderRadius: 11,
//     borderColor: "#000000",
//     borderWidth: 0,
//     marginTop: 94,
//     marginLeft: 52
//   },
//   submit: {
//     color: "rgba(253,250,250,1)",
//     fontSize: 24,
//     fontFamily: "roboto-700",
//     textAlign: "center",
//     marginTop: 10,
//     marginLeft: 99
//   }
// });

export default LogClassDataScreen;
