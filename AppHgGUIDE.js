import React, { useState } from "react";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createDrawerNavigator } from "react-navigation-drawer";
// import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import OcticonsIcon from "react-native-vector-icons/Octicons";
import { setNavigator } from './src/navigationRef';
import SimpleLineIconsIcon from "react-native-vector-icons/SimpleLineIcons";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";

// import { AppLoading } from "expo";
// import * as Font from "expo-font";
import LandingPage from "./srcHgApp/screens/LandingPage";
import Untitled1 from "./srcHgApp/screens/Untitled1";
import SelectLocation from "./srcHgApp/screens/SelectLocation";
import ConfirmLocation from "./srcHgApp/screens/ConfirmLocation";
import SelectDate from "./srcHgApp/screens/SelectDate";
// import Untitled4f from "./srcHgApp/screens/Untitled4f";
import EventDetail from "./srcHgApp/screens/EventDetail";
import CountdownScreen from "./srcHgApp/screens/CountdownScreen";
import LogoTitle from './srcHgApp/components/LogoTitle';
import { Dimensions } from 'react-native';
import { Provider  } from './srcHgApp/context/eventsContext';
import Tabbar from "./srcHgApp/components/Tabbar";

// const DrawerNavigation = createDrawerNavigator({
//   EventDetail: EventDetail,

//   SelectDate: SelectDate,
//   SelectLocation: SelectLocation,
//   Untitled1: Untitled1,
//   LandingPage: LandingPage,

// const DrawerNavigation = createDrawerNavigator({
//   SelectLocation: SelectLocation,
//   EventDetail: EventDetail,

//   SelectDate: SelectDate,
  
//   Untitled1: Untitled1,
//   LandingPage: LandingPage,
//   // ConfirmLocation: ConfirmLocation,
  
//   // Untitled4f: Untitled4f,
//   Untitled6: Untitled6,
//   // Untitled7: Untitled7
// });

// const StackNavigation = createStackNavigator(
//   {
//     DrawerNavigation: {
//       screen: DrawerNavigation
//     },
//     Untitled1: Untitled1,
//     LandingPage: LandingPage,
    
//     SelectLocation: SelectLocation,
//     ConfirmLocation: ConfirmLocation,
//     SelectDate: SelectDate,
//     // Untitled4f: Untitled4f,
//     EventDetail: EventDetail,
//     Untitled6: Untitled6,
//     // Untitled7: Untitled7
//   },
//   {
//     defaultNavigationOptions:{
//       headerTitle: <LogoTitle />,
//     headerTintColor: 'white',
//     headerStyle: {
//       // backgroundColor: '#131416',
//       height : Dimensions.get('window').height/12,
//     },
//   // headerLeft: <BackButton/> 
//     headerBackTitle: 'abck '
//     }
//   }
// );

const {height, width} = Dimensions.get('window')

const stackNavOptions = 
{
  defaultNavigationOptions:
      {headerTitle: <LogoTitle />,
  headerTintColor: 'black',
  headerStyle: {
    // backgroundColor: '#131416',
    height : Dimensions.get('window').height/12,
  },
// headerLeft: <BackButton/> 
  // headerBackTitle: 'abck '
} 
  
}


const navigator = createBottomTabNavigator({
  TabA: createStackNavigator({
    TabA: SelectLocation,
    EventDetail: EventDetail
    },
    {
      ...stackNavOptions
    }
  ), 
  TabB: createStackNavigator({
    TabB: SelectDate,
    EventDetail: EventDetail
  },
  {
    ...stackNavOptions
  }
  ),
  TabC: createStackNavigator({
    TabC: CountdownScreen,
    EventDetail: EventDetail
  },
  {
    ...stackNavOptions
  }
  ) ,
},
{
  // tabBarComponent: Tabbar,
  defaultNavigationOptions: ({navigation}) =>({
    tabBarIcon: ({focused, horizontal, tintColor }) => {
      const {routeName} = navigation.state
      color_name = focused ? 'white' :  'rgba(0,0,0,1)' 
      let iconSize = 35
      if (routeName === 'TabA'){
        return <OcticonsIcon name = 'home' size = {iconSize} color= {color_name}></OcticonsIcon>
      }else if (routeName === 'TabB'){
        return <SimpleLineIconsIcon name="calendar" size = {iconSize} color= {color_name} ></SimpleLineIconsIcon>
      }else if (routeName === 'TabC'){
        return <MaterialCommunityIconsIcon name="circle-slice-5" size = {iconSize} color = {color_name}></MaterialCommunityIconsIcon>
      }
    },
  
}),


backBehavior: 'history',
tabBarOptions: {
    showLabel: false,
    keyboardHidesTabBar: false,
    tabStyle: {
      // marginTop: 1,
      // height: height /6,
      backgroundColor: '#e6e6e6'
    },
    style: {
      height: height /12,
      backgroundColor: '#e6e6e6',
    },
}
}
);


export default createAppContainer(navigator);


// const AppContainer = createAppContainer(StackNavigation);

// function App() {
//   const [isLoadingComplete, setLoadingComplete] = useState(false);
//   if (!isLoadingComplete) {
//     return (
//       <AppLoading
//         startAsync={loadResourcesAsync}
//         onError={handleLoadingError}
//         onFinish={() => handleFinishLoading(setLoadingComplete)}
//       />
//     );
//   } else {
//     return isLoadingComplete ? <AppContainer /> : <AppLoading />;
//   }
// }
// async function loadResourcesAsync() {
//   await Promise.all([
//     Font.loadAsync({
//       "fugaz-one-regular": require("./src/assets/fonts/fugaz-one-regular.ttf"),
//       "roboto-regular": require("./src/assets/fonts/roboto-regular.ttf"),
//       "Futura-Medium": require("./src/assets/fonts/roboto-regular.ttf"),
//       "FuturaBT-Medium": require("./src/assets/fonts/roboto-regular.ttf")
//     })
//   ]);
// }
// function handleLoadingError(error) {
//   console.warn(error);
// }

// function handleFinishLoading(setLoadingComplete) {
//   setLoadingComplete(true);
// }

// export default App;
