import { AsyncStorage } from 'react-native';
import createDataContext from './createDataContext';
import FixrApi from '../api/motorFixr';
import { navigate } from '../navigationRef';

const productReducer = (state, action) => {
  switch (action.type) {
    case 'get_category_products':
      return  {...state, products: action.payload}
    case 'get_product_with_id':
      return  {...state, product: action.payload}
    default:
      return state;
  }
};


const getCategoryProducts = dispatch => async (id) => {
    const token = await AsyncStorage.getItem('token');

    const response = await FixrApi.get(`/products/?categories=${id}`, { token });
    dispatch({ type: 'get_category_products' , payload: response.data.results});
}

const getProductWithId = dispatch => async (id) => {
    const token = await AsyncStorage.getItem('token');
    const response = await FixrApi.get(`/products/${id}`, { token });
    dispatch({ type: 'get_product_with_id' , payload: response.data.results});
}


export const { Provider, Context } = createDataContext(
    productReducer,
  { getCategoryProducts, getProductWithId },
  []
);
