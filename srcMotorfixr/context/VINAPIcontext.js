import { AsyncStorage } from 'react-native';
import createDataContext from './createDataContext';
import FixrApi from '../api/motorFixr';
import { navigate } from '../navigationRef';

const vinapiReducer = (state, action) => {
  switch (action.type) {
    case 'decode_VIN':
      return  {...state, selectedCar: action.payload }
    default:
      return state;
  }
};


const decodeVIN = dispatch => async (vin) => {
    const token = await AsyncStorage.getItem('token');

    const response = await FixrApi.get(`/vin/cardetailsfromvin/?vin=${vin}`, { token });
    dispatch({ type: 'decode_VIN' , payload: response.data.results});
}



export const { Provider, Context } = createDataContext(
    vinapiReducer,
  { decodeVIN },
  []
);
