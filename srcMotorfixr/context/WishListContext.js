import { AsyncStorage } from 'react-native';
import createDataContext from './createDataContext';
import FixrApi from '../api/motorFixr';
import { navigate } from '../navigationRef';

const productReducer = (state, action) => {
  switch (action.type) {
    case 'remove_from_wishlist':
      return  {...state, products: state.products.filter(product=> product.id !== action.payload.id), wishlistids: state.products.filter(product=> product.id !== action.payload.id).map(product=> product.id) }
    case 'add_to_wishlist':
      return  {...state, products: [...state.products, action.payload], wishlistids: [...state.products, action.payload].map(product=> product.id)}
    case 'get_wishlist':
      return  {products: action.payload, wishlistids: action.payload.map(product=> product.id) }
    case 'get_wishlist_ids':
      return  { ...state, wishlistids: action.payload };
    default:
      return state;
  }
};


const addToWishList = dispatch => async (product) => {
    const response = await FixrApi.post(`/wishlist/`, { id : product.id });
    if (response.data.startsWith('true') ){
      dispatch({ type: 'add_to_wishlist' , payload: product });
    }else{
      dispatch({ type: 'remove_from_wishlist' , payload: product })
    }
    
}

const getWishList = dispatch => async () => {
  try{
    // const token = await AsyncStorage.getItem('token');
    const response = await FixrApi.get(`/wishlist/`);
    dispatch({ type: 'get_wishlist' , payload: response.data.results[0].products});
  }catch(error){
    console.log(error)
  }
}


const getWishListIds = dispatch => async () => {
  try{
    const response = await FixrApi.get(`/wishlist/`);
    dispatch({ type: 'get_wishlist_ids' , payload: response.data.results[0].products.map((product)=>product.id)});
  }catch(error){
    console.log(error)
  }
}


export const { Provider, Context } = createDataContext(
    productReducer,
  { addToWishList, getWishList, getWishListIds},
  []
);
