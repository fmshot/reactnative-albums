import { AsyncStorage } from 'react-native';
import createDataContext from './createDataContext';
import FixrApi from '../api/motorFixr';
import { navigate } from '../navigationRef';

const categoryReducer = (state, action) => {
  switch (action.type) {
    case 'get_categories':
      return  action.payload
    default:
      return state;
  }
};


const getCategories = dispatch => async () => {
    const token = await AsyncStorage.getItem('token');
    const response = await FixrApi.get('/categories', { token });
    dispatch({ type: 'get_categories' , payload: response.data.results});
}


export const { Provider, Context } = createDataContext(
    categoryReducer,
  { getCategories },
  []
);
