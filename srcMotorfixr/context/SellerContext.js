import { AsyncStorage } from 'react-native';
import createDataContext from './createDataContext';
import FixrApi from '../api/motorFixr';


const productReducer = (state, action) => {
  switch (action.type) {
    case 'get_seller_reviews':
      return  action.payload
    default:
      return state;
  }
};

const getSellerReviews = dispatch => async (id) => {
    const token = await AsyncStorage.getItem('token');
    const response = await FixrApi.get(`/reviews/?sellerRating__seller=${id}`, { token });
    dispatch({ type: 'get_seller_reviews' , payload: response.data.results});
}


export const { Provider, Context } = createDataContext(
    productReducer,
  { getSellerReviews },
  []
);
