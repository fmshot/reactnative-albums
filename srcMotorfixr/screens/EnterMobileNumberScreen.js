//
//  CreateUsername
//  Project
//
//  Created by girlProg.
//  Copyright © 2019 YediTech. All rights reserved.
//

import { TextInput, View, TouchableOpacity, Dimensions, StyleSheet, Text } from "react-native"
import React from "react"
import LogoTitle from "../components/Logotitle"
import MotorFixrButton from "../components/MotorFixrButton"
import FixrInput from "../components/FixrInput"


const EnterMobileNumberScreen = ({navigation}) => {
    return <View style={styles.container}>
            <FixrInput label='Enter your mobile number' placeholder="Phone number" />
        	<MotorFixrButton navigation={navigation} text="Continue" destination="EnterVerificationCode"/>
            <Text style={styles.signInButtonText}>Tap continue to get an SMS confirmation </Text>
        </View>
}

export default EnterMobileNumberScreen

EnterMobileNumberScreen.navigationOptions = {
    headerTitle: <LogoTitle />,
    headerTintColor: 'white',
    headerStyle: {
      backgroundColor: '#131416',
      height : 120,
    }
}

const styles = StyleSheet.create({
	container: {
		width: Dimensions.get("window").width,
		height: 264,
		flex:1,
		justifyContent: "center",
	  },
	  signInButtonText: {
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Book",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
        textAlign: "center",
        marginTop: 10
	},
	
})
