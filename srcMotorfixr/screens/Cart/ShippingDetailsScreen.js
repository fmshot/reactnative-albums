//
//  ShippingDetails
//  Project
//
//  Created by girlProg.
//  Copyright © 2018 YediTech. All rights reserved.
//

import { StyleSheet, TouchableOpacity, Image, TextInput, Text, View, Dimensions, ScrollView } from "react-native"
import React from "react"
import ChosenCarHeader from "../../components/ChosenCarHeader"
import BreadcrumbsNav from "../../components/BreadcrumbsNav"
import MotorFixrButton from "../../components/MotorFixrButton"

const CartShippingDetails = ({navigation}) => {

	
		return <>
            <BreadcrumbsNav withbackArrow={true} navigation={navigation} screenName="Add Shipping Details"/>
                <ScrollView contentContainerStyle={styles.shippingDetailsView}>
				<Image
					source={require("../../../assets/images/group-5.png")}
					style={styles.toppaginationImage}/>
				<View style={styles.shippingdetailsView}>
					<Text style={styles.shippingAddressLabelText}>Shipping Address</Text>
					<View style={styles.addrstackView}>
						<Text style={styles.streetAddressLabelText}>Street Address</Text>
						<View pointerEvents="box-none"
							style={{
								flex: 1,
								marginLeft: 1,
								marginTop: 7,
							}}>
							<TextInput
								autoCorrect={false}
								placeholder="0024 Shania Causeway Apt. 721"
								style={styles.streetaddrTextInput}/>
							<View
								style={styles.dividerView}/>
						</View>
					</View>
					<View
						style={styles.citystackView}>
						<Text
							style={styles.citylabelText}>City</Text>
						<View
							pointerEvents="box-none"
							style={{
								flex: 1,
								marginLeft: 1,
								marginTop: 8,
							}}>
							<View
								style={styles.dividerTwoView}/>
							<TextInput
								autoCorrect={false}
								placeholder="South Hester"
								style={styles.cityTextInput}/>
						</View>
					</View>
					<View
						style={{
							flex: 1,
						}}/>
					<View
						style={styles.phonenumberstackView}>
						<Text
							style={styles.phoneNumberLebelText}>Phone Number</Text>
						<View
							pointerEvents="box-none"
							style={{
								height: 23,
							}}>
							<Image
								source={require("../../../assets/images/path-3.png")}
								style={styles.dividerImage}/>
							<TextInput
								autoCorrect={false}
								placeholder="232-320-8145"
								style={styles.phonenumberTextInput}/>
						</View>
					</View>
				</View>
				{/* <MotorFixrButton navigation={navigation} text="Continue" destination="ReviewOrder" /> */}
				<MotorFixrButton navigation={navigation} text="Continue" destination="ReviewOrder" />
				{/* <TouchableOpacity
					onPress={navigation.navigate('CartFlow2', {}, NavigationActions.navigate({ routeName: 'ShippingDetails' }))}
					style={styles.continueButton}>
					<Text
						style={styles.continueButtonText}>Continue</Text>
				</TouchableOpacity> */}
			</ScrollView>
            </>
	
}

export default CartShippingDetails

CartShippingDetails.navigationOptions ={
	header: <ChosenCarHeader/>,
}

const styles = StyleSheet.create({
	shippingDetailsView: {
		// backgroundColor: "white",
        // flex: 1,
        marginBottom: 40
	},
	toppaginationImage: {
		resizeMode: "contain",
        height: 17,
        width: Dimensions.get("window").width,
        justifyContent: "center"
	},
	shippingdetailsView: {
		backgroundColor: "transparent",
		height: 231,
		marginLeft: 30,
		marginRight: 31,
		marginTop: 44,
	},
	shippingAddressLabelText: {
		backgroundColor: "transparent",
		color: "rgb(58, 44, 60)",
		fontFamily: "Avenir-Medium",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		alignSelf: "flex-start",
	},
	addrstackView: {
		backgroundColor: "transparent",
		height: 45,
		marginLeft: 2,
		marginRight: 2,
		marginTop: 18,
	},
	streetAddressLabelText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.28,
		backgroundColor: "transparent",
		alignSelf: "flex-start",
	},
	streetaddrTextInput: {
		backgroundColor: "transparent",
		padding: 0,
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Medium",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		position: "absolute",
		left: 4,
		width: 232,
		bottom: 0,
		height: 22,
	},
	dividerView: {
		backgroundColor: "white",
		position: "absolute",
		left: 0,
		right: 0,
		top: 20,
		height: 1,
	},
	citystackView: {
		backgroundColor: "transparent",
		height: 46,
		marginLeft: 2,
		marginRight: 2,
		marginTop: 26,
	},
	citylabelText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.28,
		alignSelf: "flex-start",
	},
	dividerTwoView: {
		backgroundColor: "white",
		position: "absolute",
		left: 0,
		right: 0,
		top: 21,
		height: 1,
	},
	cityTextInput: {
		backgroundColor: "transparent",
		padding: 0,
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Medium",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		position: "absolute",
		left: 4,
		width: 96,
		bottom: 0,
		height: 22,
	},
	phonenumberstackView: {
		backgroundColor: "transparent",
		height: 50,
		marginLeft: 2,
		marginBottom: 1,
		justifyContent: "flex-end",
	},
	phoneNumberLebelText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.28,
		backgroundColor: "transparent",
		alignSelf: "flex-start",
		marginBottom: 11,
	},
	dividerImage: {
		resizeMode: "cover",
		backgroundColor: "transparent",
		position: "absolute",
		left: 0,
		right: 0,
		bottom: 0,
		height: 3,
	},
	phonenumberTextInput: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Medium",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		backgroundColor: "transparent",
		padding: 0,
		position: "absolute",
		left: 6,
		width: 100,
		bottom: 1,
		height: 22,
	},
	continueButtonText: {
		color: "white",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
	},
	continueButton: {
		backgroundColor: "rgb(19, 20, 22)",
		borderRadius: 30,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		height: 60,
		marginLeft: 35,
		marginRight: 30,
		marginTop: 101,
	},
	continueButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
})
