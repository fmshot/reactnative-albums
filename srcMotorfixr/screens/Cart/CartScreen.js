import React, {useContext, useEffect} from 'react';
import { StyleSheet, View, Image, Text, FlatList, TouchableOpacity, Dimensions, ScrollView,  ActivityIndicator } from 'react-native';
import ChosenCarHeader from "../../components/ChosenCarHeader"
import BreadcrumbsNav from "../../components/BreadcrumbsNav"
import CartLineItem from "../../components/CartLineItem"
import MotorFixrButton from "../../components/MotorFixrButton"
import {SafeAreaView, NavigationActions} from "react-navigation"
import {Context as CartContext} from "../../context/CartContext"

const CartScreen = ({ navigation }) => {
	const {state,  getCartItems } = useContext(CartContext)
	useEffect(() => {
		getCartItems();
	  }, []);
  	return (
    	<>
		<BreadcrumbsNav withbackArrow={false} navigation={navigation} screenName="Cart"/>
		{state.cartItems?
	  	<ScrollView contentContainerStyle={styles.cartView}>
			 <SafeAreaView> 
				<FlatList
				data ={state.cartItems}
				keyExtractor={item => item.id.toString()}
				renderItem={({ item }) => {
				return (
					
					<CartLineItem navigation={navigation} lineitem={item}  />
				);
				}}
				/>
				
				<View style={styles.moreOptionsStack}>
					<TouchableOpacity style={styles.moreOptionsView}>
						<Text style={styles.moreOptionsText}>Promo Code</Text>
						<Image source={require("../../../assets/images/icon-group.png")} style={styles.iconGroupImage}/>
					</TouchableOpacity>
					<TouchableOpacity style={styles.moreOptionsView}>
						<Text style={styles.moreOptionsText}>Payment Options</Text>
						<Image source={require("../../../assets/images/icon-group.png")} style={styles.iconGroupImage}/>
					</TouchableOpacity>
				</View> 

				<View style={styles.carttotalView}>
					<View style={styles.summaryView}>
						<Text style={styles.summaryTextLeft}>Subtotal</Text>
						<Text style={styles.summaryTextRight}>₦{state.cartItems.reduce((total, cartItem)=>(total + (cartItem.quantity * cartItem.product.price)), 0)} </Text>
					</View>
					<View style={styles.summaryView}>
						<Text style={styles.summaryTextLeft}>Shipping</Text>
						<Text style={styles.summaryTextRight}>TBD</Text>
					</View>
					<View style={{...styles.summaryView, marginTop: 20}}>
						<Text style={styles.summaryTextLeft}>Total</Text>
						<Text style={styles.summaryTextRight}>₦6,000 </Text>
					</View>
					<View style={styles.dividerView}/>
					
				</View>
				<MotorFixrButton navigation={navigation} text="Proceed to Checkout" destination="ShippingDetails" />
				
				</SafeAreaView>
			</ScrollView>
			: <ActivityIndicator/> }
			
    </>
  );
};

CartScreen.navigationOptions = {
	header: <ChosenCarHeader/>,
};

const styles = StyleSheet.create({
	reviewOrderView: {
		backgroundColor: "white",
		flex: 1,
		marginBottom: 40,
	},
	group5TwoView: {
		backgroundColor: "transparent",
		alignSelf: "center",
		width: 223,
		height: 17,
		marginTop: 191,
	},
	ovalView: {
		backgroundColor: "rgb(19, 20, 22)",
		borderRadius: 8.5,
		width: 17,
		height: 17,
	},
	iconImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: null,
		height: 7,
		marginLeft: 4,
		marginRight: 4,
	},
	lineView: {
		backgroundColor: "white",
		width: 87,
		height: 1,
	},
	iconTwoImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		flex: 1,
		height: 7,
		marginLeft: 3,
		marginRight: 4,
	},
	lineTwoView: {
		backgroundColor: "white",
		width: 87,
		height: 1,
		marginRight: 16,
	},
	ovalThreeView: {
		backgroundColor: "transparent",
		borderRadius: 8.5,
		borderWidth: 0.5,
		borderColor: "white",
		borderStyle: "solid",
		width: 17,
		height: 17,
	},
	ovalTwoView: {
		backgroundColor: "rgb(19, 20, 22)",
		borderRadius: 8.5,
		width: 17,
		height: 17,
	},
	cartlineitemstackView: {
		backgroundColor: "transparent",
		height: 143,
		marginTop: 31,
	},
	cartLineItemView: {
		backgroundColor: "transparent",
		height: 143,
		justifyContent: "center",
	},
	group4View: {
		backgroundColor: "white",
		shadowColor: "rgba(0, 0, 0, 0.03)",
		shadowRadius: 8,
		shadowOpacity: 1,
		height: 143,
	},
	groupView: {
		backgroundColor: "transparent",
		height: 90,
		marginLeft: 15,
		marginRight: 15,
		marginTop: 15,
		flexDirection: "row",
		alignItems: "flex-start",
	},
	imageView: {
		backgroundColor: "transparent",
		alignSelf: "center",
		width: 90,
		height: 90,
	},
	rectangle2Image: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: null,
		height: 90,
	},
	profileImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: null,
		height: 70,
		marginLeft: 10,
		marginRight: 10,
	},
	alternatorBeltText: {
		backgroundColor: "transparent",
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		lineHeight: 20,
		marginLeft: 15,
		marginTop: 6,
	},
	textText: {
		backgroundColor: "transparent",
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Roman",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "right",
		lineHeight: 20,
		marginTop: 6,
	},
	textTwoText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
	},
	conditionBelgiumText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
		marginTop: 21,
	},
	editText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
	},
	saveForLaterText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
		marginRight: 54,
	},
	removeText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
		backgroundColor: "transparent",
	},
	quantity2Text: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
		marginLeft: 121,
	},
	group6View: {
		backgroundColor: "transparent",
		alignSelf: "flex-end",
		width: 322,
		height: 71,
		marginRight: 21,
		marginTop: 41,
		alignItems: "flex-start",
	},
	subtotal6000Text: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Roman",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
		marginLeft: 24,
	},
	shippingTbdText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Roman",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
		marginLeft: 24,
		marginTop: 8,
	},
	line2View: {
		backgroundColor: "white",
		alignSelf: "stretch",
		height: 1,
		marginTop: 7,
	},
	total6000Text: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		letterSpacing: 0.25,
		marginLeft: 24,
	},
	group5View: {
		backgroundColor: "rgb(19, 20, 22)",
		borderRadius: 30,
		height: 60,
		marginLeft: 33,
		marginRight: 32,
		marginTop: 50,
		justifyContent: "center",
		alignItems: "center",
	},
	completeOrderText: {
		backgroundColor: "transparent",
		color: "white",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		letterSpacing: 0.94,
	},




	cartView: {
		// backgroundColor: "red",
		// flex: 1,
	},
	summaryView:{
		marginLeft: Dimensions.get("window").width/5,
		marginRight: Dimensions.get("window").width/5,
		// width: Dimensions.get("window").width,
		flexDirection: "row",
		// justifyContent : "space-around",
		// alignItems: "center"
	},
	cartlineitemView: {
		backgroundColor: "transparent",
		// position: "absolute",
		// left: 0,
		// right: 0,
		// top: 0,
		// height: 90,
		flexDirection: "row",
		alignItems: "flex-start",
	},
	imageImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		alignSelf: "center",
		width: 90,
		height: 90,
	},
	productnameText: {
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		lineHeight: 20,
		backgroundColor: "transparent",
		alignSelf: "flex-start",
		marginLeft: 15,
		marginTop: 6,
		flexWrap: "wrap"
	},
	conditionView: {
		flex: 1,
		shadowColor: "rgba(0, 0, 0, 0.1)",
		shadowRadius: 4,
		shadowOpacity: 1,
		alignSelf: "flex-start",
		// justifyContent: "flex-end",
		marginLeft: 10,
		marginTop: 5,
		// width: 66,
		height: 22,

	},
	conditionbgImage: {
		resizeMode: "contain",
		backgroundColor: "transparent",
		// width: null,
		height: 22,
		justifyContent: "flex-end",
		width: "100%",
	},
	conditionText: {
		// flex: 1,	
		position: "absolute",
		marginTop: 5,
		color: "rgb(6, 6, 6)",
		fontSize: 9,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "right",
		lineHeight: 12,
		letterSpacing: 0.15,
		// backgroundColor: "pink",
		marginLeft: 11,
		marginRight: 12,
		width: "80%",
		flexWrap: "wrap"
	},
	quantitypriceText: {
		// flex: 1,
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "right",
		lineHeight: 20,
		justifyContent: "flex-end",
	},
	productpriceText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
	},
	quantityText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
		marginLeft: 1,
		marginTop: 3,
	},
	conditionTwoText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
		backgroundColor: "transparent",
		marginLeft: 1,
		marginTop: 3,
	},
	saveForLaterButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	saveForLaterButton: {
		backgroundColor: "transparent",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		width: 69,
		height: 15,
		marginRight: 54,
	},
	saveForLaterButtonText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
	},
	removeButton: {
		backgroundColor: "transparent",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		width: 42,
		height: 15,
	},
	removeButtonText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
	},
	removeButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	moreOptionsStack: {
		marginTop: 54,
	},
	moreOptionsView: {
		height: 16,
		marginLeft: 22,
		marginRight: 23,
		marginTop: 14,
		flexDirection: "row",
		justifyContent: "space-between"
		// alignItems: "flex-start",
	},
	moreOptionsText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		letterSpacing: 0.28,
	},
	iconGroupImage: {
		// resizeMode: "center",
		// flex: 1,
		justifyContent: "flex-end",
		height: 13,
		// marginLeft: 239,
		marginTop: 1,
		
	},
	paymentoptionsView: {
		backgroundColor: "transparent",
		height: 17,
		marginLeft: 22,
		marginRight: 23,
		marginTop: 13,
		flexDirection: "row",
		alignItems: "flex-start",
	},
	paymentOptionsText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		letterSpacing: 0.28,
		alignSelf: "center",
	},
	carttotalView: {
		// height: 71,
		// marginLeft: 32,
		// marginRight: 21,
		marginTop: 89,
		textAlign: "left"
		// alignItems: "flex-start",
	},
	summaryTextRight: {
		flex: 1,
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Roman",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "right",
		letterSpacing: 0.25,
		marginLeft: 50,
	},
	summaryTextLeft: {
		flex: 1,
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Roman",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
		// marginLeft: 24,
	},
	shippingText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Roman",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
		backgroundColor: "transparent",
		marginLeft: 24,
		marginTop: 8,
	},
	dividerView: {
		backgroundColor: "white",
		alignSelf: "stretch",
		height: 1,
		marginTop: 7,
	},
	totalText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		letterSpacing: 0.25,
		marginLeft: 24,
	},
	checkoutbuttonButton: {
		backgroundColor: "rgb(19, 20, 22)",
		borderRadius: 30,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		height: 60,
		marginLeft: 33,
		marginRight: 32,
		marginTop: 40,
		marginBottom: 40,
	},
	checkoutbuttonButtonText: {
		color: "white",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
	},
	checkoutbuttonButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
});

export default CartScreen;
