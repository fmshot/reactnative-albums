//
//  AddACar
//  Project
//
//  Created by girlProg.
//  Copyright © 2018 YediTech. All rights reserved.
//

import { Image, Text, View, StyleSheet, TouchableOpacity } from "react-native"
import React from "react"
import carSelectNavOpts from "../components/CarSelectNavOpts"


const AddCarStepOne =({navigation}) =>  {
    
    return <View style={styles.addACarView}>
				{/* <View  style={{
						height: 22,
						marginLeft: 30,
						marginRight: 155,
						marginTop: 43,
						flexDirection: "row",
						alignItems: "flex-start",
					}}>
					<Text style={styles.cancelText}>Cancel</Text>
					<View style={{ flex: 1, }}/>
					<Text style={styles.addCarText}>Add Car</Text>
				</View> */}
				<View pointerEvents="box-none"
					style={{
						height: 301,
						marginLeft: 30,
						marginRight: 26,
						marginTop: 30,
					}}>
					<View style={styles.rectangleView}/>
					<View pointerEvents="box-none"
						style={{
							position: "absolute",
							left: 0,
							right: 0,
							top: 19,
							height: 262,
							alignItems: "flex-start",
						}}>
						<TouchableOpacity onPress = {() => {navigation.navigate("AddVIN")}}>
						<View
							style={styles.group4TwoView}>
							<View
								style={styles.group3TwoView}>
								<View
									pointerEvents="box-none"
									style={{
										width: 19,
										height: 19,
									}}>
									<View
										pointerEvents="box-none"
										style={{
											position: "absolute",
											left: 0,
											top: 0,
											bottom: 0,
											justifyContent: "center",
										}}>
										<View
											style={styles.rectangleThreeView}/>
									</View>
									<View
										pointerEvents="box-none"
										style={{
											position: "absolute",
											left: 0,
											top: 0,
											bottom: 0,
											justifyContent: "center",
										}}>
										<Text
											style={styles.textTwoText}>1</Text>
									</View>
								</View>
								<View
									style={{
										flex: 1,
									}}/>
								<Text style={styles.addVinNumberText}>Add VIN number</Text>
							</View>
							<Image source={require("./../../assets/images/icon-4.png")} style={styles.iconTwoImage}/>
						</View>
						</TouchableOpacity>

						<View
							style={styles.lineCopyView}/>
						<View style={styles.group4View}>
							<View style={styles.group3View}>
								<View pointerEvents="box-none"
									style={{
										width: 19,
										height: 19,
									}}>
									<View pointerEvents="box-none"
										style={{
											position: "absolute",
											left: 0,
											top: 0,
											bottom: 0,
											justifyContent: "center",
										}}>
										<View
											style={styles.rectangleTwoView}/>
									</View>
									<View pointerEvents="box-none"
										style={{
											position: "absolute",
											left: 0,
											top: 0,
											bottom: 0,
											justifyContent: "center",
										}}>
										<Text style={styles.textText}>2</Text>
									</View>
								</View>
								<View style={{ flex: 1, }}/>
								<Text style={styles.selectCarMakeText}>Select car make </Text>
							</View>
							<Image
								source={require("./../../assets/images/icon-6.png")}
								style={styles.iconImage}/>
						</View>
						<View style={styles.lineView}/>
						<View style={styles.group5View}>
							<View style={styles.groupView}>
								<View pointerEvents="box-none"
									style={{
										width: 19,
										height: 19,
									}}>
									<View pointerEvents="box-none"
										style={{
											position: "absolute",
											left: 0,
											top: 0,
											bottom: 0,
											justifyContent: "center",
										}}>
										<View style={styles.rectangleCopyView}/>
									</View>
									<View pointerEvents="box-none"
										style={{
											position: "absolute",
											left: 0,
											top: 0,
											bottom: 0,
											justifyContent: "center",
										}}>
										<Text style={styles.textFiveText}>3</Text>
									</View>
								</View>
								<View style={{ flex: 1, }}/>
								<Text style={styles.selectModelText}>Select model </Text>
							</View>
							<Image
								source={require("./../../assets/images/icon-3.png")}
								style={styles.iconFiveImage}/>
						</View>
						<View style={styles.lineCopyTwoView}/>
						<View style={styles.group6View}>
							<View style={styles.group2View}>
								<View pointerEvents="box-none"
									style={{
										width: 19,
										height: 19,
									}}>
									<View
										pointerEvents="box-none"
										style={{
											position: "absolute",
											left: 0,
											top: 0,
											bottom: 0,
											justifyContent: "center",
										}}>
										<View
											style={styles.rectangleCopy2View}/>
									</View>
									<View
										pointerEvents="box-none"
										style={{
											position: "absolute",
											left: 0,
											top: 0,
											bottom: 0,
											justifyContent: "center",
										}}>
										<Text
											style={styles.textThreeText}>4</Text>
									</View>
								</View>
								<View
									style={{
										flex: 1,
									}}/>
								<Text
									style={styles.selectSubmodelText}>Select Submodel </Text>
							</View>
							<Image
								source={require("./../../assets/images/icon-3.png")}
								style={styles.iconThreeImage}/>
						</View>
						<View
							style={styles.lineCopyThreeView}/>
						<View
							style={styles.group6TwoView}>
							<View
								style={styles.group2TwoView}>
								<View
									pointerEvents="box-none"
									style={{
										width: 19,
										height: 19,
									}}>
									<View
										pointerEvents="box-none"
										style={{
											position: "absolute",
											left: 0,
											top: 0,
											bottom: 0,
											justifyContent: "center",
										}}>
										<View
											style={styles.rectangleCopy2TwoView}/>
									</View>
									<View
										pointerEvents="box-none"
										style={{
											position: "absolute",
											left: 0,
											top: 0,
											bottom: 0,
											justifyContent: "center",
										}}>
										<Text
											style={styles.textFourText}>5</Text>
									</View>
								</View>
								<View
									style={{
										flex: 1,
									}}/>
								<Text
									style={styles.selectEngineTypeText}>Select Engine Type </Text>
							</View>
							<Image
								source={require("./../../assets/images/icon-3.png")}
								style={styles.iconFourImage}/>
						</View>
					</View>
				</View>
			</View>
	}
	
AddCarStepOne.navigationOptions = ({navigation}) => {
	// return carSelectNavOpts ('Add car', "Cancel", "Homepage", navigation)
	return {
        headerStyle: { shadowColor: 'transparent', borderBottomWidth: 0, }, //removes visibility of default header
		headerTitle: <Text style={styles.addCarText}>Add Car</Text>,
		headerLeft: <TouchableOpacity onPress={()=>{navigation.navigate("Homepage")}}>
			<Text style={styles.cancelText}>Cancel</Text>
		</TouchableOpacity>
	};
  };


export default AddCarStepOne

const styles = StyleSheet.create({
	addACarView: {
		backgroundColor: "white",
		flex: 1,
	},
	cancelText: {
		marginLeft: 30,
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Roman",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		backgroundColor: "transparent",
	},
	addCarText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
	},
	rectangleView: {
		backgroundColor: "white",
		borderRadius: 14,
		position: "absolute",
		alignSelf: "center",
		width: 315,
		top: 0,
		height: 301,
	},
	group4TwoView: {
		backgroundColor: "transparent",
		alignSelf: "stretch",
		height: 22,
		marginLeft: 13,
		marginRight: 26,
		flexDirection: "row",
		alignItems: "flex-start",
	},
	group3TwoView: {
		backgroundColor: "transparent",
		alignSelf: "center",
		width: 152,
		height: 22,
		flexDirection: "row",
		alignItems: "center",
	},
	rectangleThreeView: {
		backgroundColor: "rgb(245, 245, 245)",
		borderRadius: 5,
		width: 19,
		height: 19,
	},
	textTwoText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Roman",
		fontSize: 10,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		backgroundColor: "transparent",
		marginLeft: 7,
	},
	addVinNumberText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
	},
	iconTwoImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		flex: 1,
		height: 11,
		marginLeft: 120,
		marginTop: 8,
	},
	lineCopyView: {
		backgroundColor: "white",
		alignSelf: "flex-end",
		width: 317,
		height: 2,
		marginTop: 18,
	},
	group4View: {
		backgroundColor: "transparent",
		width: 281,
		height: 22,
		marginLeft: 13,
		marginTop: 18,
		flexDirection: "row",
		alignItems: "flex-start",
	},
	group3View: {
		backgroundColor: "transparent",
		alignSelf: "center",
		width: 142,
		height: 22,
		flexDirection: "row",
		alignItems: "center",
	},
	rectangleTwoView: {
		backgroundColor: "rgb(245, 245, 245)",
		borderRadius: 5,
		width: 19,
		height: 19,
	},
	textText: {
		color: "rgb(224, 222, 225)",
		fontFamily: "Avenir-Roman",
		fontSize: 10,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		backgroundColor: "transparent",
		marginLeft: 7,
	},
	selectCarMakeText: {
		color: "rgb(224, 222, 225)",
		fontFamily: "Avenir-Roman",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		backgroundColor: "transparent",
	},
	iconImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		flex: 1,
		height: 11,
		marginLeft: 130,
		marginTop: 8,
	},
	lineView: {
		backgroundColor: "white",
		alignSelf: "center",
		width: 316,
		height: 2,
		marginTop: 18,
	},
	group5View: {
		backgroundColor: "transparent",
		width: 281,
		height: 22,
		marginLeft: 13,
		marginTop: 18,
		flexDirection: "row",
		alignItems: "center",
	},
	groupView: {
		backgroundColor: "transparent",
		width: 124,
		height: 22,
		flexDirection: "row",
		alignItems: "center",
	},
	rectangleCopyView: {
		backgroundColor: "rgb(245, 245, 245)",
		borderRadius: 5,
		width: 19,
		height: 19,
	},
	textFiveText: {
		backgroundColor: "transparent",
		color: "rgb(224, 222, 225)",
		fontFamily: "Avenir-Roman",
		fontSize: 10,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		marginLeft: 7,
	},
	selectModelText: {
		backgroundColor: "transparent",
		color: "rgb(224, 222, 225)",
		fontFamily: "Avenir-Roman",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
	},
	iconFiveImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		flex: 1,
		alignSelf: "flex-start",
		height: 11,
		marginLeft: 148,
		marginTop: 8,
	},
	lineCopyTwoView: {
		backgroundColor: "white",
		alignSelf: "flex-end",
		width: 317,
		height: 2,
		marginTop: 18,
	},
	group6View: {
		backgroundColor: "transparent",
		width: 281,
		height: 22,
		marginLeft: 13,
		marginTop: 18,
		flexDirection: "row",
		alignItems: "center",
	},
	group2View: {
		backgroundColor: "transparent",
		width: 150,
		height: 22,
		flexDirection: "row",
		alignItems: "center",
	},
	rectangleCopy2View: {
		backgroundColor: "rgb(245, 245, 245)",
		borderRadius: 5,
		width: 19,
		height: 19,
	},
	textThreeText: {
		color: "rgb(224, 222, 225)",
		fontFamily: "Avenir-Roman",
		fontSize: 10,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		backgroundColor: "transparent",
		marginLeft: 7,
	},
	selectSubmodelText: {
		backgroundColor: "transparent",
		color: "rgb(224, 222, 225)",
		fontFamily: "Avenir-Roman",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
	},
	iconThreeImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		flex: 1,
		height: 11,
		marginLeft: 122,
	},
	lineCopyThreeView: {
		backgroundColor: "white",
		alignSelf: "flex-end",
		width: 317,
		height: 2,
		marginTop: 18,
	},
	group6TwoView: {
		backgroundColor: "transparent",
		width: 281,
		height: 22,
		marginLeft: 13,
		marginTop: 18,
		flexDirection: "row",
		alignItems: "center",
	},
	group2TwoView: {
		backgroundColor: "transparent",
		width: 164,
		height: 22,
		flexDirection: "row",
		alignItems: "center",
	},
	rectangleCopy2TwoView: {
		backgroundColor: "rgb(245, 245, 245)",
		borderRadius: 5,
		width: 19,
		height: 19,
	},
	textFourText: {
		backgroundColor: "transparent",
		color: "rgb(224, 222, 225)",
		fontFamily: "Avenir-Roman",
		fontSize: 10,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		marginLeft: 7,
	},
	selectEngineTypeText: {
		backgroundColor: "transparent",
		color: "rgb(224, 222, 225)",
		fontFamily: "Avenir-Roman",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
	},
	iconFourImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		flex: 1,
		height: 11,
		marginLeft: 108,
	},
})
