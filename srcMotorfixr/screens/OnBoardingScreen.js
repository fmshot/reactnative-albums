import React, { useState} from 'react';
import { View, StyleSheet, Text, Image , TouchableOpacity, ScrollView, Dimensions } from 'react-native';
import { NavigationEvents, Header } from 'react-navigation';


const OnBoardingScreen = ({navigation}) => {
    const [activatePaginator, setPaginationActivator] = useState(false)
//   const { state, signin, clearErrorMessage } = useContext(Context);

  return <View style={styles.onboarding2View}>
				<View style={{ height: Dimensions.get('window').height/2}}>
					<Image
						source={require("./../../assets/images/photo-1487101588220-01e039029925-2.png")}
						style={styles.photo148710158822001e039029925Image}/>
					<View style={styles.rectangleView}/>
					{/* <View style={styles.rectangleView}> */}
					{/* <View style={{
							position: "absolute",
							right: 70,
							width: 247,
							top: 30,
							height: 234,
                            alignItems: "center",
                            backgroundColor: "red"
						}}> */}
						<TouchableOpacity
							onPress={()=>{navigation.navigate("Homepage")}}
							style={styles.skipButton}>
							<Text style={styles.skipButtonText}>Skip</Text>
						</TouchableOpacity>
						<View style={styles.logoView}>
							{/* <View style={styles.group6View}> */}
								<Image
									source={require("./../../assets/images/logo.png")}
									style={styles.logoImage}/>
								<View style={{ flex: 1, }}/>
								{/* <Text style={styles.motorfixrText}>MOTORFIXR</Text> */}
							{/* </View> */}
							{/* <Text style={styles.fixingAllYourCarText}>Fixing all your car needs</Text> */}
						</View>
					{/* </View> */}
				</View>
                <ScrollView style={{marginTop: 30}}
                    onScrollEndDrag={()=>{setPaginationActivator(!activatePaginator)}}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    pagingEnabled={true}>
                        <View style={{ width: Dimensions.get('window').width, height: "100%"}}>
                            <Text style={styles.strandedWithABrokText}>Stranded with a broken down car?</Text>
                            <Text style={styles.connectWithExpertText}>Connect with expert mechanics near you</Text>
                        </View>
                        <View style={{width: Dimensions.get('window').width, height: "100%"}}>
                            <Text style={styles.strandedWithABrokText}>Looking for particular spare part?</Text>
                            <Text style={styles.connectWithExpertText}>Shop for original and verified spare parts and get it delivered straight to your door</Text>
				        </View>
                </ScrollView>
                <View style={styles.paginatorBox}>
                    <View style={!activatePaginator ? styles.activePaginator : styles.inactivePaginator}></View>
                    <View style={ activatePaginator ? styles.activePaginator : styles.inactivePaginator}></View>
                </View>
                
				{/* <Image
					source={require("./../../assets/images/pagination-2.png")}
					style={styles.paginationImage}/> */}
				<View
					style={{
						flex: 1,
					}}/>
				<TouchableOpacity
					onPress={() => navigation.navigate('PickAService')}
					style={styles.buttonButton}>
					<Text style={styles.buttonButtonText}>Get Started</Text>
				</TouchableOpacity>
			</View>
};


const ImageHeader = props => (
    <View >
      {/* <Image
        style={StyleSheet.absoluteFill}
        source={{ uri: 'https://upload.wikimedia.org/wikipedia/commons/3/36/Hopetoun_falls.jpg' }}
      /> */}
      {/* <Header style={{ backgroundColor: 'transparent' }}/> */}
    </View>
  )

OnBoardingScreen.navigationOptions=({navigation}) => {
    return {
        header: (props) => <ImageHeader />,
        // headerStyle: { shadowColor: 'transparent', borderBottomWidth: 0, backgroundColor: "#131416" }, // 131416 removes visibility of default header
	  	// headerRight: <TouchableOpacity onPress={()=>{navigation.navigate("Homepage")}}>
		// 				<Text style={styles.skipButtonText}>Skip</Text>
		// 			</TouchableOpacity> 
    }
}

const styles = StyleSheet.create({
    paginatorBox: {
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center",
        marginTop: 20,
        marginBottom: 20,


    },
    inactivePaginator: {
        marginRight: 5,
        marginLeft: 5,
        width: Dimensions.get('window').width/45,
        height: Dimensions.get('window').width/45,
        opacity: 0.2,
        backgroundColor: "#b5bbdf",
        borderRadius: 30,
        
    },
    activePaginator: {
        marginRight: 5,
        marginLeft: 5,
        borderRadius: 30,
        width: Dimensions.get('window').width/45,
        height: Dimensions.get('window').width/45,
        backgroundColor: "#000000",
        alignSelf: "center",
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    // marginBottom: 250
  },
  onboarding2View: {
    backgroundColor: "white",
    flex: 1,
},
photo148710158822001e039029925Image: {
    // resizeMode: "cover",
    backgroundColor: "transparent",
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    width: Dimensions.get('window').width,
    alignSelf: "stretch",
    height: Dimensions.get('window').height/2,
    marginRight: 75,
},
rectangleView: {
    backgroundColor: "rgb(19, 20, 22)",
    opacity: 0.9,
    position: "absolute",
    // left: 38,
    // right: 40,
    // top: 114,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height/2,
},
skipButtonImage: {
    resizeMode: "contain",
    marginRight: 10,
},
skipButtonText: {
    color: "white",
    fontSize: 16,
    fontStyle: "normal",
    fontWeight: "bold",
    marginTop: 50,
    marginRight: 20,
    // textAlign: "left",
},
skipButton: {
    backgroundColor: "transparent",
    // flexDirection: "row",
    // alignItems: "center",
    // justifyContent: "center",
    padding: 0,
    alignSelf: "flex-end",
    // width: 33,
    // height: 22,
},
logoView: {
    alignSelf: "center",
    backgroundColor: "transparent",
    width: 200,
    height: 206,
    marginTop: Dimensions.get('window').width/5,
},
group6View: {
    backgroundColor: "transparent",
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    height: 90,
    alignItems: "center",
},
logoImage: {
    resizeMode: "contain",
    backgroundColor: "transparent",
    width: null,
    height: Dimensions.get('window').width/3,
    // marginLeft: 74,
    // marginRight: 67,
},
// motorfixrText: {
//     color: "white",
//     // fontFamily: ".SFNSDisplay",
//     fontSize: 24,
//     fontStyle: "normal",
//     fontWeight: "normal",
//     textAlign: "center",
//     letterSpacing: 4.5,
//     backgroundColor: "transparent",
//     marginLeft: 10,
//     marginBottom: 1,
// },
// fixingAllYourCarText: {
//     backgroundColor: "transparent",
//     color: "white",
//     fontSize: 12,
//     fontStyle: "normal",
//     fontWeight: "normal",
//     textAlign: "left",
//     lineHeight: 16,
//     paddingTop: 1,
//     position: "absolute",
//     alignSelf: "center",
//     bottom: 0,
// },
strandedWithABrokText: {
    // backgroundColor: "transparent",
    // color: "rgb(74, 74, 74)",
    // fontSize: 28,
    // fontStyle: "normal",
    // fontWeight: "bold",
    // textAlign: "left",
    // // lineHeight: 35,
    // // paddingTop: 4,
    // alignSelf: "center",
    // width: 315,
    // // marginTop: 35,

    width: Dimensions.get('window').width - 60,
    marginLeft: 57,
    marginRight: 27,
    height: 70,
    fontFamily: "Avenir",
    fontSize: Dimensions.get('window').width/15,
    fontWeight: "900",
    fontStyle: "normal",
    lineHeight: 35,
    letterSpacing: 0,
    color: "#4a4a4a",
    alignSelf: "center",
    textAlign: "left",
    flexWrap: 'wrap',
    

},
connectWithExpertText: {
    // backgroundColor: "red",
    // color: "rgb(6, 6, 6)",
    // fontSize: 14,
    // fontStyle: "normal",
    // fontWeight: "normal",
    // textAlign: "center",
    // letterSpacing: 0.2,
    // marginLeft: 31,
    // marginRight: 31,
    width: Dimensions.get('window').width,
    // marginLeft: 57,
    // marginRight: 57,
    width: 291,
    height: 88,
    flexWrap: 'wrap',
    fontFamily: "Avenir",
    fontSize: Dimensions.get('window').width/25,
    fontWeight: "normal",
    fontStyle: "normal",
    letterSpacing: 0.2,
    color: "#060606",
    marginLeft: 45,

    // marginTop:  8,
},
paginationImage: {
    resizeMode: "center",
    backgroundColor: "transparent",
    width: null,
    height: 8,
    marginLeft: 180,
    marginRight: 177,
    marginTop: 45,
},
buttonButtonText: {
    color: "white",
    fontSize: 14,
    fontStyle: "normal",
    fontWeight: "bold",
    textAlign: "center",
},
buttonButton: {
    backgroundColor: "rgb(19, 20, 22)",
    borderRadius: 30,
    shadowColor: "rgba(0, 0, 0, 0.08)",
    shadowRadius: 8,
    shadowOpacity: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    padding: 0,
    height: 60,
    marginLeft: 24,
    marginRight: 24,
    marginBottom: 30,
},
buttonButtonImage: {
    resizeMode: "contain",
    marginRight: 10,
},
});

export default OnBoardingScreen;
