//
//  CreateUsername
//  Project
//
//  Created by girlProg.
//  Copyright © 2019 YediTech. All rights reserved.
//

import { TextInput, View, Image, StyleSheet, Dimensions } from "react-native"
import React from "react"
import MotorFixrButton from "../components/MotorFixrButton"
import FixrInput from "../components/FixrInput"
import LogoTitle from "../components/Logotitle"


const EnterVerificationCodeScreen = ({navigation}) => {
    return <View style={styles.container}>
            <FixrInput label='Enter verification code' placeholder="Verification code" />
        	<MotorFixrButton navigation={navigation} text="Continue" destination="AccountVerified"/>
        </View>
}

export default EnterVerificationCodeScreen

EnterVerificationCodeScreen.navigationOptions = {
    headerTitle: <LogoTitle />,
    headerTintColor: 'white',
    headerStyle: {
      backgroundColor: '#131416',
      height : 120,
    }
}

const styles = StyleSheet.create({
	container: {
		width: Dimensions.get("window").width,
		height: 264,
		flex:1,
		justifyContent: "center",
	  },
})
