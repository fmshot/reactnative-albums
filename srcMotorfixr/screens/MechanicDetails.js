//
//  MechanicDetails
//  Project
//
//  Created by girlProg.
//  Copyright © 2019 YediTech. All rights reserved.
//

import { Image, StyleSheet, View, TouchableOpacity, Text, ScrollView, Linking, Dimensions } from "react-native"
import React from "react"
import LogoTitle from './../components/Logotitle'
import HeaderwithBG from './../components/HeaderwithBG'


const MechanicDetails = () => {

    return  <View
            style={styles.mechanicDetailsView}>
            <View
                style={styles.bgView}/>
            <View
                pointerEvents="box-none"
                style={{
                    // position: "absolute",
                    // left: 0,
                    // right: 0,
                    // top: 0,
                    // height: 679,
                }}>
                
                <View
                    pointerEvents="box-none"
                    style={{
                        alignSelf: "flex-start",
                        width: 267,
                        height: 70,
                        marginLeft: 24,
                        marginTop: 20,
                        flexDirection: "row",
                        alignItems: "flex-start",
                    }}>
                    <View style={styles.shopavatarView}>
                        <Image source={require("./../../assets/images/icon.png")} style={styles.iconImage}/>
                    </View>
                    <View style={styles.infoAndRatingView}>
                        <Text style={styles.placeNameText}>Chucka Motors</Text>
                        <Text style={styles.addressText}>Gudu Mechanic Village</Text>
                            <View style={styles.distanceView}>
                                <Image source={require("./../../assets/images/icon---direction.png")} style={styles.iconDirectionImage}/>
                                <Text style={styles.distanceText}>200 </Text>
                        </View>
                    </View>
                </View>
                <View style={styles.dividerView}/>

                <ScrollView contentContainerStyle={{height: Dimensions.get("window").height}}>
                <View style={styles.detailsView}>
                    <View style={styles.operationView}>
                        <Text style={styles.subtitleText}>OPERATION</Text>
                        <Text style={styles.timeText}>Open • 9am - 6pm</Text>
                    </View>
                    <View >
                        <TouchableOpacity style={styles.addressView}>
                            <Text style={styles.subtitleTwoText}>ADDRESS</Text>
                            <Text style={styles.addressTwoText}>341 Alexzander Viaduct Apt. 682</Text>
                            <Text style={styles.addressThreeText}>005-951-3317</Text>
                            <Image source={require("./../../assets/images/maps.png")} style={styles.mapsImage}/>
                            <Text style={styles.linkText}>Directions</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                {/* <ScrollView> */}
                    <View style={styles.ratingsView}>
                        <Text style={styles.ratingsText}>Ratings</Text>
                        <Image source={require("./../../assets/images/group-3.png")} style={styles.group3Image}/>
                    </View>
                    <Text style={styles.shopsalespitchText}>If you are looking for a new way to promote your business that won’t cost you more money, maybe printing is one of the options you won’t resist. </Text>
                {/* </ScrollView> */}
                <TouchableOpacity
                    // onPress={()=>{Linking.openURL(`tel:${phoneNumber}`)}}
                    onPress={()=>{Linking.openURL(`tel:08033638686`)}}
                    style={styles.contactbuttonButton}>
                    <Text style={styles.contactbuttonButtonText}>Contact Chucka Motors</Text>
                </TouchableOpacity>
                </ScrollView>
            </View>
           
        </View>
}

export default MechanicDetails

MechanicDetails.navigationOptions ={
    headerTitle: <HeaderwithBG />,
    // title: "chucks",
    headerTintColor: 'white',
    headerStyle: {
      backgroundColor: '#131416',
      height : 100,
    }
}



const styles = StyleSheet.create({
	mechanicDetailsView: {
		backgroundColor: "white",
		flex: 1,
	},
	bgView: {
		backgroundColor: "rgb(253, 254, 255)",
		position: "absolute",
		left: 0,
		right: 0,
		top: -40,
		height: 1149,
	},
	headerBgView: {
		backgroundColor: "transparent",
		height: 116,
	},
	headerBgImage: {
		resizeMode: "cover",
		backgroundColor: "transparent",
		width: null,
		height: 116,
	},
	chuckaMotorsText: {
		color: "rgb(240, 232, 229)",
		fontFamily: "Avenir-Heavy",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		letterSpacing: 0.37,
		backgroundColor: "transparent",
		position: "absolute",
		alignSelf: "center",
		top: 64,
	},
	shopavatarView: {
		backgroundColor: "rgb(178, 178, 178)",
		borderRadius: 26,
		width: 52,
		height: 52,
		justifyContent: "center",
	},
	iconImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: null,
		height: 34,
		marginLeft: 9,
		marginRight: 9,
	},
	infoAndRatingView: {
		backgroundColor: "transparent",
		width: 195,
		height: 70,
        marginLeft: 20,
        flexDirection: "column",
	},
	placeNameText: {
		backgroundColor: "transparent",
		color: "rgb(81, 81, 81)",
		fontFamily: "Avenir-Book",
		fontSize: 22,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.2,
	},
	distanceView: {
		backgroundColor: "transparent",
		flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-start"
	},
	iconDirectionImage: {
		backgroundColor: "transparent",
        resizeMode: "center",
        justifyContent: "flex-start",
	},
	distanceText: {
		backgroundColor: "transparent",
		color: "rgb(81, 81, 81)",
		fontFamily: "Avenir-Book",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		lineHeight: 18,
		letterSpacing: 0.2,
	},
	addressText: {
		color: "rgb(74, 74, 74)",
		fontFamily: "Avenir-Book",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		lineHeight: 18,
		letterSpacing: 0.2,
		backgroundColor: "transparent",
		opacity: 0.5,
	},
	dividerView: {
		backgroundColor: "rgb(10, 36, 99)",
		opacity: 0.1,
		height: 1,
		marginTop: 22,
	},
	detailsView: {
		backgroundColor: "transparent",
		height: 149,
		marginLeft: 24,
		marginRight: 36,
		marginTop: 33,
	},
	operationView: {
		backgroundColor: "transparent",
		position: "absolute",
		left: 0,
		width: 127,
		bottom: 0,
		height: 42,
		alignItems: "flex-start",
	},
	subtitleText: {
		backgroundColor: "transparent",
		color: "rgb(81, 81, 81)",
		fontFamily: "Avenir-Black",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		letterSpacing: 1,
	},
	timeText: {
		backgroundColor: "transparent",
		color: "rgb(178, 178, 178)",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		lineHeight: 22,
		letterSpacing: 0.2,
		alignSelf: "stretch",
		marginRight: 2,
		marginTop: 3,
	},
	addressView: {
		backgroundColor: "transparent",
		height: 149,
		alignItems: "flex-start",
	},
	subtitleTwoText: {
		backgroundColor: "transparent",
		opacity: 0.5,
		color: "rgb(81, 81, 81)",
		fontFamily: "Avenir-Book",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 1,
	},
	addressTwoText: {
		color: "rgb(81, 81, 81)",
		fontFamily: "Avenir-Book",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		lineHeight: 22,
		letterSpacing: 0.2,
		backgroundColor: "transparent",
		marginTop: 14,
	},
	addressThreeText: {
		color: "rgb(81, 81, 81)",
		fontFamily: "Avenir-Book",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		lineHeight: 22,
		letterSpacing: 0.2,
		backgroundColor: "transparent",
	},
	mapsImage: {
		backgroundColor: "transparent",
		shadowColor: "rgba(10, 36, 99, 0.13)",
		shadowRadius: 4,
		shadowOpacity: 1,
		resizeMode: "center",
		alignSelf: "flex-end",
		width: 48,
		height: 48,
		marginTop: 1,
	},
	linkText: {
		backgroundColor: "transparent",
		color: "rgb(81, 81, 81)",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		lineHeight: 18,
		letterSpacing: 0.2,
		paddingTop: 2,
		alignSelf: "flex-end",
	},
	ratingsView: {
		backgroundColor: "transparent",
		alignSelf: "flex-start",
		width: 120,
		height: 51,
		marginLeft: 24,
		marginTop: 42,
	},
	ratingsText: {
		backgroundColor: "transparent",
		color: "rgb(50, 54, 67)",
		fontFamily: "Avenir-Book",
		fontSize: 18,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		alignSelf: "flex-start",
	},
	group3Image: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: null,
		height: 20,
	},
	shopsalespitchText: {
		backgroundColor: "transparent",
		color: "rgb(50, 54, 67)",
		fontFamily: "Avenir-Book",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		marginLeft: 24,
		marginRight: 36,
	},
	contactbuttonButtonText: {
		color: "rgb(251, 251, 251)",
		fontFamily: "Avenir-Medium",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
        textAlign: "left",
        
	},
	contactbuttonButton: {
		backgroundColor: "rgb(23, 24, 26)",
		borderRadius: 30,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		height: 60,
		marginLeft: 24,
        marginRight: 36,
        marginTop: 28,
	},
	contactbuttonButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	backIconImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		position: "absolute",
		left: 25,
		width: 22,
		top: 67,
		height: 16,
	},
})
