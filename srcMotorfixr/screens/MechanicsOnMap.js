//
//  Map
//  Project
//
//  Created by girlProg.
//  Copyright © 2018 YediTech. All rights reserved.
//

import { View, StyleSheet, Text, Image, Dimensions } from "react-native"
import React from "react"
import CustomSearchbar from "./../components/CustomSearchbar"


const  MechanicsOnMap = () => {

	
		return <View style={styles.mapView}>
				<View style={styles.bgView}/>
                
				<View pointerEvents="box-none"
					style={{
						position: "absolute",
						left: 0,
						right: 0,
						top: 0,
						bottom: 0,
						justifyContent: "center",
					}}>
					<Image
						source={require("./../../assets/images/8-misc---map---map.png")}
						style={styles.miscMapMapImage}/>
                        	
				</View>
				<View
					pointerEvents="box-none"
					style={{
						position: "absolute",
						left: 32,
						right: 32,
						top: 64,
						height: 636,
					}}>
                    <CustomSearchbar placeholder="Search"  showHistory={false}/>
					
					<View style={styles.nearbyParkingView}>
						
						<View
							pointerEvents="box-none"
							style={{
								// position: "absolute",
								// left: 20,
								// right: 24,
								// top: 23,
								// height: 121,
                                // alignItems: "flex-start",
                                backgroundColor: "pink",
							}}>
                            <View style={styles.nearestMechanicView}>
                                <Text style={styles.nearestMechanicText}>Nearest Mechanic</Text>
                            </View>
							
							<View style={styles.mechanictextdetailsView}>
								<Text style={styles.machanicnameText}>Chucka Motors</Text>
								<View
									pointerEvents="box-none"
									style={{
										alignSelf: "stretch",
										height: 17,
										marginTop: 4,
										flexDirection: "row",
										alignItems: "flex-start",
									}}>
									<Text style={styles.statusText}>4.6 </Text>
									<View
										style={{
											// flex: 1,
										}}/>
									<Text style={styles.locationText}>Gudu Mechanic Village</Text>
                                    <Text style={styles.distancefrommeText}>200</Text>
								</View>
								
							</View>
						</View>
					</View>
				</View>
			</View>
	
}

export default MechanicsOnMap

MechanicsOnMap.navigationOptions ={
    header: null
}

const styles = StyleSheet.create({
	mapView: {
		backgroundColor: "white",
		flex: 1,
	},
	bgView: {
		backgroundColor: "rgb(242, 242, 242)",
		position: "absolute",
		left: 0,
		right: 0,
		top: 24,
		height: 667,
	},
	miscMapMapImage: {
		resizeMode: "cover",
		backgroundColor: "transparent",
		width: null,
		height: 812,
	},
	
	addDestinationText: {
		color: "rgb(10, 36, 99)",
		// fontFamily: ".AppleSystemUIFont",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		lineHeight: 20,
		letterSpacing: 0.2,
		backgroundColor: "transparent",
		opacity: 0.3,
		marginLeft: 16,
	},
	nearbyParkingView: {
        borderRadius: 4,
        backgroundColor: "#fdfeff",
		height: 294,
		marginTop: 294,
	},
	containerImage: {
		backgroundColor: "transparent",
		resizeMode: "cover",
		width: null,
		height: 295,
	},
	nearestMechanicView: {
        // width: 311,
        height: 64,
        borderRadius: 4,
        backgroundColor: "#fdfeff",
        alignItems: "center",
        justifyContent: "flex-start"

    },
	nearestMechanicText: {
		// backgroundColor: "transparent",
		// color: "rgb(10, 36, 99)",
		// fontFamily: "Avenir-Book",
		// fontSize: 16,
		// fontStyle: "normal",
		// fontWeight: "normal",
		// textAlign: "left",
		// lineHeight: 20,
        // paddingTop: 2,
        // width: 129,

        height: 20,
        fontFamily: "Avenir",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: 20,
        letterSpacing: 0,
        color: "#0a2463",
        textAlign: "left",
        marginLeft: 20,
        // alignSelf: "center",
        
	},
	mechanictextdetailsView: {
		backgroundColor: "transparent",
		alignSelf: "flex-end",
		width: 174,
		height: 69,
		marginTop: 30,
		alignItems: "flex-start",
	},
	machanicnameText: {
		backgroundColor: "transparent",
		color: "rgb(74, 74, 74)",
		fontFamily: "Avenir-Book",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		lineHeight: 18,
		letterSpacing: 0.2,
		paddingTop: 2,
		alignSelf: "flex-end",
		marginRight: 76,
	},
	statusText: {
		backgroundColor: "transparent",
		color: "rgb(195, 195, 195)",
		fontFamily: "Avenir-Medium",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		marginTop: 1,
	},
	locationText: {
		backgroundColor: "transparent",
		opacity: 0.5,
		color: "rgb(34, 34, 34)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		lineHeight: 14,
		letterSpacing: 1,
		paddingTop: 2,
	},
	distancefrommeText: {
		color: "rgb(74, 74, 74)",
		fontFamily: "Avenir-Book",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.2,
		backgroundColor: "transparent",
		marginLeft: 17,
		marginTop: 12,
	},
})
