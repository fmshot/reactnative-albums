//
//  Homepage
//  Project
//
//  Created by girlProg.
//  Copyright © 2018 YediTech. All rights reserved.
//

import { View, StyleSheet, Text, TouchableOpacity, Image, FlatList, ScrollView, Dimensions } from "react-native"
import React, {useState, useContext, useEffect} from "react"
import { SwipeRow } from 'react-native-swipe-list-view';
import RecentTextPopUp from "../components/RecentTextPopUp"
import ChosenCarHeader from "../components/ChosenCarHeader"
import CustomSearchbar from "./../components/CustomSearchbar"
import  {Context} from '../context/CategoryContext'


const Homepage = ({navigation}) => {
	const [showRecents, setShowRecents] = useState(false)
	const { state,  getCategories} = useContext(Context)
	useEffect (()=>{getCategories()},[])
	
		return  <View style={{ height:Dimensions.get('window').height }}>	
				<CustomSearchbar showHistory={true}  />			
				<ScrollView contentContainerStyle={styles.homepageView}>
				<View style={styles.promotionView}>
					<View style={styles.promoView}>
						<Text style={styles.promotionText}>PROMOTION</Text>
						<Image
							source={require("./../../assets/images/promo-pic.png")}
							style={styles.promoPicImage}/>
					</View>
					<TouchableOpacity
						onPress={()=>{}}
						style={styles.closePromotionButton}>
						<Image source={require("./../../assets/images/group-2-2.png")} style={styles.closePromotionButtonImage}/>
					</TouchableOpacity>
				</View>
				{state.length > 0? 
				<View style={styles.categoriesView}>
					<Text style={styles.sparePartsText}>SPARE PARTS</Text>

					<FlatList
						data ={state}
						keyExtractor={category => category.id.toString()}
						renderItem={({item}) => {
						return (
							<TouchableOpacity
							onPress={()=>{navigation.navigate("CategoryProducts", {screenName: item.name, chosenCategory: item.id})}}
							style={styles.singleCategoryButton}>
							{/* <Image source={require("./../../assets/images/bitmap-8.png")} */}
							<Image source={{uri: item.image}}
								style={styles.singleCategoryButtonImage}/>
							<Text style={styles.categoryButtonText}>{item.name}</Text>
						</TouchableOpacity>
						
							
						);
						}}
						/>
				

					
				</View> :  null}
				</ScrollView>
			</View>
            
	}
  export default Homepage

  Homepage.navigationOptions =  {
	header: <ChosenCarHeader/>,
	// tabBarIcon: ({ focused,tintColor }) => (
	// 	focused ? <Image source={require('./../../assets/images/home.png')} />
	// 	  :
	// 	  <Image source={require('./../../assets/images/home.png')} />
	//   ),
	//   tabBarLabel : "monms"
	// drawerLabel: 'Notifications',
}

const styles = StyleSheet.create({
	homepageView: {
		backgroundColor: "rgb(251, 252, 254)",
		height: Dimensions.get('window').height + 150,
	},
	removeText: {
		width: 46,
		height: 16,
		fontFamily: "Avenir",
		fontSize:12,
		fontWeight: "normal",
		fontStyle: "normal",
		letterSpacing: 0.28,
		color: "black",
		marginTop: 30,
		marginRight: 10,
		// justifyContent: "flex-end",
		// flexDirection: "column",
		textAlign: "right",

	},
	removeSliderBackground: {
		height: 59,
		width: "100%",
		backgroundColor: "#131416",
		alignSelf: "flex-end",
	},
	removeSlider: {
		width: "100%", //value from how much row is swipable
		height: 57, //value from size of visible row
		alignSelf: "flex-end",
		backgroundColor: "white",
		alignItems: "flex-end"	  
	},
	headerBgView: {
		backgroundColor: "transparent",
		position: "absolute",
		left: 0,
		right: 0,
		top: 0,
		height: 116,
		justifyContent: "center",
	},
	headerBgImage: {
		resizeMode: "cover",
		backgroundColor: "transparent",
		width: null,
		height: 116,
	},
	addcarmenuView: {
		backgroundColor: "transparent",
		position: "absolute",
        left: 100,
        // width: "80%",
		right: 57,
		top: 59,
		height: 40,
		flexDirection: "row",
		alignItems: "center",
	},
	groupImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: 25,
		height: 25,
	},
	addCarText: {
		color: "white",
		fontFamily: "Avenir-Book",
		fontSize: 18,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.41,
		backgroundColor: "transparent",
		position: "absolute",
		left: 0,
		// right: 164,
		top: 0,
	},
	addCarToSeeAvailText: {
		color: "white",
		fontFamily: "Avenir-Book",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.28,
		backgroundColor: "transparent",
		position: "absolute",
		// width: "100%",
		height: 36,
		flex: 1, 
		flexWrap: 'wrap',
		left: 0,
		right: 0,
		bottom: 0,
		top:20,
	},
	viewButtonText: {
		color: "black",
		fontFamily: ".SFNSText",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
	},
	addCarButton: {
		backgroundColor: "white",
		borderRadius: 21.5,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		position: "absolute",
		left: 30,
		right: 302,
		top: 58,
		width: 43,
  		height: 43,

	},
	addCarButtonImage: {
		resizeMode: "contain",
	},
	searchbarView: {
		backgroundColor: "white",
		borderRadius: 24,
		shadowColor: "rgba(0, 0, 0, 0.08)",
		shadowRadius: 8,
		shadowOpacity: 1,
		height: 48,
		marginLeft: 24,
		marginRight: 24,
		marginTop: 15,
		marginBottom: 10,
		flexDirection: "row",
		alignItems: "flex-start",
		elevation: 2,
	},
	iconSearchImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		height: 24,
		marginLeft: 20,
		marginRight: 10,
		marginTop: 12,
	},
	placeholderTextInput: {
		backgroundColor: "transparent",
		opacity: 1,
		padding: 0,
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		position: "absolute",
		left: 0,
		right: 0,
		top: 5,
		height: 20,
	},
	indicatorTwoView: {
		// backgroundColor: "black",
		// width: 1,
		// height: 29,
	},
	promotionView: {
		backgroundColor: "transparent",
		height: 201,
		marginLeft: 29,
		marginRight: 30,
		marginTop: 27,
	},
	promoView: {
		backgroundColor: "transparent",
		position: "absolute",
		left: 0,
		right: 0,
		top: 8,
		height: 193,
		alignItems: "flex-start",
	},
	promotionText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		letterSpacing: 0.37,
		backgroundColor: "transparent",
	},
	promoPicImage: {
		backgroundColor: "transparent",
		opacity: 0.65,
		resizeMode: "cover",
		alignSelf: "stretch",
		width: '100%',
		height: 167,
		marginLeft: 1,
		marginTop: 4,
	},
	closePromotionButton: {
		backgroundColor: "transparent",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		position: "absolute",
		left: "100%",
		right: 0,
		top: 0,
		height: 27,
	},
	group2ButtonText: {
		color: "black",
		fontFamily: ".SFNSText",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
	},
	closePromotionButtonImage: {
		resizeMode: "contain",
	},
	categoriesView: {
		backgroundColor: "transparent",
		// height: 289,
		marginLeft: 30,
		marginRight: 30,
		marginTop: 23,
	},
	sparePartsText: {
		backgroundColor: "transparent",
		color: "rgb(34, 34, 34)",
		fontFamily: "Avenir-Heavy",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		letterSpacing: 0.37,
		marginRight: 100,
	},
	singleCategoryButton: {
		// backgroundColor: "transparent",
		flexDirection: "row",
		// alignItems: "center",
		justifyContent: "space-around",
		padding: 0,
        height: 107,
		shadowRadius: 20,
		// width: "100%",
		borderRadius: 15,
		backgroundColor: "#ffffff",
		shadowColor: "rgba(0, 0, 0, 0.08)",
		shadowOffset: {
			width: 0,
			height: 0
		},
		shadowRadius: 20,
		shadowOpacity: 1,
		marginBottom: 15,
		elevation: 1,
	},
	categoryButtonText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "right",
		// justifyContent: "flex-end",
		alignSelf : "center",
		// marginLeft: 30,
	},
	singleCategoryButtonImage: {
		resizeMode: "contain",
		alignSelf: 'center',
		width: 76,
		height: 63
		// marginLeft: 20,
		// marginRight: 80,
	},
	
	group2View: {
		backgroundColor: "transparent",
		position: "absolute",
		left: 20,
		right: 17,
		top: 15,
		height: 40,
		flexDirection: "row",
		alignItems: "flex-start",
	},
})
