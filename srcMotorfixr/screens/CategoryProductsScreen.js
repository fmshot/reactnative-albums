//
//  SearchResult
//  
//
//  Created by girlProg.
//  Copyright © 2018 YediTech. All rights reserved.
//

import { StyleSheet, View, Image, Text, TouchableOpacity, FlatList, ActivityIndicator, Dimensions, Picker } from "react-native"
import React ,{useEffect, useContext} from "react"
import ChosenCarHeader from "../components/ChosenCarHeader"
import BreadcrumbsNav from "../components/BreadcrumbsNav"
import ProductListItem from "../components/ProductListItem"
import {Context as ProductContext} from '../context/ProductContext'
import { WaveIndicator, } from 'react-native-indicators';
import {Context as WishListContext} from '../context/WishListContext'


const CategoryProductsScreen = ({navigation}) =>  {
	const {state, getCategoryProducts} = useContext(ProductContext)
	const categoryId = navigation.getParam("chosenCategory")
	
	

	useEffect(()=>{ getCategoryProducts(categoryId) }, [])
	
		return <View style={styles.searchResultView}>
				<BreadcrumbsNav navigation={navigation} screenName= {navigation.getParam("screenName")}/>
				{state.products  ?
				<View
					pointerEvents="box-none"
					style={{
						height: 32,
						marginLeft: 15,
						marginRight: 15,
						marginTop: 15,
						flexDirection: "row",
                        alignItems: "flex-start",
                        justifyContent: "space-evenly"
					}}>
                    <TouchableOpacity onPress={()=> {}}>
                        <View style={styles.sortbyviewView}>
                            <View style={styles.defaultAbsolutePosition}>
                                <Image source={require("./../../assets/images/-background-4.png")} style={styles.sortbyimageImage}/>
                            </View>
                            <View style={styles.defaultAbsolutePosition}>
                                <Text style={styles.sortbytextText}>SORT BY</Text>
                            </View>
                            <Image source={require("./../../assets/images/sort.png")} style={styles.sorticon}/>

                        </View>
                    </TouchableOpacity>
                    
					<View style={{ flex: 1, }}/>
                    
                    <TouchableOpacity onPress={()=>{navigation.navigate('FilterDrawer')}}>
                        <View
                            style={styles.filtersbuttonviewView}>
                            <View
                                pointerEvents="box-none"
                                style={styles.defaultAbsolutePosition}>
                                <Image source={require("./../../assets/images/-background-12.png")} style={styles.backgroundImage}/>
                            </View>
                            <View style={styles.defaultAbsolutePosition}>
                                <Text style={styles.filterstextText}>FILTERS</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
				</View>
					: <ActivityIndicator style={{ alignSelf:"center", justifyContent: "center"}} size="large" color="black" />   }

						{state.products ? 
						<FlatList data={state.products}
							keyExtractor = {(item)=>item.id.toString()}
							numColumns={2}
							onRefresh ={()=>getCategoryProducts(categoryId)}
							refreshing={false}
							renderItem={({ item }) => {

								return (
									
									<ProductListItem navigation={navigation} product={item} />
								);
								}}
						 />  : null  }
					
					
							
			</View>
    }
    
export default CategoryProductsScreen

CategoryProductsScreen.navigationOptions =  {
	header: <ChosenCarHeader/>,
	drawerLabel: 'Notifications',
}


const styles = StyleSheet.create({
	searchResultView: {
		backgroundColor: "white",
		// height: Dimensions.get("window").height*2,
		// flex: 1,
	},
	defaultAbsolutePosition: {
        position: "absolute",
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: "center",
    },
	headerBgView: {
		backgroundColor: "transparent",
		position: "absolute",
		left: 0,
		right: 0,
		top: 0,
		height: 116,
		justifyContent: "center",
	},
	headerBgImage: {
		resizeMode: "cover",
		backgroundColor: "transparent",
		width: null,
		height: 116,
	},
	addedcarviewView: {
		backgroundColor: "transparent",
		position: "absolute",
		left: 30,
		right: 176,
		top: 58,
		height: 43,
		flexDirection: "row",
		alignItems: "center",
	},
	carlogoviewView: {
		backgroundColor: "white",
		borderRadius: 21.5,
		width: 43,
		height: 43,
		alignItems: "flex-start",
	},
	bitmapImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: 30,
		height: 30,
		marginLeft: 7,
		marginTop: 9,
	},
	carnameText: {
		color: "white",
		fontFamily: "Avenir-Book",
		fontSize: 18,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.41,
		backgroundColor: "transparent",
		position: "absolute",
		right: 0,
		top: 0,
	},
	cartrimText: {
		backgroundColor: "transparent",
		color: "white",
		fontFamily: "Avenir-Book",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.28,
		position: "absolute",
		right: 40,
		bottom: 0,
	},
	sortbyviewView: {
		backgroundColor: "transparent",
		width: Dimensions.get('window').width/2.5,
		height: 32,
	},
	sorticon: {
        justifyContent: "flex-start",
        alignSelf:  "flex-end",
        // width: 15,
        // height: 10,
        marginRight:  5,
        marginTop: 5,
        
    },
	sortbyimageImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: null,
		height: 32,
	},
	sortbytextText: {
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		lineHeight: 14,
		letterSpacing: 0.4,
		paddingTop: 3,
		backgroundColor: "transparent",
		marginLeft: 15,
	},
	filtersbuttonviewView: {
		backgroundColor: "transparent",
		width: Dimensions.get('window').width/2.5,
		height: 32,
	},
	backgroundImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: null,
		height: 32,
	},
	filterstextText: {
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		lineHeight: 14,
		letterSpacing: 0.4,
		paddingTop: 3,
		backgroundColor: "transparent",
		marginLeft: 15,
	},
	productitemviewView: {
		backgroundColor: "transparent",
        height: 214,
        width: Dimensions.get('window').width/2.3,
		marginLeft: 15,
		// marginRight: 195,
		marginTop: 29,
		alignItems: "flex-start",
	},
	photoviewView: {
		backgroundColor: "rgb(242, 242, 242)",
		borderRadius: 12,
		alignSelf: "stretch",
		height: 145,
	},
	belgiumviewView: {
		backgroundColor: "transparent",
		width: 69,
		height: 28,
		marginTop: 3,
	},
	belgiumbackgroundImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		position: "absolute",
		left: 0,
		width: 69,
		top: 0,
		height: 22,
	},
	belgiumtextText: {
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Heavy",
		fontSize: 9,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		lineHeight: 12,
		letterSpacing: 0.15,
		backgroundColor: "transparent",
		position: "absolute",
		left: 4,
		width: 61,
		top: 4,
	},
	starbuttonviewView: {
		backgroundColor: "transparent",
		width: 32,
		height: 32,
	},
	roundviewImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: null,
		height: 32,
	},
	starviewImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: null,
		height: 18,
		marginLeft: 7,
		marginRight: 6,
	},
	profileImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		alignSelf: "center",
		width: 70,
		height: 70,
		marginTop: 1,
	},
	dealernameText: {
		color: "rgb(171, 171, 171)",
		fontFamily: "Avenir-Book",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		lineHeight: 16,
		paddingTop: 1,
		backgroundColor: "transparent",
		position: "absolute",
		left: 0,
		bottom: 0,
	},
	productnameText: {
		backgroundColor: "transparent",
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		lineHeight: 18,
		letterSpacing: 0.2,
		paddingTop: 2,
		position: "absolute",
		left: 0,
		bottom: 16,
	},
	priceText: {
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		lineHeight: 18,
		letterSpacing: 0.2,
		paddingTop: 2,
		backgroundColor: "transparent",
	},
})
