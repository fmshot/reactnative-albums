import { Image, StyleSheet, Text, View , Dimensions, FlatList, ActivityIndicator} from "react-native"
import React, {useContext, useEffect} from "react"
import ChosenCarHeader from "../../components/ChosenCarHeader"
import BreadcrumbsNav from "../../components/BreadcrumbsNav"
// import { FontAwesome } from '@expo/vector-icons';
import {Context as SellerContext} from '../../context/SellerContext'

const ProductReviewScreen = ({navigation}) => {

	const {state, getSellerReviews} = useContext(SellerContext)
	const sellerid = navigation.getParam("sellerid")
	const screenname = navigation.getParam("screenName")
	useEffect(()=>{ getSellerReviews(sellerid) }, [])
	
	const readableDate = (date) => {
		var options = { year: 'numeric', month: 'long',  day: 'numeric' };
		var reviewDate  = new Date(date);
		return reviewDate.toLocaleDateString("en-gb", options); 
	}
	

    return <>
        <BreadcrumbsNav navigation={navigation} screenName={screenname}/>
        {/* {state[0] ? console.log(state) : null}
		{state[0] ?  */}
		
		<FlatList
			data ={state}
			keyExtractor = {item => item.id.toString()}
			renderItem = {({item})=>{
		return <View style={styles.reviewsView}>
				<View style={styles.singlereviewView}>
					<View style={styles.userphotoView}>
						
						<Image source={require("../../../assets/images/icon.png")} style={styles.iconImage}/>
					</View>
					{/* <View style={{ flex: 1, }}/> */}
					<View style={styles.textareaView}>
						<View pointerEvents="box-none"
							style={{
								height: 30,
								marginRight: 6,
								flexDirection: "row",
								alignItems: "flex-start",
							}}>
							<View pointerEvents="box-none"
								style={{
									width: Dimensions.get('window').width/2,
									height: 30,
								}}>
								<Text style={styles.titleofreviewText}>{item.title}</Text>
								<Text style={styles.nameofreviewerText}>{item.author.user.username}</Text>
							</View>
							<View
								style={{
									// flex: 1,
								}}/>
							<Text style={styles.dateofreviewText}>{readableDate(item.created)}</Text>
						</View>
						{/* <Image source={require("../../../assets/images/stars.png")}
							style={styles.starsImage}/> */}
							<View style={{flexDirection: "row"}}>
								<FontAwesome style={{marginRight: 3}} name="star" color='#f1af09' size={13} />
								<FontAwesome style={{marginRight: 3}} name="star" color='#f1af09' size={13} />
								<FontAwesome style={{marginRight: 3}} name="star" color='#f1af09' size={13} />
							</View>
                        
						<View
							style={{
								// flex: 1,
							}}/>
						<Text style={styles.reviewtextText}>{item.text}</Text>
					</View>
				</View>
				<View style={styles.separatorView}/>
			</View> 
			}}
			/>
			
			{/* : <ActivityIndicator size="large" color="black" /> }  */}
			<View style={styles.reviewsView}>
				<View style={styles.singlereviewView}>
					<View style={styles.userphotoView}>
						
						<Image source={require("../../../assets/images/icon.png")} style={styles.iconImage}/>
					</View>
					{/* <View style={{ flex: 1, }}/> */}
					<View style={styles.textareaView}>
						<View pointerEvents="box-none"
							style={{
								height: 30,
								marginRight: 6,
								flexDirection: "row",
								alignItems: "flex-start",
							}}>
							<View pointerEvents="box-none"
								style={{
									width: Dimensions.get('window').width/2,
									height: 30,
								}}>
								<Text style={styles.titleofreviewText}>Something Nice</Text>
								<Text style={styles.nameofreviewerText}>Jodie</Text>
							</View>
							<View
								style={{
									// flex: 1,
								}}/>
							<Text style={styles.dateofreviewText}>5th June 2019</Text>
						</View>
						<Image source={require("../../../assets/images/stars.png")}
							style={styles.starsImage}/>
                        {/* <FontAwesome name="star" color='yellow' size={20} /> */}
						<View
							style={{
								// flex: 1,
							}}/>
						<Text style={styles.reviewtextText}>As you pour the first glass of your 
                        favorite Chianti or Chardonnay and settle into an intimate Friday evening,
                         you wonder about the wine’s origins. Look no further.</Text>
					</View>
				</View>
				<View style={styles.separatorView}/>
			</View> 
            </>
}

export default ProductReviewScreen

ProductReviewScreen.navigationOptions =  {
	header: <ChosenCarHeader/>,
}

const styles = StyleSheet.create({
	reviewsView: {
		backgroundColor: "white",
		flex: 1,
		alignItems: "center",
	},
	singlereviewView: {
        marginLeft: 27,
        marginRight: 32,
		backgroundColor: "transparent",
		width: Dimensions.get('window').width,
        height: 117,
		marginTop: 30,
		flexDirection: "row",
        alignItems: "flex-start",
        justifyContent: "center"
	},
	userphotoView: {
		backgroundColor: "rgb(178, 178, 178)",
		borderRadius: 26,
		width: 52,
		height: 52,
        justifyContent: "center",
        alignSelf: "center",
        marginRight: 15
	},
	iconImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: null,
		height: 34,
		marginLeft: 9,
		marginRight: 9,
	},
	textareaView: {
		backgroundColor: "transparent",
		alignSelf: "center",
		width: Dimensions.get('window').width/1.4,
		height: 92,
	},
	titleofreviewText: {
		backgroundColor: "transparent",
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Book",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.2,
		position: "absolute",
		left: 0,
		top: 0,
	},
	nameofreviewerText: {
		backgroundColor: "transparent",
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Book",
		fontSize: 9,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.13,
		position: "absolute",
		left: 0,
		top: 18,
	},
	dateofreviewText: {
		backgroundColor: "transparent",
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Book",
		fontSize: 9,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "right",
		letterSpacing: 0.13,
		marginTop: 18,
	},
	starsImage: {
		resizeMode: "center",
        backgroundColor: "transparent",
        justifyContent: "flex-start",
		// width: null,
		height: 11,
		// marginRight: 183,
		marginTop: 5,
	},
	reviewtextText: {
		backgroundColor: "transparent",
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Book",
		fontSize: 9,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
        letterSpacing: 0.13,
        // height: 300,
        flexWrap: "wrap",
	},
	separatorView: {
		backgroundColor: "white",
		alignSelf: "stretch",
		height: 1,
		marginTop: 20,
	},
})