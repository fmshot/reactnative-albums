import React from 'react';
import { StyleSheet, View, Image, Text, TextInput, Dimensions } from 'react-native';
import LogoTitle from "../components/Logotitle"
import MotorFixrButton from "../components/MotorFixrButton"
import FixrInput from "../components/FixrInput"


const CreateAProfileScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
        <FixrInput label='Create a profile' placeholder="First Name" />
        <FixrInput label={null} placeholder="Last Name" />
        <MotorFixrButton navigation={navigation} text="Continue" destination="BusinessInformation"/>
    </View>
);
}

CreateAProfileScreen.navigationOptions = {
    headerTitle: <LogoTitle />,
    headerTintColor: 'white',
    headerStyle: {
      backgroundColor: '#131416',
      height : 120,
    }
}

const styles = StyleSheet.create({
container: {
  width: Dimensions.get("window").width,
  height: 264,
  flex:1,
  justifyContent: "center",
},

});


export default CreateAProfileScreen;

