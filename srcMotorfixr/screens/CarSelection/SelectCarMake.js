//
//  SelectCarMake
//  Project
//
//  Created by girlProg.
//  Copyright © 2018 YediTech. All rights reserved.
//

import React from "react"
import { TouchableOpacity, View, Image, Text, StyleSheet, TextInput, ScrollView, Dimensions, StatusBar } from "react-native"
import CarSelectNavOpts from "../../components/CarSelectNavOpts"
// import { ScrollView } from "react-native-gesture-handler"

const SelectCarMake = ({navigation}) => {
	
		return <View style={{height: Dimensions.get('window').height, flex:1}}>
                <StatusBar barStyle="dark-content" />
				<View >
					<View style={styles.searchareaView}>
						<Image
							source={require("../../../assets/images/icon---search.png")}
							style={styles.iconSearchImage}/>
						<TextInput
							autoCorrect={false}
							placeholder="Search keyword, category or brand "
							style={styles.placeholderText}/>
					</View>
					<Text style={styles.popularCarMakesText}> Popular Car Makes</Text>
                </View>
                <ScrollView contentContainerStyle={{height: Dimensions.get('window').height*1}}>
					{/* <View
						style={{
                            
							alignSelf: "stretch",
							height: 301,
							marginTop: 15,
						}}> */}
						{/* <View
							style={styles.rectangle3View}/> */}
						{/* <View
							style={{
								position: "absolute",
								left: 0,
								right: 0,
								top: 14,
								height: 273,
								alignItems: "flex-start",
							}}> */}
							<TouchableOpacity
								onPress={this.onCarMakeandLogoPressed}
								style={{...styles.carmakeandlogoButton, ...styles.carMakeTouchable}}>
								<Image source={require("../../../assets/images/bitmap-4.png")} style={styles.carmakeandlogoButtonImage}/>
								<Text style={styles.carmakeandlogoButtonText}>Toyota</Text>
							</TouchableOpacity>
							<View
								style={{
									width: 96,
									height: 22,
									marginLeft: 25,
									marginTop: 20,
									flexDirection: "row",
									alignItems: "flex-start",
								}}>
								<Image
									source={require("../../../assets/images/honda.png")}
									style={styles.hondaImage}/>
								<Text
									style={styles.hondaText}>Honda</Text>
							</View>
							<View
								style={styles.lineCopy4View}/>
							<View
								style={{
									width: 115,
									height: 25,
									marginLeft: 30,
									marginTop: 11,
									flexDirection: "row",
									alignItems: "flex-start",
								}}>
								<Image
									source={require("../../../assets/images/bitmap-10.png")}
									style={styles.bitmapThreeImage}/>
								<Text
									style={styles.mercedesText}>Mercedes</Text>
							</View>
							<View
								style={styles.lineCopy5View}/>
							<View
								style={{
									width: 93,
									height: 26,
									marginLeft: 19,
									marginTop: 14,
									flexDirection: "row",
									alignItems: "flex-start",
								}}>
								<Image
									source={require("../../../assets/images/bitmap-6.png")}
									style={styles.bitmapTwoImage}/>
								<Text
									style={styles.bmwText}>BMW</Text>
							</View>
							<View
								style={styles.lineCopy6View}/>
							<View
								style={{
									width: 64,
									height: 22,
									marginLeft: 30,
									marginTop: 14,
									flexDirection: "row",
									alignItems: "flex-start",
								}}>
								<Image
									source={require("../../../assets/images/kia.png")}
									style={styles.kiaImage}/>
								<Text
									style={styles.kiaText}>Kia</Text>
							</View>
							<View
								style={styles.lineCopy9View}/>
							<View
								style={{
									width: 95,
									height: 23,
									marginLeft: 28,
									marginTop: 14,
									flexDirection: "row",
									alignItems: "flex-start",
								}}>
								<Image
									source={require("../../../assets/images/bitmap-9.png")}
									style={styles.bitmapImage}/>
								<Text
									style={styles.hyudaiText}>Hyudai</Text>
							</View>
						{/* </View> */}
						<View style={styles.lineCopy7View}/>
					{/* </View> */}
                    
					<Text
						style={styles.allCarMakesAZText}>All Car Makes		A-Z</Text>
                    
					<View
						style={{
							// flex: 1,
							alignSelf: "stretch",
							marginTop: 15,
						}}>
						<View
							style={styles.rectangle3CopyView}/>
						<View
							style={{
								position: "absolute",
								left: 0,
								right: 0,
								top: 14,
								bottom: 51,
								alignItems: "flex-start",
							}}>
							<View
								style={{
									width: 78,
									height: 30,
									marginLeft: 28,
									flexDirection: "row",
									alignItems: "flex-start",
								}}>
								<Image
									source={require("../../../assets/images/bitmap-4.png")}
									style={styles.bitmapCopy3Image}/>
								<Text
									style={styles.audiText}>Audi</Text>
							</View>
							<View
								style={styles.lineCopy3View}/>
							<View
								style={{
									width: 98,
									height: 22,
									marginLeft: 25,
									marginTop: 14,
									flexDirection: "row",
									alignItems: "flex-start",
								}}>
								<Image
									source={require("../../../assets/images/honda.png")}
									style={styles.hondaCopyImage}/>
								<Text
									style={styles.accuraText}>Accura</Text>
							</View>
							<View
								style={styles.lineCopy8View}/>
							<View
								style={{
									width: 139,
									height: 25,
									marginLeft: 30,
									marginTop: 12,
									flexDirection: "row",
									alignItems: "flex-start",
								}}>
								<Image
									source={require("../../../assets/images/bitmap-10.png")}
									style={styles.bitmapCopy4Image}/>
								<Text
									style={styles.astonMartinText}>Aston Martin</Text>
							</View>
							<View
								style={styles.lineCopy10View}/>
							<View
								style={{
									width: 109,
									height: 26,
									marginLeft: 19,
									marginTop: 14,
									flexDirection: "row",
									alignItems: "flex-start",
								}}>
								<Image
									source={require("../../../assets/images/bitmap-6.png")}
									style={styles.bitmapCopy2Image}/>
								<Text style={styles.bentleyText}>Bentley</Text>
							</View>
							<View style={styles.lineCopy11View}/>
							<View
								style={{
									width: 91,
									height: 45,
									marginLeft: 20,
									marginTop: 2,
									flexDirection: "row",
									alignItems: "flex-start",
								}}>
								<Image source={require("../../../assets/images/kia-copy.png")}
									style={styles.kiaCopyImage}/>
								<Text style={styles.buickText}>Buick</Text>
							</View>
							<View style={{
									// flex: 1,
								}}/>
							<View style={styles.lineCopy12View}/>
						</View>
						<View style={styles.lineCopy13View}/>
					</View>
                    </ScrollView>
			</View>
	}
export default SelectCarMake

SelectCarMake.navigationOptions=({navigation}) => {
    return {
        headerStyle: { shadowColor: 'transparent', borderBottomWidth: 0, }, //removes visibility of default header
        headerTitle: <Text style={styles.selectCarMakeText}>Select Car Make</Text>,
	  	headerLeft: <TouchableOpacity onPress={()=>{navigation.navigate("Homepage")}}>
						<Text style={styles.cancelButton}>Cancel</Text>
					</TouchableOpacity>
    }
}

const styles = StyleSheet.create({
	selectCarMakeView: {
        height:Dimensions.get('window').height,
		// backgroundColor: "white",
		// flex: 1,
	},
	carMakeTouchable: {
        width: 96,
        height: 22,
        marginLeft: 25,
        marginTop: 20,
        flexDirection: "row",
        alignItems: "flex-start",
    },
	selectCarMakeText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		position: "absolute",
		alignSelf: "center",
		// top: 43,
	},
	cancelButton: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Roman",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		alignSelf: "flex-start",
		marginLeft: 31,
	},
	cancelButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	cancelButtonText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Roman",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
	},
	searchareaView: {
		backgroundColor: "white",
		borderRadius: 24,
		shadowColor: "rgba(0, 0, 0, 0.08)",
		shadowRadius: 8,
		shadowOpacity: 1,
		alignSelf: "stretch",
		height: 48,
		marginLeft: 25,
		marginRight: 24,
		marginTop: 15,
		flexDirection: "row",
	},
	iconSearchImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		height: null,
		width: 24,
		marginLeft: 20,
		marginTop: 12,
		marginBottom: 12,
	},
	placeholderText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		lineHeight: 20,
		letterSpacing: 0.18,
		backgroundColor: "transparent",
		opacity: 1,
		flex: 1,
		marginLeft: 16,
		marginRight: 38,
		marginTop: 15,
		marginBottom: 13,
	},
	popularCarMakesText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		letterSpacing: 0.37,
		backgroundColor: "transparent",
        marginLeft: 26,
        marginBottom: 8,
		marginTop: 19,
	},
	rectangle3View: {
		// backgroundColor: "white",
		// position: "absolute",
		// left: 0,
		// right: 0,
		// top: 0,
		// height: 301,
	},
	carmakeandlogoButton: {
		backgroundColor: "transparent",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		width: 95,
		height: 30,
		marginLeft: 28,
	},
	carmakeandlogoButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	carmakeandlogoButtonText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
	},
	hondaImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: 33,
		height: 20,
		marginTop: 1,
	},
	hondaText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Medium",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.37,
		marginLeft: 12,
	},
	lineCopy4View: {
		backgroundColor: "white",
		alignSelf: "stretch",
		height: 2,
		marginTop: 13,
	},
	bitmapThreeImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: 25,
		height: 25,
	},
	mercedesText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Medium",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.37,
		marginLeft: 15,
		marginTop: 2,
	},
	lineCopy5View: {
		backgroundColor: "white",
		alignSelf: "stretch",
		height: 2,
		marginTop: 11,
	},
	bitmapTwoImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: 45,
		height: 26,
	},
	bmwText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Medium",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.37,
		marginLeft: 6,
	},
	lineCopy6View: {
		backgroundColor: "white",
		alignSelf: "stretch",
		height: 1,
		marginLeft: 1,
		marginRight: 1,
		marginTop: 9,
	},
	kiaImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: 26,
		height: 14,
		marginTop: 4,
	},
	kiaText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Medium",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.37,
		marginLeft: 14,
	},
	lineCopy9View: {
		backgroundColor: "white",
		alignSelf: "stretch",
		height: 2,
		marginTop: 13,
	},
	bitmapImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: 30,
		height: 23,
	},
	hyudaiText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Medium",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.37,
		marginLeft: 12,
	},
	lineCopy7View: {
		backgroundColor: "white",
		position: "absolute",
		left: 0,
		right: 1,
		top: 199,
		height: 2,
	},
	allCarMakesAZText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		letterSpacing: 0.37,
		marginLeft: 31,
		marginTop: 40,
	},
	rectangle3CopyView: {
		// backgroundColor: "white",
		// position: "absolute",
		// left: 0,
		// right: 0,
		// bottom: 0,
		// height: 301,
	},
	bitmapCopy3Image: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: 30,
		height: 30,
	},
	audiText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Medium",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.37,
		marginLeft: 12,
	},
	lineCopy3View: {
		backgroundColor: "white",
		alignSelf: "stretch",
		height: 2,
		marginTop: 5,
	},
	hondaCopyImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: 33,
		height: 20,
		marginTop: 1,
	},
	accuraText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Medium",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.37,
		backgroundColor: "transparent",
		marginLeft: 12,
	},
	lineCopy8View: {
		backgroundColor: "white",
		alignSelf: "stretch",
		height: 2,
		marginTop: 13,
	},
	bitmapCopy4Image: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: 25,
		height: 25,
	},
	astonMartinText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Medium",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.37,
		backgroundColor: "transparent",
		marginLeft: 15,
		marginTop: 2,
	},
	lineCopy10View: {
		backgroundColor: "white",
		alignSelf: "stretch",
		height: 2,
		marginTop: 11,
	},
	bitmapCopy2Image: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: 45,
		height: 26,
	},
	bentleyText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Medium",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.37,
		marginLeft: 6,
	},
	lineCopy11View: {
		backgroundColor: "white",
		alignSelf: "stretch",
		height: 1,
		marginLeft: 1,
		marginRight: 1,
		marginTop: 9,
	},
	kiaCopyImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: 45,
		height: 45,
	},
	buickText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Medium",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.37,
		marginLeft: 5,
		marginTop: 12,
	},
	lineCopy12View: {
		backgroundColor: "white",
		alignSelf: "stretch",
		height: 2,
	},
	lineCopy13View: {
		backgroundColor: "white",
		position: "absolute",
		left: 0,
		right: 1,
		top: 199,
		height: 2,
	},
})
