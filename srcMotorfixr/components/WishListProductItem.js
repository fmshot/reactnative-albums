//
//  Wishlist
//  Project
//
//  Created by girlProg.
//  Copyright © 2018 YediTech. All rights reserved.
//

import { StyleSheet, Image, TouchableOpacity, Text, View } from "react-native"
import React from "react"


const WishlistProductItem = ({navigation, product}) => {
    return <TouchableOpacity onPress={() => navigation.navigate("ProductDetail", {product})} style={styles.wishlistView}>
            <View style={styles.productitemlisttreeView}>
                <View pointerEvents="box-none"
                    style={{
                        height: 90,
                        marginLeft: 15,
                        marginRight: 13,
                        flexDirection: "row",
                        alignItems: "flex-start",
                    }}>
                    <View
                        pointerEvents="box-none"
                        style={{
                            width: 218,
                            height: 90,
                        }}>
                        <View style={styles.productdetailsView}>
                            <Image
                                source={{uri: product.productImages[0].image}}
                                style={styles.productimageImage}/>
                            <View
                                pointerEvents="box-none"
                                style={{
                                    // width: 128,
                                    height: 43,
                                    marginLeft: 15,
                                    marginTop: 5,
                                    alignItems: "flex-start",
                                }}>
                                <Text style={styles.productnameText}>{product.name}</Text>
                                <Text style={styles.sellernameText}>{product.seller.shop[0].name}</Text>
                            </View>
                        </View>
                        <View style={styles.neworbelgiumView}>
                            <View style={styles.group4View}>
                                <View
                                    pointerEvents="box-none"
                                    style={{
                                        // position: "absolute",
                                        // left: 0,
                                        // right: 0,
                                        // top: 0,
                                        // bottom: 0,
                                        // justifyContent: "center",
                                    }}>
                                    <Image source={require("./../../assets/images/-background-11.png")} style={styles.backgroundImage}/>
                                </View>
                                <View
                                    pointerEvents="box-none"
                                    style={{
                                        position: "absolute",
                                        right: 0,
                                        top: 0,
                                        bottom: 0,
                                        justifyContent: "center",
                                    }}>
                                    <Text style={styles.numbersText}>{product.condition.name}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View
                        style={{
                            flex: 1,
                        }}/>
                    <TouchableOpacity
                        onPress={this.onBuyNowButtonPressed}
                        style={styles.buynowbuttonButton}>
                        <Text style={styles.buynowbuttonButtonText}>Buy Now</Text>
                    </TouchableOpacity>
                </View>
                <View style={{
                        flex: 1,
                    }}/>
                <View style={styles.dividerView}/>
            </View>
        </TouchableOpacity>
	
}

export default WishlistProductItem

const styles = StyleSheet.create({
	wishlistView: {
		backgroundColor: "white",
		// flex: 1,
	},
	productitemlisttreeView: {
		backgroundColor: "transparent",
		// height: 106,
		// marginTop: 189,
	},
	productdetailsView: {
		backgroundColor: "transparent",
		position: "absolute",
		left: 0,
		width: 218,
		top: 0,
		height: 90,
		flexDirection: "row",
		alignItems: "flex-start",
	},
	productimageImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: 90,
		height: 90,
	},
	productnameText: {
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Heavy",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		lineHeight: 20,
		paddingTop: 2,
		backgroundColor: "transparent",
	},
	sellernameText: {
		color: "rgb(171, 171, 171)",
		fontFamily: "Avenir-Book",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		lineHeight: 16,
		paddingTop: 1,
		backgroundColor: "transparent",
		marginTop: 4,
	},
	neworbelgiumView: {
		backgroundColor: "transparent",
		shadowColor: "rgba(0, 0, 0, 0.1)",
		shadowRadius: 4,
		shadowOpacity: 1,
		position: "absolute",
		left: 103,
		width: 66,
		top: 59,
		height: 22,
		justifyContent: "center",
	},
	group4View: {
		backgroundColor: "transparent",
		height: 22,
	},
	backgroundImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: null,
		height: 22,
	},
	numbersText: {
		color: "rgb(6, 6, 6)",
		fontSize: 9,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		lineHeight: 12,
		letterSpacing: 0.15,
		backgroundColor: "transparent",
		marginRight: 22,
	},
	buynowbuttonButton: {
		backgroundColor: "rgb(19, 20, 22)",
		borderRadius: 16,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		width: 78,
		height: 32,
		marginTop: 32,
	},
	buynowbuttonButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	buynowbuttonButtonText: {
		color: "white",
		fontFamily: "Avenir-Heavy",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
	},
	dividerView: {
		backgroundColor: "white",
		height: 2,
		marginRight: 1,
		marginTop: 14,
		marginTop: 15,
	},
})
