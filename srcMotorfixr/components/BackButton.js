import React from 'react'
import {Image} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';

const BackButton = ({navigation}) => {
      return (
        <TouchableOpacity onPress={()=>{navigation.navigate.pop()}}>
            <Image
            source={require('../../assets/images/back-icon-2.png')}
            style={{ width: 20, height: 15 }}
            />
        </TouchableOpacity>
      );
    }
  
export default BackButton
  