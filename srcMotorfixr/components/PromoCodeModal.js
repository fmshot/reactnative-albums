import React from 'react';
import { StyleSheet, View, Text } from "react-native";
import Svg, { Path } from "react-native-svg";

const PromoCodeModal = ({ navigation }) => {

  return (
    <View style={styles.container}>
      <View style={styles.rectangle4Stack}>
        <View style={styles.rectangle4}>
          <Text style={styles.enterPromoCode}>Enter Promo Code</Text>
          <View style={styles.rectangle5}>
            <Text style={styles.promoCode1}>Promo Code</Text>
          </View>
          <View style={styles.cancelRow}>
            <Text style={styles.cancel}>Cancel</Text>
            <Text style={styles.apply}>Apply</Text>
          </View>
        </View>
        <Svg viewBox="-0.25 -0.25 249 3" style={styles.path}>
          <Path
            strokeWidth={0.5}
            fill="transparent"
            stroke="rgba(219,219,222,1)"
            fillOpacity={1}
            strokeOpacity={1}
            d="M0.50 1.17 L247.50 1.17 "
          ></Path>
        </Svg>
        <Svg viewBox="-0.25 -0.25 3 42" style={styles.path1}>
          <Path
            strokeWidth={0.5}
            fill="transparent"
            stroke="rgba(219,219,222,1)"
            fillOpacity={1}
            strokeOpacity={1}
            d="M0.50 40.50 L0.83 0.67 "
          ></Path>
        </Svg>
      </View>
    </View>
    );
};

const styles = StyleSheet.create({
  container: {
    width: 248,
    height: 174,
    opacity: 1
  },
  rectangle4: {
    top: 0,
    left: 0,
    width: 247,
    height: 173,
    backgroundColor: "rgba(255,255,255,1)",
    position: "absolute"
  },
  enterPromoCode: {
    backgroundColor: "transparent",
    color: "rgba(51,51,51,1)",
    opacity: 1,
    fontSize: 14,
    marginTop: 36,
    marginLeft: 24
  },
  rectangle5: {
    width: 203,
    height: 37,
    backgroundColor: "transparent",
    borderColor: "rgba(219,219,222,1)",
    borderWidth: 0.5,
    marginTop: 16,
    marginLeft: 24
  },
  promoCode1: {
    backgroundColor: "transparent",
    color: "rgba(51,51,51,1)",
    opacity: 0.26,
    fontSize: 12,
    letterSpacing: 0.28,
    marginTop: 11,
    marginLeft: 8
  },
  cancel: {
    backgroundColor: "transparent",
    color: "rgba(51,51,51,1)",
    opacity: 1,
    fontSize: 14
  },
  apply: {
    backgroundColor: "transparent",
    color: "rgba(51,51,51,1)",
    opacity: 1,
    fontSize: 14,
    marginLeft: 82
  },
  cancelRow: {
    height: 13,
    flexDirection: "row",
    marginTop: 41,
    marginLeft: 37,
    marginRight: 44
  },
  path: {
    top: 132,
    left: 0,
    width: 249,
    height: 3,
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  path1: {
    top: 133,
    left: 123,
    width: 3,
    height: 42,
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  rectangle4Stack: {
    width: 249,
    height: 175
  }
});
export default PromoCodeModal;

