import { StyleSheet, View, Text, Image , TouchableOpacity} from "react-native"
import React from "react"
import {SafeAreaView} from "react-navigation"



const HomeTab = ({navigation}) => {
    return <>
    
        <TouchableOpacity style={styles.homeView}>
            <Image ource={require("../../../assets/images/home.png")} style={styles.homeImage}/>
            <View
                style={{
                    flex: 1,
                }}/>
            <Text style={styles.homeText}>Home</Text>
        </TouchableOpacity>
            </>
}

export default HomeTab

const styles = StyleSheet.create({
	group2View: {
		backgroundColor: "transparent",
		position: "absolute",
		left: 16,
		right: 6,
		top: -0,
		height: 83,
		flexDirection: "row",
		alignItems: "center",
	},
	homeView: {
		backgroundColor: "transparent",
		width: 29,
		height: 40,
		marginLeft: 16,
		alignItems: "flex-start",
	},
	homeImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: 24,
		height: 24,
		marginLeft: 1,
	},
	homeText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 10,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "center",
		letterSpacing: 0.12,
		backgroundColor: "transparent",
	},
})