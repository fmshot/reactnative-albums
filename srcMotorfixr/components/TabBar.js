import * as React from "react"
import { View, Text, TouchableOpacity, StyleSheet, Image } from "react-native"
import Tab from "./Tab"
import {SafeAreaView} from "react-navigation"


const TabBar = (props) => {
  const { navigationState, navigation, position } = props
    return <View
				style={styles.addedToBasketView}>
				<View
					style={styles.menuView}>
					<View style={styles.tabBarView}> 
						<View style={styles.group2View}>
							<TouchableOpacity
                                onPress={() => navigation.navigate("Homepage")}
								style={styles.homeView}>
								<Image
									source={require("./../../assets/images/home.png")}
									style={styles.homeImage}/>
								<View
									style={{
										// flex: 1,
									}}/>
								<Text
									style={styles.homeText}>Home</Text>
							</TouchableOpacity>
							<TouchableOpacity
                                onPress={() => navigation.navigate("Wishlist")}
								style={styles.wishlistView}>
								<Text
									style={styles.wishlistText}>Wishlist</Text>
								<Image
									source={require("./../../assets/images/star.png")}
									style={styles.starImage}/>
							</TouchableOpacity>
							<TouchableOpacity style={styles.cartView} onPress={() => navigation.navigate("Cart")} >
								<View
									pointerEvents="box-none"
									style={{
										// flex: 1,
									}}>
									<View style={styles.cartquantityView}>
										<Text style={styles.cartqtynum}>1</Text>
									</View>
									<Image
										source={require("./../../assets/images/cart-2-2.png")}
										style={styles.cart2Image}/>
								</View>
								<Text style={styles.cartText}>Cart</Text>
							</TouchableOpacity>
							<View
								style={{
									// flex: 1,
								}}/>
							<TouchableOpacity onPress={() => navigation.navigate("NearbyMechanic")} style={styles.nearbyView}>
								<Image
									source={require("./../../assets/images/pin.png")}
									style={styles.pinImage}/>
								<View
									style={{
										// flex: 1,
									}}/>
								<Text style={styles.nearbyText}>Nearby</Text>
							</TouchableOpacity>
							<TouchableOpacity
								style={styles.moreView}>
								<Image
									source={require("./../../assets/images/menu.png")}
									style={styles.menuImage}/>
								<View
									style={{
										// flex: 1,
									}}/>
								<Text style={styles.moreText}>More</Text>
							</TouchableOpacity>
						</View>
					</View>
				</View>
			</View>
	
}

export default TabBar

const styles = StyleSheet.create({
	addedToBasketView: {
		backgroundColor: "rgb(251, 252, 254)",
		// flex: 1,
        justifyContent: "flex-end",
        // marginBottom: 30,
	},
	menuView: {
		backgroundColor: "transparent",
        justifyContent: "center",
        // marginBottom: 30,
	},
	tabBarView: {
		backgroundColor: "transparent",
        height: 83,
        justifyContent: "center",
	},
	tabBarImage: {
		// resizeMode: "cover",
		// backgroundColor: "transparent",
		// shadowColor: "rgba(45, 45, 45, 0.14)",
		// shadowRadius: 20,
		// shadowOpacity: 1,
		// width: null,
	},
	group2View: {
		backgroundColor: "transparent",
		// position: "absolute",
		// left: 6,
		// right: 6,
		// top: -0,
		// height: 83,
		flexDirection: "row",
        alignItems: "center",
        // marginTop: 17,
        justifyContent: "space-evenly",


        width: null,
        height: 83.5,
        // borderRadius: 25,
        borderTopLeftRadius:25,
        borderTopRightRadius: 25,
        backgroundColor: "#ffffff",
        shadowColor: "#2d2d2d",
        shadowOffset: {
            width: 0,
            height: -3
        },
        shadowRadius: 20,
        shadowOpacity: 0.5
	},
	homeView: {
		backgroundColor: "transparent",
		// width: 29,
		height: 40,
		// marginLeft: 16,
		// alignItems: "flex-start",
	},
	homeImage: {
		// resizeMode: "center",
		backgroundColor: "transparent",
		width: 24,
		height: 24,
		// marginLeft: 1,
	},
	homeText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 10,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "center",
		letterSpacing: 0.12,
		backgroundColor: "transparent",
	},
	wishlistView: {
		backgroundColor: "transparent",
		width: 38,
		height: 43,
		// marginLeft: 35,
	},
	wishlistText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 10,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "center",
		letterSpacing: 0.12,
		backgroundColor: "transparent",
		position: "absolute",
		left: 0,
		bottom: 0,
	},
	starImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		position: "absolute",
		left: 3,
		width: 30,
		top: 0,
		height: 30,
	},
	cartView: {
		backgroundColor: "transparent",
		width: 27,
		height: 38,
		// marginLeft: 38,
	},
	cartquantityView: {
		backgroundColor: "rgb(51, 51, 51)",
		borderRadius: 20,
		position: "absolute",
		left: 18,
		right: 0,
		bottom: 10,
        height: 15,
        width: 15,
		justifyContent: "center",
	},
	cartqtynum: {
		color: "rgb(251, 252, 254)",
		fontFamily: "Avenir-Heavy",
		fontSize: 9,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "center",
		letterSpacing: 0.13,
		backgroundColor: "transparent",
		// marginLeft: 4,
		// marginRight: 5,
	},
	cart2Image: {
		resizeMode: "center",
		backgroundColor: "transparent",
		// position: "absolute",
		// left: 0,
		// width: 24,
		// top: 4,
		height: 21,
	},
	cartText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 10,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "center",
		letterSpacing: 0.12,
		backgroundColor: "transparent",
		alignSelf: "center",
	},
	nearbyView: {
		backgroundColor: "transparent",
		// width: 37,
		height: 38,
		// marginRight: 44,
		// alignItems: "flex-end",
	},
	pinImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: 19,
		height: 22,
		// marginRight: 10,
	},
	nearbyText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 10,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "center",
		letterSpacing: 0.12,
	},
	moreView: {
		backgroundColor: "transparent",
		// alignSelf: "center",
		// width: 26,
		height: 35,
		// marginRight: 23,
		marginTop: 20,
		alignItems: "center",
	},
	menuImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: 23,
		height: 18,
		marginRight: 2,
	},
	moreText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 10,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "center",
		letterSpacing: 0.12,
	},
})
