import React from 'react'
import {Image} from 'react-native'

const LogoTitle = () => {
      return (
        <Image
          source={require('../../assets/images/logoimg.png')}
          style={{ width: 200, height: 70 }}
        />
      );
    }
  
export default LogoTitle
  