import React from "react"
import {View, StyleSheet, TouchableOpacity, Text, TextInput, Image} from "react-native"

const RecentTextPopUp = () => {
    return <View style={styles.container}>
            {/* <Image source={require("../../assets/images/container-5.png")}
                style={styles.containerImage}/> */}
            {/* <View pointerEvents="box-none"
                style={{
                    position: "absolute",
                    left: 24,
                    width: 150,
                    top: 206,
                    bottom: 22,
                    alignItems: "flex-start",
                }}> */}
                    
                <Text style={styles.titleText}>RECENT SEARCHES</Text>
                
                <TouchableOpacity style={styles.bg} >
                    <Image source={require("../../assets/images/group-2-2.png")} style={styles.containerImage}/>
                    <Text style={styles.districtText}>Car part </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.bg} >
                    <Image source={require("../../assets/images/group-2-2.png")} style={styles.containerImage}/>
                    <Text style={styles.districtText}>Accessories </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.bg} >
                    <Image source={require("../../assets/images/group-2-2.png")} style={styles.containerImage}/>
                    <Text style={styles.districtText}>Tyres</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.bg} >
                    <Image source={require("../../assets/images/group-2-2.png")} style={styles.containerImage}/>
                    <Text style={styles.districtText}>Batteries</Text>
                </TouchableOpacity>
            {/* </View> */}
        </View>
}

export default RecentTextPopUp

const styles = StyleSheet.create({
    container:  {
        marginLeft: 24,
		marginRight: 24,
        // width: "100%",
        // height: 486,
        // borderRadius: 32,
        borderBottomLeftRadius: 32,
        borderBottomRightRadius: 32,
        backgroundColor: "#ffffff",
        // backgroundColor: "red",
        shadowColor: "rgba(0, 0, 0, 0.08)",
        shadowOffset: {
          width: 0,
          height: 4
        },
        shadowRadius: 8,
        shadowOpacity: 1
      },
      bg : {
        width: "100%",
        height: 44,
        alignItems : "center",
        backgroundColor: "transparent",
        flexDirection: "row"
      },
      containerImage : {
        // alignSelf : "flex-start",
        marginLeft: 30,
        justifyContent: "center",

        
      },
    searchRecentView: {
		backgroundColor: "transparent",
		shadowColor: "rgba(0, 0, 0, 0.08)",
		shadowRadius: 8,
		shadowOpacity: 1,
		position: "absolute",
		left: 0,
		right: 0,
		top: 0,
		height: 486,
    },
    districtText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		lineHeight: 20,
		letterSpacing: 0.2,
		paddingTop: 2,
		backgroundColor: "transparent",
        marginLeft: 64,
        width: "100%"
    },
    titleText: {
		color: "rgb(10, 36, 99)",
		fontFamily: "Avenir-Book",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 1,
		backgroundColor: "transparent",
        opacity: 0.5,
        marginBottom: 20,
        marginTop: 20,
	},
})