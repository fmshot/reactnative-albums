import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text, Image } from "react-native";

function MechanicOnMap(props) {
  return (
    <View style={[styles.container, props.style]}>
      <TouchableOpacity style={styles.button}>
        <View style={styles.rect2Row}>
          <View style={styles.rect2}></View>
          <View style={styles.chuckMotorsColumn}>
            <Text style={styles.chuckMotors}>Chuck Motors</Text>
            <View style={styles.chuckMotors1Row}>
              <Text style={styles.chuckMotors1}>4.6 • </Text>
              <Text style={styles.chuckMotors2}>Gudu Mechanic Village</Text>
            </View>
            <View style={styles.image2Stack}>
              <Image
                source={require("../../../assets/images/icon---direction.png")}
                resizeMode="contain"
                style={styles.image2}
              ></Image>
              <Text style={styles.loremIpsum}>200m</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  button: {
    flex: 1
  },
  rect2: {
    width: 83,
    height: 75,
    backgroundColor: "rgba(230, 230, 230,1)"
  },
  chuckMotors: {
    color: "#121212",
    fontFamily: "Avenir",
    marginLeft: 5
  },
  chuckMotors1: {
    color: "rgba(121,118,118,1)",
    fontSize: 11,
    fontFamily: "Avenir"
  },
  chuckMotors2: {
    color: "rgba(105,103,103,1)",
    fontSize: 11,
    fontFamily: "Avenir",
    marginLeft: 10
  },
  chuckMotors1Row: {
    height: 11,
    flexDirection: "row",
    marginTop: 19,
    marginLeft: 5
  },
  image2: {
    top: 0,
    left: 0,
    width: 27,
    height: 13,
    position: "absolute"
  },
  loremIpsum: {
    top: 0,
    left: 26,
    color: "#121212",
    position: "absolute",
    fontSize: 12,
    fontFamily: "Avenir"
  },
  image2Stack: {
    width: 56,
    height: 13,
    marginTop: 14
  },
  chuckMotorsColumn: {
    width: 150,
    marginLeft: 15,
    marginBottom: 5
  },
  rect2Row: {
    height: 75,
    flexDirection: "row",
    marginTop: 14,
    marginLeft: 13,
    marginRight: 25
  }
});

export default MechanicOnMap;
