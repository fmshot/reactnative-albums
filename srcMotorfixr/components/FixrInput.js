import React from 'react';
import { StyleSheet, View, Image, Text, TextInput, TouchableOpacity } from 'react-native';

const FixrInput = ({ navigation, label, placeholder  }) => {

  return (
    <>
        {label ? <Text style={styles.createAUsernameText}>{label}</Text> : null}
        <View style={styles.eMailView}>
            <TextInput
                clearButtonMode="always"
                autoCorrect={false}
                placeholder={placeholder}
                style={styles.usernameTextInput}/>
        </View>
    </>
  );
};

const styles = StyleSheet.create({
    createAUsernameText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 21,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "center",
	},
	eMailView: {
		backgroundColor: "transparent",
		borderRadius: 30,
		borderWidth: 0.5,
		borderColor: "#dbd7ce",
		borderStyle: "solid",
		height: 60,
		marginLeft: 30,
		marginRight: 30,
		marginTop: 10,
	},
	usernameTextInput: {
		color: "rgb(136, 136, 136)",
		fontFamily: "Avenir-Medium",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		backgroundColor: "transparent",
		padding: 0,
		height: 39,
		marginLeft: 18,
		marginRight: 18,
		marginTop: 12,
	},
});

export default FixrInput;




