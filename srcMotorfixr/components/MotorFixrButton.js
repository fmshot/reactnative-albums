import React from 'react';
import { StyleSheet, View, Image, Text, FlatList, TouchableOpacity } from 'react-native';

const CheckoutScreen = ({ navigation, text, destination }) => {

  return (
    <>
      <TouchableOpacity
            onPress={()=> navigation.navigate(destination)}
            style={styles.continueButton}>
            <Text
                style={styles.continueButtonText}>{text}</Text>
        </TouchableOpacity>
    </>
  );
};

CheckoutScreen.navigationOptions = {
//   title: 'Tracks'
};

const styles = StyleSheet.create({
    continueButtonText: {
		color: "white",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
	},
	continueButton: {
		backgroundColor: "rgb(19, 20, 22)",
		borderRadius: 30,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		height: 60,
		marginLeft: 35,
		marginRight: 30,
		marginTop: 101,
	},
	continueButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
});

export default CheckoutScreen;

