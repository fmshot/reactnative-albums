import React from "react";
import { StyleSheet, View, Text, ScrollView, Dimensions } from "react-native";
import MechanicListItem from "../components/MechanicListItem"

import { Image } from "react-native-elements";

function OnMapMechList(props) {
  return (
    <View style={styles.container}>
      <View style={styles.container}>
        <View style={styles.topBar}>
          <View style={styles.bound}>
            <View style={styles.nearestMechanicRow}>
              <Text style={styles.nearestMechanic}>Nearest Mechanic</Text>
              <Image style={styles.iconNext} source={require("../../assets/images/icon-4.png")}></Image>
              {/* <IconNext style={styles.iconNext}></IconNext> */}
            </View>
          </View>
        </View>
        <ScrollView contentContainerStyle={{flex:1, }}>
          <MechanicListItem style={styles.listParkingSmall2}></MechanicListItem>
          <MechanicListItem style={styles.listParkingSmall1}></MechanicListItem>
          <MechanicListItem style={styles.listParkingSmall}></MechanicListItem>
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    // width: 311,
    width: Dimensions.get("window").width/1.1,
    height: 294,
    backgroundColor: "rgba(253,254,255,1)",
    opacity: 1,
    borderRadius: 8,
    shadowOffset: {
      height: 8,
      width: 0
    },
    shadowColor: "rgba(10,36,99,0.24)",
    shadowOpacity: 1,
    shadowRadius: 20,
    overflow: "hidden",
    alignSelf: "center",
    justifyContent: "center",
    
  },
  topBar: {
    width: 311,
    height: 64,
    opacity: 1
  },
  bound: {
    width: 311,
    height: 64,
    backgroundColor: "rgba(253,254,255,1)",
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    flexDirection: "row"
  },
  nearestMechanic: {
    backgroundColor: "transparent",
    color: "rgba(10,36,99,1)",
    opacity: 1,
    fontSize: 16,
    lineHeight: 20,
    marginTop: 4
  },
  iconNext: {
    width: 8,
    height: 14,
    backgroundColor: "transparent",
    opacity: 1,
    marginLeft: 107,
    alignSelf: "flex-end"
  },
  nearestMechanicRow: {
    height: 24,
    flexDirection: "row",
    flex: 1,
    marginRight: 24,
    marginLeft: 20,
    marginTop: 20
  },
  listParkingSmall2: {
    height: 96,
    backgroundColor: "transparent",
    opacity: 1
  },
  listParkingSmall1: {
    width: 311,
    height: 96,
    backgroundColor: "transparent",
    opacity: 1
  },
  listParkingSmall: {
    height: 76,
    backgroundColor: "transparent",
    opacity: 1
  }
});

export default OnMapMechList;
