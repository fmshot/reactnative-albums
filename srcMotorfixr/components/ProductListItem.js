import React, {useContext, useEffect,  useState} from 'react';
import { StyleSheet, View, Text, ActivityIndicator, TouchableOpacity, Dimensions ,ImageBackground} from 'react-native';
import { Image } from 'react-native-elements';
import {Context as WishListContext} from '../context/WishListContext'
import Svg, { Path } from "react-native-svg";



const ProductListItem = ({ navigation, product }) => {
	const {state, addToWishList, getWishList, getWishListIds} = useContext(WishListContext)
	useEffect(()=>{getWishList();getWishListIds()}, [])

  return (
  		<>
        <TouchableOpacity onPress={()=>navigation.navigate("ProductDetail", {product})}>
				<View style={styles.productitemviewView}>
					<ImageBackground source={{uri: product.productImages[0].image}}  style={styles.photoviewView}>
						<View
							pointerEvents="box-none"
							style={{
								height: 32,
								marginLeft: 8,
								marginRight: 8,
								marginTop: 5,
								flexDirection: "row",
								alignItems: "flex-start",
							}}>
							<View style={styles.belgiumviewView}>
								<Image source={require("./../../assets/images/-background-3.png")} style={styles.belgiumbackgroundImage}/>
								<Text style={styles.belgiumtextText}>{product.condition.name}</Text>
							</View>
							<View style={{ flex: 1, }}/>
                            <TouchableOpacity onPress={()=>{addToWishList(product)}} >
                                <View style={state.wishlistids? state.wishlistids.includes(product.id) ? styles.starbuttonviewView : styles.notstarbuttonviewView2 : styles.notstarbuttonviewView2 }>
                                    {/* <View style={styles.defaultAbsolutePosition}>
                                        <Image source={require("./../../assets/images/info.png")} style={styles.roundviewImage} />
                                    </View> */}
                                    <View style={styles.defaultAbsolutePosition}>
                                        <Image source={require("./../../assets/images/star-2.png")} style={styles.starviewImage}/>
                                    </View>
                                </View>
                            </TouchableOpacity>
						</View>
						</ImageBackground>
					<View style={{ flex: 1, }}/>
					<View style={{
							// width: 81,
							height: 36,
							marginBottom: 4,
						}}>
						<Text style={styles.dealernameText}>{product.seller.shop[0].name}</Text>
						<Text style={styles.productnameText}>{product.name}</Text>
					</View>
                    <Text style={styles.priceText}>₦{product.price} </Text>
				</View>
                </TouchableOpacity> 
    </>
  );
};


const styles = StyleSheet.create({
    productitemviewView: {
		backgroundColor: "transparent",
        height: 214,
        width: Dimensions.get('window').width/2.3,
		marginLeft: 15,
		// marginRight: 195,
		marginTop: 29,
		alignItems: "flex-start",
    },
    defaultAbsolutePosition: {
        position: "absolute",
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
		justifyContent: "center",
		
    },
	photoviewView: {
		borderWidth: 1,
		borderColor: 'grey',
		backgroundColor: "rgb(242, 242, 242)",
		borderRadius: 12,
		alignSelf: "stretch",
		height: 145,
		borderRadius: 15,
	},
	belgiumviewView: {
		backgroundColor: "transparent",
		width: 69,
		height: 28,
		marginTop: 3,
		shadowColor: "rgba(0, 0, 0, 0.08)",
		shadowRadius: 8,
		shadowOpacity: 1,
		shadowColor: "grey",
		shadowOffset: {
			width: 0,
			height: 1
		},
		shadowRadius: 4,
		shadowOpacity: 1
	},
	belgiumbackgroundImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		position: "absolute",
		left: 0,
		width: 69,
		top: 0,
		height: 22,
		
	},
	belgiumtextText: {
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Heavy",
		fontSize: 9,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "center",
		lineHeight: 12,
		letterSpacing: 0.15,
		backgroundColor: "transparent",
		position: "absolute",
		left: 4,
		width: 61,
		top: 4,
	},
	starbuttonviewView: {
		backgroundColor: "transparent",
		width: 32,
		height: 32,
		backgroundColor : 'yellow',
		borderRadius : 18,

		shadowColor: "#0000",
		shadowOffset: {
			width: 0,
			height: 1
		},
		shadowRadius: 4,
		shadowOpacity: 1
	},
	notstarbuttonviewView2: {
		backgroundColor: "transparent",
		width: 32,
		height: 32,
		borderRadius : 18,
		backgroundColor: "white",
		shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 1
  },
  shadowRadius: 4,
  shadowOpacity: 1
	},
	roundviewImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: null,
		height: 32,
		
	},
	starviewImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: null,
		height: 18,
		marginLeft: 6,
		marginRight: 6,
	},
	profileImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		alignSelf: "center",
		width: 70,
		height: 70,
		marginTop: 1,
	},
	dealernameText: {
		color: "rgb(171, 171, 171)",
		fontFamily: "Avenir-Book",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		lineHeight: 16,
		paddingTop: 1,
		backgroundColor: "transparent",
		position: "absolute",
		left: 0,
		bottom: 0,
	},
	productnameText: {
		backgroundColor: "transparent",
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		lineHeight: 18,
		letterSpacing: 0.2,
		paddingTop: 2,
		position: "absolute",
		left: 0,
		bottom: 16,
	},
	priceText: {
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		lineHeight: 18,
		letterSpacing: 0.2,
		paddingTop: 2,
		backgroundColor: "transparent",
	},
});

export default ProductListItem;

