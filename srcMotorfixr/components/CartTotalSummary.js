import React from 'react';
import { StyleSheet, View, Text, Dimensions } from 'react-native';

const CartTotalSummary = ({ navigation }) => {

  return (
    <>
      <View style={styles.carttotalView}>
        <View style={styles.summaryView}>
            <Text style={styles.summaryTextLeft}>Subtotal</Text>
            <Text style={styles.summaryTextRight}>₦6,000 </Text>
        </View>
        <View style={styles.summaryView}>
            <Text style={styles.summaryTextLeft}>Shipping</Text>
            <Text style={styles.summaryTextRight}>TBD</Text>
        </View>
        <View style={{...styles.summaryView, marginTop: 20}}>
            <Text style={styles.summaryTextLeft}>Total</Text>
            <Text style={styles.summaryTextRight}>₦6,000 </Text>
        </View>
        <View style={styles.dividerView}/>
        
    </View>
    </>
  );
};

CartTotalSummary.navigationOptions = {
//   title: 'Tracks'
};

const styles = StyleSheet.create({
    carttotalView: {
		marginTop: 89,
		textAlign: "left"
	},
	summaryTextRight: {
		flex: 1,
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Roman",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "right",
		letterSpacing: 0.25,
		marginLeft: 50,
	},
	summaryTextLeft: {
		flex: 1,
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Roman",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
    },
    summaryView:{
		marginLeft: Dimensions.get("window").width/5,
		marginRight: Dimensions.get("window").width/5,
		flexDirection: "row",
	},
});

export default CartTotalSummary;
