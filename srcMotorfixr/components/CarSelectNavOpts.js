import React from 'react'
import { TouchableOpacity, Text, StyleSheet } from 'react-native'

const carSelectNavOpts = (headerTitle, headerLeftText, headerLeftNavigation, navigation) => {
      return {
        headerTitle: <Text style={styles.addCarText}>{headerTitle}</Text>,
        headerLeft: <TouchableOpacity onPress={()=>{navigation.navigate({headerLeftNavigation})}}>
                        <Text style={styles.cancelText}>{headerLeftText}</Text>
                    </TouchableOpacity>
      }
      
}

export default carSelectNavOpts

const styles = StyleSheet.create({

	cancelText: {
		marginLeft: 30,
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Roman",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		backgroundColor: "transparent",
	},
	addCarText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
    }
})