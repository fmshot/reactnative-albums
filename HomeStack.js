import React from 'react'; 
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import HomeScreen from "./src/screens/HomeScreen";
import ComponentsScreen from "./src/screens/ComponentsScreen";
import ListScreen from './src/screens/ListScreen';
import ImageScreen from './src/screens/ImageScreen';
import CounterScreen from './src/screens/CounterScreen';
import ColorScreen from './src/screens/ColorScreen';
import SquareScreen from './src/screens/SquareScreen';
import TextScreen from './src/screens/TextScreen';




import {createBottomTabNavigator} from 'react-navigation-tabs';
import { StyleSheet, View, TouchableOpacity, Text, Image, ScrollView} from "react-native";

import BottomTabA from './src/screens/ScreenBottomA';
import BottomTabB from './src/screens/ScreenBottomB';
import BottomTabC from './src/screens/ScreenBottomC';
import BottomTabD from './src/screens/ScreenBottomD';
import BottomTabE from './src/screens/ScreenBottomE';
import OcticonsIcon from "react-native-vector-icons/Octicons";



const navigator = createStackNavigator(
  {
    Home: HomeScreen,
    Components: ComponentsScreen,
    List: ListScreen,
    Image: ImageScreen,
    Counter: CounterScreen,
    Color: ColorScreen,
    Square: SquareScreen,
    Text:   TextScreen
  },
  {
    initialRouteName: "Home",
    defaultNavigationOptions: {
      title: "App"
    }
  }
);

// tabBarIcon:({tintColor})=>(  
//   <Icon name="ios-home" color={tintColor} size={25}/>  
// ) 

// Home:{  
//   screen:HomeScreen,  
//   navigationOptions:{  
//     tabBarLabel:'Home',  
//     tabBarIcon:({tintColor})=>(  
//       <View>
//       <OcticonsIcon name="home"></OcticonsIcon>
// </View>      )  
//   }  
// }, 
// const navigator = createBottomTabNavigator({
//   TabA: createStackNavigator({
//     TabA: BottomTabA,
//     ScreenA: ImageScreen
//     }  
//   ), 
//   TabB: createStackNavigator({
//     TabB: BottomTabB,
//   }),
//   TabC: createStackNavigator({
//     TabC: BottomTabC,
//   }) ,
//   TabD: createStackNavigator({
//     TabD: BottomTabD,
//   }) ,
//   TabE: createStackNavigator({
//     TabE: BottomTabE,
//   }) ,
// },
// {
// defaultNavigationOptions: ({navigation}) =>({
//   tabBarIcon: ({focused, horizontal, tintColor }) => {
//     const {routeName} = navigation.state
//     let IconComponent = OcticonsIcon
//     let iconName
//     if (routeName === 'TabA'){
//       iconName = focused ? 'home' : 'home'
//     }else if (routeName === 'TabB'){
//       iconName = focused ? 'home' : 'home'
//     }
//     return <IconComponent name = {iconName} size = {25} color = {tintColor}></IconComponent>
//   }
// }),


// backBehavior: 'none',
// tabBarOptions: {
//     // showLabel: false,
//     tabBarIcon:({tintColor})=>(  
//       <View>
//       <OcticonsIcon name="home"></OcticonsIcon>
// </View>
// )  ,

//     keyboardHidesTabBar: false,
//     tabStyle: {
//       backgroundColor: '#f4511e'
//     },
//     style: {
//       backgroundColor: 'blue',
//     },
// }
// }
// );



// const navigator = createBottomTabNavigator({  
  // Home:{  
  //   screen:HomeScreen,  
  //   navigationOptions:{  
  //     tabBarLabel:'Home',  
  //     tabBarIcon:({tintColor})=>(  
  //         <Icon name="ios-home" color={tintColor} size={25}/>  
  //     )  
  //   }  
  // },   
//     Profile: {  
//       screen:BottomTabD,  
//       navigationOptions:{  
//         tabBarLabel:'Profile',  
//         tabBarIcon:({tintColor})=>(  
//           <View>
//             {/* // <Icon name="ios-person" color={tintColor} size={25}/>   */}
//             <OcticonsIcon name="home"></OcticonsIcon>
//             </View>
//         )  
//       }  
//     },  
 
//   },  
//   // {  
//   //   initialRouteName: "Home"  
//   // },  
//   {
// backBehavior: 'none',
// tabBarOptions: {
//     showLabel: false,

//     keyboardHidesTabBar: false,
//     tabStyle: {
//       backgroundColor: 'green'
//     },
//     style: {
//       backgroundColor: 'blue',
//     },
// }
// }
// );  

export default createAppContainer(navigator);