import React, { useState } from "react";
import {AppRegistry, View } from 'react-native';

import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createDrawerNavigator } from "react-navigation-drawer";
// import { AppLoading } from "expo";
// import * as Font from "expo-font";
// import Untitled from "./srcAyahay/screens/Untitled";
import IndexScreen from "./srcUdemyFatBlog/screens/IndexScreen";
// import ResultsShowScreen from "./srcUdemyFat/screens/ResultsShowScreen";
import { Provider as BlogProvider } from './srcUdemyFatBlog/context/BlogContext';

// const DrawerNavigation = createDrawerNavigator({
//   // Untitled: Untitled,
//   SearchScreen: SearchScreen
// });

const navigator = createStackNavigator(
  {
    // DrawerNavigation: {
    //   screen: DrawerNavigation
    // },
    // Untitled: Untitled,
    Index: IndexScreen,
    // ResultsShow: ResultsShowScreen
  },
//   {
//     headerMode: "none"
//   }
  {
    // headerMode: "none",
    initialRouteName: "Index",
    defaultNavigationOptions: {
      title: "Blogs"
    }
  }
);

const App =  createAppContainer(navigator);

 export default () => {
     return <BlogProvider>
         <App />
        </BlogProvider>
 }