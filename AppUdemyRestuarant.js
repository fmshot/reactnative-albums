import React, { useState } from "react";
import {AppRegistry, View } from 'react-native';

import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createDrawerNavigator } from "react-navigation-drawer";
// import { AppLoading } from "expo";
// import * as Font from "expo-font";
// import Untitled from "./srcAyahay/screens/Untitled";
import SearchScreen from "./srcUdemyFat/screens/SearchScreen";
import ResultsShowScreen from "./srcUdemyFat/screens/ResultsShowScreen";


// const DrawerNavigation = createDrawerNavigator({
//   // Untitled: Untitled,
//   SearchScreen: SearchScreen
// });

const navigator = createStackNavigator(
  {
    // DrawerNavigation: {
    //   screen: DrawerNavigation
    // },
    // Untitled: Untitled,
    SearchScreen: SearchScreen,
    ResultsShow: ResultsShowScreen
  },
//   {
//     headerMode: "none"
//   }
  {
    // headerMode: "none",
    initialRouteName: "SearchScreen",
    defaultNavigationOptions: {
      title: "Business Search"
    }
  }
);

export default createAppContainer(navigator);