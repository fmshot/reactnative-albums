import axios from 'axios';

export default axios.create({
    baseURL: 'https://rallycoding.herokuapp.com/api',
    headers: {
        Authorization:
        'Bearer 42676gg6876a76gg6767gj67665433tggt66266dhuhduugt'
    }
});

// axios.get('https://rallycoding.herokuapp.com/api/music_albums')
