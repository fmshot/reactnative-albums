import React, { Component } from "react";
import { StyleSheet, View, Text, Image, ScrollView, TextInput } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";

import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';


const SearchBar = ({ term, onTermChange, onTermSubmit }) => {
    return(
        <View style={styles.backgroundStyle}>
            <Icon name="search"  style={styles.icon}></Icon>
            <TextInput 
            autoCapitalize="none"
            autoCorrect={false}
            style={styles.InputStyle} 
            placeholder="Search"
            value={term}
            onChangeText={onTermChange}
            onEndEditing={onTermSubmit}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    backgroundStyle: {
        backgroundColor: '#F0EEEE',
        height: 50,
        borderRadius: 5,
        marginHorizontal: 5,
        flexDirection: 'row',
        marginTop: 10,
    },
    icon: {
        // marginTop: 10,
        // marginRight: 5,
        fontSize: 40,
        alignSelf: "center",
        marginHorizontal: 1,
    },
    InputStyle: {
        // borderColor: 'black',
        // borderWidth: 1,
        flex: 1,
        fontSize: 18,
    }
});

export default SearchBar;