import React, { Component } from "react";
import { StyleSheet, View, Text, FlatList, Image, ScrollView, TextInput } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";

import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';


const ResultsDetail = ({ result }) => {
    // console.log({result})
    return(
        <View style={styles.container}>
            {/* <Image style={styles.logo} source={{ uri: result.image }} /> */}
            <Text style={styles.title}>{result.title}</Text>
            <Text style={styles.artist}>{result.artist}</Text>
            {/* <Text>Results: {results.length}</Text> */}
            {/* <FlatList
            horizontal
            data={results}
            renderItem={({item}) =>{
                return <Text>{item.title}</Text>
            } }
            /> */}
        </View>
    );
};
    // style={styles.logo}

const styles = StyleSheet.create({
    container: {
        marginLeft: 10,
    },
    title: {
        fontSize: 14,
        fontWeight: 'bold'
    },
    artist: {
        fontSize: 12,
        // fontWeight: 'bold'
    },
    logo: {
        width: 66,
        height: 58,
      },
    // Image: {
    //     width: 250,
    //     height: 121,
    //     borderRadius: 1
    // },
    backgroundStyle: {
        backgroundColor: '#F0EEEE',
        height: 50,
        borderRadius: 5,
        marginHorizontal: 5,
        flexDirection: 'row',
        marginTop: 10,
    },
    icon: {
        // marginTop: 10,
        // marginRight: 5,
        fontSize: 40,
        alignSelf: "center",
        marginHorizontal: 1,
    },
    InputStyle: {
        // borderColor: 'black',
        // borderWidth: 1,
        flex: 1,
        fontSize: 18,
    }
});

export default ResultsDetail;