import React, { Component } from "react";
import { StyleSheet, View, Text, FlatList, TouchableOpacity, Image, ScrollView, TextInput } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import ResultsDetail from './ResultDetail';

import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import { withNavigation } from 'react-navigation';


const ResultsList = ({ title, results, navigation }) => {
    console.log({results});
    if (!results.length) {
        return null;
    }

    return(
        <View>
            <Text style={styles.title}>{title}</Text>
            {/* <Text>Results: {results.length}</Text> */}
            <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            keyExtractor={item => item.title}
            data={results}
            renderItem={({item}) =>{
                // return <Text>{item.title}</Text>
                return (
                // <TouchableOpacity onPress={() => navigation.navigate('ResultsShow')}>
                <TouchableOpacity onPress={() => navigation.navigate('ResultsShow', {apiEnd: 'music_albums', itemTitle: item.title,})}>
                {/* <TouchableOpacity onPress={() => navigation.navigate('ResultsShow', {title: item.id})}> */}
                    <ResultsDetail result={item} />
                </TouchableOpacity>
                )
            } }
            />
        </View>
    );
};

const styles = StyleSheet.create({
    title: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    backgroundStyle: {
        backgroundColor: '#F0EEEE',
        height: 50,
        borderRadius: 5,
        marginHorizontal: 5,
        flexDirection: 'row',
        marginTop: 10,
    },
    icon: {
        // marginTop: 10,
        // marginRight: 5,
        fontSize: 40,
        alignSelf: "center",
        marginHorizontal: 1,
    },
    InputStyle: {
        // borderColor: 'black',
        // borderWidth: 1,
        flex: 1,
        fontSize: 18,
    }
});

export default withNavigation(ResultsList);