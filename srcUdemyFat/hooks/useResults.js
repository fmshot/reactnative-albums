import { useEffect, useState } from 'react';
import yelp from '../api/yelp';

export default () => {
    const [results, setResults] = useState([]);
    const [errorMessage, setErrorMessage] = useState('');



    const searchApi = async (searchTerm) => {
        console.log('Hi there!');
        try {
     const response = await yelp.get( `/${searchTerm}`);
     setResults(response.data);
     console.log('Hi there2!');

    } 
        catch (err) {
            console.log(err);
            setErrorMessage('Something went wrong');
    }
    };

    // const searchApifromTutorial = async () => {
    //     const response = await yelp.get('/search', {
    //         params: {
    //             Limit: 50,
    //             term,
    //             location: 'san jose'
    //         }
    //     });
    //     setResults(response.data);
    //    };
     
  //Bad code for initial component call
//   searchApi('pasta');

    useEffect(() => {
        searchApi('music_albums');
    }, [])
    return [searchApi, results, errorMessage]
};