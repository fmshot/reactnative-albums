import React, { Component, useState, useEffect } from "react";
import { StyleSheet, View, Text, Image, ScrollView } from "react-native";
import SearchBar from '../components/SearchBar';
import useResults from "../hooks/useResults";
import ResultsList from '../components/ResultsList';

import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';


const SearchScreen = () => {
    // console.log(props)
    const [term, setTerm] = useState('');
    const [searchApi, results, errorMessage] = useResults();

    // console.log(results);

    const filterResultsByPrice = (artist) => {

        return results.filter(result => {
            return result.artist === artist;
        });

    }


    return(
        <>
        {/* // <View style={styles.container}> */}
            <SearchBar 
            term={term} 
            onTermChange={setTerm} 
            onTermSubmit={() => searchApi(term)}
            />
            
            {errorMessage ? <Text>{errorMessage}</Text> : null}
            <Text>We have found {results.length} results</Text>
            <ScrollView>
            <ResultsList 
            results={filterResultsByPrice('Taylor Swift')}
             title='Cost Effective'
             />
            {/* <ResultsList 
            results={filterResultsByPrice('Taylor Swift')} 
            title='Bit Pricier'

            />
            <ResultsList 
            results={filterResultsByPrice('Taylor Swift')}
             title='Bit Spender'
             /> */}
            </ScrollView>
        </>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
});

export default SearchScreen;