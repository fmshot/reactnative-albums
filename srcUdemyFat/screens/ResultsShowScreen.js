import React, { Component, useState, useEffect } from "react";
import { StyleSheet, View, Text, FlatList, Image, ScrollView } from "react-native";
import SearchBar from '../components/SearchBar';
import useResults from "../hooks/useResults";
import ResultsList from '../components/ResultsList';
import yelp from '../api/yelp';


import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';


const ResultsShowScreen = ({ navigation }) => {
    const [result, setResult] = useState(null);
    const apiEnd = navigation.getParam('apiEnd');
    const itemTitle = navigation.getParam('itemTitle')
    console.log(apiEnd);
    console.log(result);



    const getResult = async (apiEnd) => {
        // const response = await yelp.get(`/${id}`);
        const response = await yelp.get(`/${apiEnd}`);
         setResult( 
             response.data.find((item) => 
                 item.title === itemTitle
             ));
    };
    // myArray.find(x => x.id === '45' && x.color == 'red').foo
    useEffect(() => {
        getResult(apiEnd);
    }, []);
    console.log('for resultShowScreen', result);

    if (!result) {
        return null;
    }
//
    // return (
    //     <View>
    //         <Text>{itemTitle}</Text>
    //         <FlatList
    //             data={result.image}
    //             keyExtractor={item => item.title}
    //             renderItem={({item}) => {
    //                 return <Image style={styles.image} source={{ uri: item }} />
    //             }}
    //             />
    //     </View>
    // );


    return(
        <View style={styles.container}>
            <Image style={styles.logo} source={{ uri: result.image }} />
            <Text style={styles.title}>{result.title}</Text>
            <Text style={styles.artist}>{result.artist}</Text>
            {/* <Text>Results: {result.length}</Text> */}
            {/* <FlatList
            horizontal
            data={results}
            renderItem={({item}) =>{
                return <Text>{item.title}</Text>
            } }
            /> */}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        marginLeft: 10,
    },
    title: {
        fontSize: 14,
        fontWeight: 'bold'
    },
    artist: {
        fontSize: 12,
        // fontWeight: 'bold'
    },
    logo: {
        width: 66,
        height: 58,
      },
});

export default ResultsShowScreen;