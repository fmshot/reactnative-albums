import React, { Component } from "react";
import { StyleSheet, View } from "react-native";

function Untitled7(props) {
  return (
    <View style={styles.container}>
      <View style={styles.rectStack}>
        <View style={styles.rect}>
          <View style={styles.rect2}></View>
          <View style={styles.rect3}></View>
        </View>
        <View style={styles.rect4}></View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  rect: {
    top: 0,
    left: 0,
    width: 324,
    height: 36,
    backgroundColor: "rgba(3,3,150,1)",
    position: "absolute",
    opacity: 0.15,
    borderColor: "#000000",
    borderWidth: 0,
    borderStyle: "dotted"
  },
  rect2: {
    flex: 0.5,
    backgroundColor: "rgba(235, 235, 235,1)"
  },
  rect3: {
    flex: 0.5,
    backgroundColor: "rgba(239, 239, 239,1)"
  },
  rect4: {
    top: 0,
    left: 0,
    width: 155,
    height: 36,
    backgroundColor: "rgba(230, 230, 230,1)",
    position: "absolute"
  },
  rectStack: {
    width: 324,
    height: 36,
    marginTop: 72,
    marginLeft: 33
  }
});

export default Untitled7;
