import React, { Component, useState , useContext, useEffect} from "react";
import { StyleSheet, View, ScrollView, Text, Image } from "react-native";
import OcticonsIcon from "react-native-vector-icons/Octicons";
import SimpleLineIconsIcon from "react-native-vector-icons/SimpleLineIcons";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {Calendar, Agenda} from 'react-native-calendars'
import EventListItem from "../components/EventListItem";
import {Context} from '../context/eventsContext'
import AdvertPanel from "../components/AdvertPanel";

const SelectDate = (props) => {
  const { state,  getEvents} = useContext(Context)
  useEffect (()=>{ getEvents();  collectEventDates()},[])
  const [months] = useState(['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'])
  const [monthname, setMonthname] = useState(months[new Date().getMonth()])
  
  const collectEventDates = () => {
    // state? state.events?  
    const x = state && state.events &&  state.events.reduce((event)=>{
      const datey = new Date(event.date)
      return  datey.toDateString() 
    }) 
    console.log(x)
  }

  return (
    <View style={styles.container}>
      <Text style={styles.monthname}>{monthname}</Text>
      
        <View style={{flex:1,backgroundColor: '#e6e6e6'}}>
          <Calendar style={{backgroundColor: '#e6e6e6'}}
            // Initially visible month. Default = Date()
            // current={new Date()}
            // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
            minDate={'2012-05-10'}
            // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
            // maxDate={'2012-05-30'}
            // Handler which gets executed on day press. Default = undefined
            onDayPress={(day) => {collectEventDates();}}
            // Handler which gets executed on day long press. Default = undefined
            onDayLongPress={(day) => {console.log('selected day', day)}}
            // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
            monthFormat={'yyyy'}
            // Handler which gets executed when visible month changes in calendar. Default = undefined
            onMonthChange={(month) => {setMonthname(months[month.month-1])}}
            // Hide month navigation arrows. Default = false
            // hideArrows={true}
            // Replace default arrows with custom ones (direction can be 'left' or 'right')
            // renderArrow={(direction) => (<Arrow/>)}
            // Do not show days of other months in month page. Default = false
            // hideExtraDays={false}
            // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
            // day from another month that is visible in calendar page. Default = false
            disableMonthChange={false}
            // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
            firstDay={1}
            // Hide day names. Default = false
            hideDayNames={false}
            // Show week numbers to the left. Default = false
            // showWeekNumbers={true}
            // Handler which gets executed when press arrow icon left. It receive a callback can go back month
            // onPressArrowLeft={substractMonth => substractMonth()}
            // Handler which gets executed when press arrow icon right. It receive a callback can go next month
            // onPressArrowRight={addMonth => addMonth()}
            // Disable left arrow. Default = false
            // disableArrowLeft={true}
            // Disable right arrow. Default = false
            // disableArrowRight={true}
            markedDates={collectEventDates()}
          
          />
          
         <EventListItem></EventListItem>
        <AdvertPanel></AdvertPanel>
        </View>
      </View>
 );
};


const styles = StyleSheet.create({ 
  container: {
    flex: 1,
    backgroundColor: 'white',
    // borderColor: 'blue',
    // borderWidth: 4,

  },
  monthname: {
    textAlign: 'center',
      color: "rgba(0,0,0,1)",
      fontSize: 40,
      fontFamily: "Futura-Medium"
    
  },
  scrollArea3: {
    width: 415,
    height: 52,
    backgroundColor: "rgba(255,255,255,1)",
    elevation: 150,
    borderRadius: 17,
    shadowOffset: {
      height: 5,
      width: 5
    },
    shadowColor: "rgba(0,0,0,1)",
    shadowOpacity: 0.24,
    shadowRadius: 50,
    marginTop: 104
  },
  image1: {
    width: wp('15%'),
    height: hp('7%'),
    marginTop: hp('1%'),
    marginBottom: hp('1%'),    
    // marginLeft: 162
    alignSelf: 'center'
  },
  january: {
    alignSelf: 'center',
    fontSize: wp('6%'),
  },
  calenderText: {
    alignSelf: 'center',
    fontSize: wp('5%'),
  },
  calenderParent: {
    // // alignSelf: 'center',
    // width: wp('100%'),
    // height: hp('20%'),
    // backgroundColor: 'green',
    // borderColor: 'black',
    // borderRadius: 10,
    paddingLeft: wp('10%'),
    paddingRight: wp('10%'),
    paddingBottom: wp('10%'),
    
  },
  calenderChild: {
    // alignSelf: 'center',
    // width: wp('100%'),
    height: hp('50%'),
    backgroundColor: 'grey',
    borderColor: 'black',
    borderRadius: 10,
  },
  event: {
    // alignSelf: 'center',
    // width: wp('100%'),
    // height: hp('20%'),
    // backgroundColor: 'white',
    // borderColor: 'black',
    // borderRadius: 10,
  },
  options: {
    borderColor: 'grey',
    backgroundColor: 'white',
    borderRadius: 10,
    borderWidth: 0.1,
    height: hp('6%'),
    // opacity: 0.4,
    elevation: 1500,
    marginBottom: hp("0.5%"),
    
  },
  januarytest1: {
    opacity: 0.5,

  },
  januarytest2: {
    opacity: 0.8,

  },
  januarytest3: {
    opacity: 0.5,

  },

  rect1: {
    // width: 375,
    // height: 101,
    // width: wp('90%'),
    height: hp('10%'),
    backgroundColor: "rgba(230, 230, 230,1)",
    // marginTop: 711
    // alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  icons: {
    
    flexDirection: "row",
    alignSelf: 'center',
    justifyContent: 'space-around',
    flex: 1
  },
  icon1Row: {
    flexDirection: "row",
    flex: 1,
    alignSelf: 'center',
    justifyContent: 'space-around',
  },
  icon1: {
    color: "rgba(255,255,255,1)",
    fontSize: hp('4%'),
  },
  icon2: {
    color: "rgba(0,0,0,1)",
    fontSize: hp('4%'),
  },
  icon5: {
    color: "rgba(0,0,0,1)",
    fontSize: hp('4%'),
  },
});

export default SelectDate;