import React, { Component } from "react";
import { StyleSheet, View, ScrollView, Text, Image } from "react-native";
import OcticonsIcon from "react-native-vector-icons/Octicons";
import SimpleLineIconsIcon from "react-native-vector-icons/SimpleLineIcons";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

function CountdownScreen(props) {
  const [daysLeft, chooseDay] = useState([
    {days: '30 days', index: 1,  key: '1'},
    {days: '20 days', index: 2, key: '2'},
    {days: '10 days', index: 3, key: '3'},
    {days: '5 days', index: 4, key: '4'},
    {days: '1 days', index: 5, key: '5'},
    {days: 'Today', index: 6, key: '6'},

  ]);
  return (
    <View style={styles.container}>
      {/* <ScrollView> */}
      {/* <Image
        source={require("../assets/images/guide_logo.png")}
        resizeMode="contain"
        style={styles.image1}
      ></Image> */}
         <Text style={styles.january}>Countdown</Text>
      {/* <View style={styles.rect111}> */}
        <FlatList 
          data={daysLeft}
          renderItem={({ item }) => (
            // <Text>{item.days}</Text>
            <CountdownItem  item={item} />
          )}
          />
             {/* <FlatList 
          data={daysLeft}
          renderItem={({ item }) => (
            // <Text>{item.days}</Text>
            <CountdownItem style={styles.rect} item={item} />
          )}
          /> */}
      {/* </View> */}

{/* 
  return (
    <View style={styles.container}>
      <ScrollView>
      <View style={styles.scrollArea1StackStack}>
        <View style={styles.scrollArea1Stack}>
          <View style={styles.scrollArea1}>
            <ScrollView
              horizontal={false}
              contentContainerStyle={styles.scrollArea1_contentContainerStyle}
            ></ScrollView>
          </View>
          <View style={styles.group2}>
            <View style={styles.today2Column}>
              <Text style={styles.today2}>10 days</Text>
              <Text style={styles.today3}>26 days</Text>
              <Text style={styles.today4}>40 days</Text>
              <Text style={styles.today5}>40 days</Text>
            </View>
            <View style={styles.today2ColumnFiller}></View>
            <View style={styles.today7Column}>
              <Text style={styles.today7}>today</Text>
              <Text style={styles.today1}>7 days</Text>
            </View>
          </View>
          <Text style={styles.countdowns}>countdowns</Text>
        </View>
        <View style={styles.scrollArea2}>
          <ScrollView
            horizontal={false}
            contentContainerStyle={styles.scrollArea2_contentContainerStyle}
          ></ScrollView>
        </View>
        <View style={styles.scrollArea3}>
          <ScrollView
            horizontal={false}
            contentContainerStyle={styles.scrollArea3_contentContainerStyle}
          ></ScrollView>
        </View>
        <View style={styles.scrollArea4}>
          <ScrollView
            horizontal={false}
            contentContainerStyle={styles.scrollArea4_contentContainerStyle}
          ></ScrollView>
        </View>
        <View style={styles.scrollArea5}>
          <ScrollView
            horizontal={false}
            contentContainerStyle={styles.scrollArea5_contentContainerStyle}
          ></ScrollView>
        </View>
        <View style={styles.scrollArea6}>
          <ScrollView
            horizontal={false}
            contentContainerStyle={styles.scrollArea6_contentContainerStyle}
          ></ScrollView>
        </View>
        <Image
          source={require("../assets/images/guide_logo.png")}
          resizeMode="contain"
          style={styles.image1}
        ></Image>
      </View>
      <View style={styles.scrollArea7Stack}>
        <View style={styles.scrollArea7}>
          <ScrollView
            horizontal={false}
            contentContainerStyle={styles.scrollArea7_contentContainerStyle}
          ></ScrollView>
        </View>
        {/* <View style={styles.rect}>
          <View style={styles.icons}>
            <View style={styles.icon1Row}>
              <OcticonsIcon name="home" style={styles.icon1}></OcticonsIcon>
              <SimpleLineIconsIcon
                name="calendar"
                style={styles.icon2}
              ></SimpleLineIconsIcon>
              <MaterialCommunityIconsIcon
                name="circle-slice-5"
                style={styles.icon3}
              ></MaterialCommunityIconsIcon>
            </View>
          </View>
        </View> */}
      </View>
      // </ScrollView>
    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  image1: {
    width: wp('15%'),
    height: hp('12%'),
    marginTop: hp('1%'),
    marginBottom: hp('4%'),    
    alignSelf: 'center'
  },
  january: {
    alignSelf: 'center',
    fontSize: wp('6%'),
  },

});

export default CountdownScreen;
