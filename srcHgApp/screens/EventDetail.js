import React, { Component, useContext, useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,FlatList,
  Dimensions, 
} from "react-native";
import EntypoIcon from "react-native-vector-icons/Entypo";
import IoniconsIcon from "react-native-vector-icons/Ionicons";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import ProgressiveImage from "../components/ProgressiveImage";
import AdvertPanel from "../components/AdvertPanel";
// import { Linking } from 'expo';


const thumb = "https://img.imageboss.me/width/200/quality:40/";
const { height, width } = Dimensions.get("window");


const EventDetail = ({ navigation }) => {
  const event = navigation.getParam("event");


  return (
    <View style={styles.container}>
      {/* <ScrollView> */}
      <Text style={styles.monthname}>{event.name}</Text>
      <View style={styles.rect111}>
        <FlatList
          keyExtractor={item => item.id.toString()}
          data = {event.images}
          showsHorizontalScrollIndicator={false}
          // key = {}
          horizontal = {true}
          renderItem= {()=>{
            return <ProgressiveImage
            thumbnailSource={{ uri: thumb + event.images[0].image }}
            style={{
              // height: width - 100,
              width: width - 60,
              resizeMode: "contain",
              borderRadius: 17,
            }}
            source={event && { uri: event.images[0].image }}
          />
          }}
        
        >

        </FlatList>
      </View>
      <View style={styles.scrollArea}>
        <ScrollView
          horizontal={false}
          contentContainerStyle={styles.scrollArea_contentContainerStyle}>

          {event.description && <View style={styles.textWrapper}>
            <Text style={styles.eventDescription}>
              {event.description}
            </Text>
          </View>}
          
          {/* TODO: refator these into one component that takes in props and cleanup unused styles */}
          {<View  style={styles.textWrapper}>
            <EntypoIcon name="calendar" style={styles.icon5}></EntypoIcon>
            <Text style={styles.thecapitalhub2}>{new Date(event.date).toDateString()}
            </Text>
          </View>}
          
          {event.address && <TouchableOpacity 
            onPress ={()=>Linking.openURL(`comgooglemaps://:${event.address}`)}
            style={styles.textWrapper}>
            <EntypoIcon name="location-pin" style={styles.icon5}></EntypoIcon>
            <Text style={styles.thecapitalhub2}>{event.address}
            </Text>
          </TouchableOpacity>}

          {event.phone && <TouchableOpacity
            onPress ={()=>Linking.openURL(`tel:${event.phone}`)}
            style={styles.textWrapper}>
            <IoniconsIcon name="ios-call" style={styles.icon5}></IoniconsIcon>
            <Text style={styles.thecapitalhub2}>{event.phone}</Text>
          </TouchableOpacity>}

          {event.url && <TouchableOpacity 
            onPress ={()=>Linking.openURL(event.url.includes('http') ? event.url : ('http://'+ event.url) )}
            style={styles.textWrapper}>
            <EntypoIcon name="link" style={styles.icon5}></EntypoIcon>
            <Text style={styles.thecapitalhub2}>Visit Website</Text>
          </TouchableOpacity>}

          {event.ig_handle && <TouchableOpacity
            onPress ={()=>Linking.openURL(`https://instagram.com/${event.ig_handle}`)}
            style={styles.textWrapper}>
            <EntypoIcon name="instagram" style={styles.icon5}></EntypoIcon>
            <Text style={styles.thecapitalhub2}>{event.ig_handle}</Text>
          </TouchableOpacity>}

        </ScrollView>
        
      </View>
      <AdvertPanel></AdvertPanel>
      {/* </ScrollView> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(49,49,49,1)",
    justifyContent: 'space-around'
  },
  monthname: {
    textAlign: 'center',
      color: "white",
      // color: "rgba(0,0,0,1)",
      fontSize: 20,
      fontFamily: "Futura-Medium",
      marginTop:10,
  },
  textWrapper: {
    // padding: wp("2%"),
    flex: 1,
    flexDirection: "row",
    marginLeft: 20,
  },

  scrollArea: {
    // width: 400,
    // height: hp("30%"),
    // alignContent: 'flex-end',
    // alignSelf: 'flex-end',
    justifyContent: 'flex-end',
    backgroundColor: 'pink',
    backgroundColor: "rgba(49,49,49,1)",
    elevation: 150,
    shadowOffset: {
      height: 5,
      width: 5,
    },
    shadowColor: "rgba(0,0,0,1)",
    shadowOpacity: 0.01,
    shadowRadius: 50,
    // flex:xs 1,
    // marginTop: 541,
    // marginLeft: -14
  },
  scrollArea_contentContainerStyle: {
    // flex:1,
    // backgroundColor: 'red',
    // alignSelf: ' flex-end'
    // width: 400,
    // height: hp("50%"),
    // flexDirection: "column"
  },
  info: {
    top: 0,
    left: 8,
    width: 357,
    height: 107,
    position: "absolute",
  },
  endWrapperFiller: {
    flex: 1,
  },
  theAtriumSale: {
    color: "rgba(0,0,0,1)",
    fontSize: 10,
    fontFamily: "Futura-Medium",
    marginBottom: 389,
    marginLeft: 21,
  },
  eventDescription: {
    color: "rgba(255,255,255,1)",
    fontSize: 15,
    fontFamily: "Futura-Medium",
    marginBottom: 17,
    padding: wp("4%"),
  },
  thecapitalhub2: {
    color: "rgba(255,255,255,1)",
    fontSize: 15,
    fontFamily: "Futura-Medium",
    marginBottom: 5,
    marginLeft: wp("4%"),
  },
  event1: {
    color: "rgba(255,255,255,1)",
    fontSize: 20,
    fontFamily: "Futura-Medium",
    marginBottom: 12,
  },
  thecapitalhub1: {
    color: "rgba(255,255,255,1)",
    fontSize: 15,
    fontFamily: "Futura-Medium",
    marginBottom: 13,
    marginLeft: wp("4%"),
  },
  thecapitalhub: {
    color: "rgba(255,255,255,1)",
    fontSize: 15,
    fontFamily: "Futura-Medium",
    marginLeft: wp("4%"),
  },
  theAtriumSaleColumn: {
    width: 357,
  },
  icons2: {
    top: 28,
    // left: 0,
    width: 24,
    height: 83,
    position: "absolute",
  },
  icon6: {
    color: "rgba(255,255,255,1)",
    fontSize: 23,
  },
  icon5: {
    color: "rgba(255,255,255,1)",
    fontSize: 18,
    // marginTop: 8,
    // marginLeft: 3
  },
  icon7: {
    color: "rgba(255,255,255,1)",
    fontSize: 23,
    marginTop: 6,
    marginLeft: 1,
  },
  infoStack: {
    width: 365,
    height: 111,
    marginTop: 29,
    marginLeft: 32,
  },
  rect3: {
    width: 375,
    height: 101,
    backgroundColor: "rgba(230, 230, 230,1)",
    marginTop: 30,
    marginLeft: 14,
  },
  icon: {
    width: 215,
    height: 32,
    flexDirection: "row",
    marginTop: 30,
    marginLeft: 76,
  },
  icon1: {
    color: "rgba(0,0,0,1)",
    fontSize: 29,
  },
  icon2: {
    color: "rgba(255,255,255,1)",
    fontSize: 29,
    marginLeft: 72,
  },
  icon3: {
    color: "rgba(0,0,0,1)",
    fontSize: 32,
    marginLeft: 67,
  },
  icon1Row: {
    height: 32,
    flexDirection: "row",
    flex: 1,
    marginRight: 8,
  },

  image1: {
    width: wp("15%"),
    height: hp("12%"),
    marginTop: hp("1%"),
    marginBottom: hp("4%"),
    alignSelf: "center",
  },
  button4: {
    alignSelf: "center",
    alignItems: "center",
    width: wp("40%"),
    height: hp("8%"),
    backgroundColor: "rgba(0,0,0,1)",
    elevation: 150,
    borderRadius: 19,
    borderColor: "#000000",
    borderWidth: 0,
    shadowOffset: {
      height: 5,
      width: 5,
    },
    shadowColor: "rgba(0,0,0,1)",
    shadowOpacity: 0.01,
    shadowRadius: 50,
    flexDirection: "row",
  },
  icon5Row: {
    flexDirection: "row",
    flex: 1,
    alignSelf: "center",
    justifyContent: "space-around",
    paddingHorizontal: wp("2%"),
  },
  icon5: {
    color: "rgba(255,255,255,1)",
    fontSize: hp("2%"),
  },
  selectHere: {
    color: "rgba(255,255,255,1)",
    fontFamily: "Futura-Medium",
    alignSelf: "center",
    fontSize: hp("3%"),
  },
  rect111: {
    // flex: 1,
    height: hp("20%"),
    alignSelf: 'center',
    backgroundColor: "grey",
    // marginRight: hp("2%"),
    // marginLeft: hp("2%"),
    borderRadius: 17,
    // marginBottom: hp("3%"),
    // marginTop: hp("2%"),
  },

  button3Row: {
    flexDirection: "row",
    flex: 1,
    alignItems: "center",
    alignSelf: "center",
  },
  button3: {
    width: wp("30%"),
    height: hp("8%"),
    backgroundColor: "rgba(218,218,218,1)",
    borderRadius: 17,
    alignSelf: "center",
  },
  button2: {
    width: wp("30%"),
    height: hp("8%"),
    backgroundColor: "rgba(218,218,218,1)",
    borderRadius: 17,
    alignSelf: "center",
  },
  going2: {
    color: "black",
    fontSize: hp("5%"),
    fontFamily: "FuturaBT-Medium",
    // letterSpacing: -3,
    alignSelf: "center",
    flex: 1,
  },
  not2: {
    color: "rgba(255,255,255,1)",
    fontSize: hp("5%"),
    fontFamily: "FuturaBT-Medium",
    // letterSpacing: -3,
    alignSelf: "center",
    flex: 1,
  },

  rect2: {
    width: wp("1%"),
    height: hp("1%"),
    backgroundColor: "rgba(0,0,0,1)",
    borderColor: "rgba(0,0,0,1)",
    borderWidth: 0,
    alignSelf: "center",
    alignItems: "center",
  },

  rect6: {
    height: hp("10%"),
    backgroundColor: "rgba(230, 230, 230,1)",
    alignSelf: "center",
    flex: 1,
    marginTop: hp("4%"),
    borderRadius: 7,
    marginTop: hp("2%"),
  },

  rect2: {
    width: wp("1%"),
    height: hp("1%"),
    backgroundColor: "rgba(0,0,0,1)",
    borderColor: "rgba(0,0,0,1)",
    borderWidth: 0,
    alignSelf: "center",
    alignItems: "center",
  },
  icon2Row: {
    flexDirection: "row",
    flex: 1,
    height: hp("9%"),
    alignSelf: "center",
    justifyContent: "space-around",
  },
  icons: {
    height: hp("9%"),
    flexDirection: "row",
    alignSelf: "center",
    flex: 1,
  },
  icon2: {
    color: "rgba(255,255,255,1)",
    fontSize: hp("5%"),
  },
  icon3: {
    color: "rgba(0,0,0,1)",
    fontSize: hp("5%"),
  },
  icon6: {
    color: "rgba(0,0,0,1)",
    fontSize: hp("5%"),
  },
});

export default EventDetail;
