import React, { Component } from "react";
import { StyleSheet, View, Text, Image, ScrollView, Dimensions } from "react-native";
import Svg, { Ellipse } from "react-native-svg";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen'

const LandingPage = (props) => {

// function Untitled(props) {
  return (<>
    <View style={styles.container}> 
      <Image
            source={require("../assets/images/guide_logo.png")}
            resizeMode="contain"
            style={styles.imagef}
        ></Image>
      <Text style={styles.welcomeText}>welcome</Text>
      <View style={styles.lineUnderWelcome}/>
      <View style={{flex:1,justifyContent: "center",alignItems: "center"}}>
        <Text style={styles.findEvents}>find{"\n"}events{"\n"}happening{"\n"}near you. </Text>
      </View>
        <Text style={styles.hgGuidef}>hg guide</Text>
    </View>
    
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    // backgroundColor: 'red',
    flex: 1,
    alignItems: 'center',
    // width: wp('100%'),
    // justifyContent: 'center'
  },

  findEvents: {
    // backgroundColor: 'red',
    color: "#121212",
    fontSize: Dimensions.get('window').width/7,
    fontFamily: "FuturaBT-Medium",
  },
 
  
  hgGuide: {
    backgroundColor: 'pink',
    color: "#121212",
    opacity: 0.06,
    fontSize: 103,
    fontFamily: "Futura-Medium",
    letterSpacing: -5,
        alignSelf: 'center',

    // marginTop: -155
  },
  image1: {
    width: 44,
    height: 49,
    marginTop: -609,
    marginLeft: 168
  },
  
  welcome1: {
    color: "#121212",
    fontSize: hp('2.2%'),
    // width: wp('10%'),
    // height: hp('18%'),
    fontFamily: "FuturaBT-Medium",
    // letterSpacing: -3,
    // marginTop: 166,
    // marginLeft: 72
    alignSelf: 'center',
    // justifyContent: 'center'
  },
  rectColumn: {
    // marginTop: 71,
    // marginLeft: -2,
    // marginRight: 59
  },
  rectColumnFiller: {
    flex: 1
  },
  welcome: {
    color: "#121212",
    fontSize: hp('3%'),
    fontFamily: "fugaz-one-regular",
    width: wp('40%'),
    height: hp('30%'),
    alignSelf: 'center'
  },
  imagef: {
    width: wp('10%'),
    height: hp('10%'),
    alignSelf: 'center',
    marginTop: hp('4%'),
    },
    welcomeText: {
      color: "#121212",
      marginTop: 20,
      fontSize: hp('4%'),
      fontFamily: "Futura-book",
      width: wp('40%'),
      // height: hp('30%'),
      textAlign: 'center',
      // justifyContent: 'center'
    },
    lineUnderWelcome: {
      marginTop: 15,
      width: wp('20%'),
      height: hp('0.3%'),
      backgroundColor: "rgba(0,0,0,1)",
      opacity: 0.9,
      alignItems: 'center'
    },
    rectf: {
      // width: wp('40%'),
      // height: hp('0.3%'),
      // backgroundColor: "rgba(0,0,0,1)",
      // opacity: 0.9,
      alignSelf: 'center'
  
    },
    
    welcome1f: {
      color: "#121212",
      fontSize: hp('4.0%'),
      // width: wp('10%'),
      // height: hp('18%'),
      fontFamily: "FuturaBT-Medium",
      // letterSpacing: -3,
      marginTop: hp('6%'),
      // marginLeft: 72
      alignSelf: 'center',
      // justifyContent: 'center'
    },
    hgguideContainer: {
      backgroundColor: 'yellow',
      flex: 1,
      alignContent: 'flex-end',
      alignSelf: 'flex-end',

    },
    hgGuidef: {
      textAlign: 'center',
      alignSelf: 'flex-end',
      color: "#121212",
      opacity: 0.06,
      fontSize: Dimensions.get('window').width/4.1,
      fontFamily: "Futura-book",
      // letterSpacing: -5,
      width: wp('100%'),
      marginBottom: 20,
    },
    ellipse2Row: {
      height: 12,
    flexDirection: "row",
    marginTop: hp('30%'),
    marginLeft: 86,
    marginRight: 78,    
    alignSelf: 'center'
    },
    ellipse2: {
      width: wp('4%'),
      height: hp('3%'),
      // alignSelf: 'center' 
    },
    ellipse1: {
      width: 12,
      height: 12,
      marginLeft: 12
    },
});

export default LandingPage;
