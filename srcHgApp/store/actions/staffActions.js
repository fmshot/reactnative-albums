import {
  GET_PAYSLIP,
  GET_PAYSLIP_SUCCESS,
  GET_BANKS,
  GET_BANKS_SUCCESS,
  ADD_BANK,
  ADD_BANK_SUCCESS,
  EDIT_BANK,
  EDIT_BANK_SUCCESS,
  DELETE_BANK,
  DELETE_BANK_SUCCESS,
  GET_STAFF,
  GET_PFAS_SUCCESS,
  GET_PFAS,
  ADD_PFA,
  ADD_PFA_SUCCESS,
  EDIT_PFA,
  EDIT_PFA_SUCCESS,
  DELETE_PFA,
  DELETE_PFA_SUCCESS,
  GET_SCHOOL_BRANCH_SUCCESS,
  GET_SCHOOL_BRANCH,
  ADD_SCHOOL_BRANCH,
  ADD_SCHOOL_BRANCH_SUCCESS,
  EDIT_SCHOOL_BRANCH,
  EDIT_SCHOOL_BRANCH_SUCCESS,
  DELETE_SCHOOL_BRANCH,
  DELETE_SCHOOL_BRANCH_SUCCESS,
  ADD_STAFF,
  ADD_STAFF_SUCCESS,
  ADD_BANK_ACCOUNT,
  ADD_BANK_ACCOUNT_SUCCESS,
  DELETE_STAFF,
  DELETE_STAFF_SUCCESS,
  FETCH_SUCCESS,
  FETCH_PROCESS,
  FETCH_FAILURE,
  EDIT_STAFF_SUCCESS,
  LOGIN_STARTasLOGIN_PROCESS,
  EDIT_VARADJ,
  EDIT_VARADJ_SUCCESS,
  EDIT_STAFF,
  ADD_VARADJ,
  ADD_VARADJ_SUCCESS,
  DELETE_VARADJ,
  DELETE_VARADJ_SUCCESS,
  GET_VARADJ,
  GET_VARADJ_SUCCESS,
  GET_VARADJ_TYPE,
  GET_VARADJ_TYPE_SUCCESS,
  ADD_VARADJ_TYPE,
  ADD_VARADJ_TYPE_SUCCESS,
  EDIT_VARADJ_TYPE,
  EDIT_VARADJ_TYPE_SUCCESS,
  DELETE_VARADJ_TYPE,
  DELETE_VARADJ_TYPE_SUCCESS
} from './constants';
import { endpoints } from '../../services/esteemAPI';

function initialDispatch(dispatch, actionType) {
  dispatch({
    type: actionType,
    payload: { loading: true, error: false, errorMesssage: null }
  });
}

function onSuccess(dispatch, actionType, newObject, status = null) {
  dispatch({
    type: actionType,
    payload: {
      ...newObject,
      status,
      loading: false,
      error: false,
      errorMesssage: null
    }
  });
}

function onDeleteSuccess(dispatch, actionType, status) {
  dispatch({
    type: actionType,
    payload: {
      status,
      loading: false,
      error: false,
      errorMesssage: null
    }
  });
}

function onFailure(dispatch, actionType, errorMesssage, status = null) {
  dispatch({
    type: actionType,
    payload: { loading: false, error: true, errorMesssage, status }
  });
}

export const actions = {
  getAllStaff: async dispatch => {
    initialDispatch(dispatch, FETCH_PROCESS);
    const { rawResponse, content } = await endpoints.getAllStaff();

    if (rawResponse.ok) {
      onSuccess(dispatch, FETCH_SUCCESS, { staff: content });
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

  editStaff: async (dispatch, staffObj) => {
    initialDispatch(dispatch, EDIT_STAFF);
    const { rawResponse } = await endpoints.editStaff(staffObj);
    const content = await rawResponse.json();
    const { status, ok } = rawResponse;

    if (ok) {
      onSuccess(dispatch, EDIT_STAFF_SUCCESS, { staff: content }, status);
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

  deleteStaff: async (dispatch, staffObj) => {
    initialDispatch(dispatch, DELETE_STAFF);
    const { rawResponse } = await endpoints.deleteStaff(staffObj);
    const { status, ok } = rawResponse;
    if (ok) {
      onDeleteSuccess(dispatch, DELETE_STAFF_SUCCESS, status);
    } else {
      onFailure(dispatch, FETCH_FAILURE, rawResponse, status);
    }
  },

  addStaff: async (dispatch, staffObj) => {
    initialDispatch(dispatch, ADD_STAFF);
    const { rawResponse } = await endpoints.addStaff(staffObj);
    const content = await rawResponse.json();
    const { status, ok } = rawResponse;

    if (ok) {
      onSuccess(dispatch, ADD_STAFF_SUCCESS, { newstaff: content }, status);
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

};
