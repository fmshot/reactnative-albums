const BaseURL = 'https://hgguide.yedite.ch/';
// import { Constants } from 'expo-constants';
// const BaseURL = 'http://127.0.0.1:8000/';
// const token = localStorage.getItem('token');

const headers =   {
  Accept: 'application/json',
'Content-Type': 'application/json',
}

// const headers = token
//   ? {
//       Accept: 'application/json',
//       'Content-Type': 'application/json',
//       Authorization: `Token ${token}`
//     }
//   : {
//       Accept: 'application/json',
//       'Content-Type': 'application/json'
//     };

const postReq = async (url, body) => {
  body = JSON.stringify(body);
  const rawResponse = await fetch(BaseURL + url, {
    method: "POST",
    headers,
    body,
  });
  // const content = await rawResponse.json();
  return { rawResponse };
};

const getReq = async url => {
  const rawResponse = await fetch(BaseURL + url, { headers });
  const content = await rawResponse.json();
  return { rawResponse, content };
};

export const endpoints = {

  getAllEvents: async (state) => {
    try {
      url = state ? `api/event?state=${state}` : 'api/event/'
      const response = await getReq(url);
      return response;
    } 
    catch (error) {
      return error
    }
    
  },
  
  
  getAllStates: async () => {
    try {
      const response = await getReq('api/state/');
      return response;
    } 
    catch (error) {
      return error
    }
    
  },

  getAdvert: async () => {
    try {
      const response = await getReq('api/adverts/');
      return response;
    } 
    catch (error) {
      return error
    }
    
  },
  getAttendanceForPhone: async (phoneid) => {
    try {
      const response = await getReq(`api/going/?phone_identifier=${phoneid}`);
      return response;
    } 
    catch (error) {
      return error
    }
    
  },
  
  markAttendance: async (event, attendance, phoneid) => {
    try {
      const response = await postReq('api/going/', {event: event.id, attend: attendance, phone_identifier: phoneid});
      return response;
    } 
    catch (error) {
      return error
    }
    
  },

}