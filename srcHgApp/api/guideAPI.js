import axios from 'axios';
import { AsyncStorage } from 'react-native';

let url;
if (__DEV__) {
  // url = 'http://127.0.0.1:8000/fixrapi';
  url = 'https://hgguide.yedite.ch/api';
} else {
  url = 'https://hgguide.yedite.ch/api';
  // url = 'https://sleepy-savannah-10606.herokuapp.com';
}






const instance = axios.create({
  baseURL: url
});

instance.interceptors.request.use(
  async config => {
    const token = await AsyncStorage.getItem('token');
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  err => {
    return Promise.reject(err);
  }
);

export default instance;
