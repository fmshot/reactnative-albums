import React from "react";
import { StyleSheet, View, TouchableOpacity, Text, Image, ScrollView, TextInput, Dimensions} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen'
import { AsyncStorage } from 'react-native';
import createDataContext from './createDataContext';
import { endpoints as api } from '../api/guide_api';
import { localstore } from '../api/localstorage';
// import Constants from 'expo-constants';

const eventsReducer = (state, action) => {
  switch (action.type) {
    case 'get_events':
      return  {...state, events: action.payload}
    case 'get_states':
      return  {...state, states: action.payload}
    case 'get_advert':
      return  {...state, advert: action.payload}
    case 'mark_attendance':
      return  {...state, attendance: action.payload}
    case 'get_attendance_for_phone':
      return  {...state, attendanceForPhone: action.payload}
    case 'choose_state':
      return  {...state, chosenState: action.payload}
    default:
      return state;
  }
};


const getEvents = dispatch => async (state) => {
    const token = await AsyncStorage.getItem('token');

    const response = await api.getAllEvents(state)
    dispatch({ type: 'get_events' , payload: response.content.results});
}

const getAdvert = dispatch => async (state) => {
    const token = await AsyncStorage.getItem('token');

    const response = await api.getAdvert()
    dispatch({ type: 'get_advert' , payload: response.content.results[0]});
}

const getAttendanceForPhone = dispatch => async () => {
    const response = await api.getAttendanceForPhone(Constants.installationId)
    dispatch({ type: 'get_attendance_for_phone' , payload: response.content.results});
}

const markAttendance = dispatch => async (event, attend, phoneid) => {
    //this has alot of data storage within the phone.
    const token = await AsyncStorage.getItem('token');

    const response = await api.markAttendance(event, attend, Constants.installationId)
    console.log(Constants.installationId)
    dispatch({ type: 'mark_attendance' , payload: response.content});
}

const chooseState = dispatch => async (state) => {
    dispatch({ type: 'choose_state' , payload: state});
}

const getStates = dispatch => async (id) => {
    const token = await AsyncStorage.getItem('token');

    const response = await api.getAllStates(1)
    
    response.content.results.map(state=>{
      state.component=<View style={styles.button4}>
                  <Text style={styles.selectHere}>{state.name}</Text>
                </View>
              
      state.label = state.name
      state.key = state.id
    })
    // console.log(response)
    dispatch({ type: 'get_states' , payload: response.content.results});
}



export const { Provider, Context } = createDataContext(
    eventsReducer,
  { getEvents, getStates, getAdvert, markAttendance, getAttendanceForPhone,chooseState },
  []
);


const styles = StyleSheet.create({
  
  button4: {
    alignSelf: 'center',
    alignItems: 'center',
    alignContent:'center' , 
    justifyContent: 'center', 
    width: wp('40%'),
    height: hp('7%'),
    backgroundColor: "rgba(0,0,0,1)",
    elevation: 150,
    borderRadius: 19,
    borderColor: "#000000",
    borderWidth: 0,
    shadowOffset: {
      height: 5,
      width: 5
    },
    shadowColor: "rgba(0,0,0,1)",
    shadowOpacity: 0.01,
    shadowRadius: 50,
    flexDirection: "row"
  },
  icon5Row: {
    alignContent:'center' , 
    justifyContent: 'center', 
  },
  selectHere: {
    color: "rgba(255,255,255,1)",
    fontFamily: "Futura-Medium",
    textAlign: 'center',
    fontSize: hp('2%'), 
  },
})