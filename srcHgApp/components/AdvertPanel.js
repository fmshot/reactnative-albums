import React from 'react'
import { StyleSheet, Text, View, Dimensions, TouchableWithoutFeedback } from 'react-native'
import { useContext, useEffect } from 'react'
import { useState } from 'react';
import ProgressiveImage from './ProgressiveImage';
import {Context} from '../context/eventsContext'
// import { Linking } from 'expo';

const AdvertPanel = ({advert}) => {
    const { state, getAdvert } = useContext(Context)
    const {height, width} = Dimensions.get('window')
    const thumb = 'https://img.imageboss.me/width/200/quality:40/'
    useEffect (()=>{ getAdvert() },[])


    return (
        <TouchableWithoutFeedback onPress ={()=>Linking.openURL(state.advert.url)}>
          <View style={{
            // marginTop: 10,
            flex: 1,
            justifyContent: 'flex-end',
            height: 60, width: width,
        //   backgroundColor: "#ffffff",
        //   shadowColor: "#2d2d2d",
        //   shadowOffset: {
        //       width: 0,
        //       height: -3
        //   },
          shadowRadius: 20,
          shadowOpacity: 0.5 }}>
              <ProgressiveImage
                thumbnailSource={state.advert && { uri: thumb+state.advert.image }}
                style={{height: 60, resizeMode: "cover", alignSelf: 'flex-end', }}
                source={state.advert && {uri: state.advert.image} } />
                </View>
          </TouchableWithoutFeedback>
    )
}

export default AdvertPanel

const styles = StyleSheet.create({})
