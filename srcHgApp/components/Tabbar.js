import React from 'react'
import { View, Text , StyleSheet} from 'react-native'
import OcticonsIcon from "react-native-vector-icons/Octicons";
import SimpleLineIconsIcon from "react-native-vector-icons/SimpleLineIcons";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";
import EntypoIcon from "react-native-vector-icons/Entypo";

import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
  } from 'react-native-responsive-screen'
  
const Tabbar = () => {
    return (
        <View style={styles.rect6}>
        <View style={styles.icons}>
          <View style={styles.icon2Row}>
            <OcticonsIcon name="home" style={styles.icon2}></OcticonsIcon>
            <SimpleLineIconsIcon
              name="calendar"
              style={styles.icon3}
            ></SimpleLineIconsIcon>
            <MaterialCommunityIconsIcon
              name="circle-slice-5"
              style={styles.icon6}
            ></MaterialCommunityIconsIcon>
          </View>
        </View>
      </View>
     
    )
}

const styles = StyleSheet.create({
    container: {
  // flex: 1,
  borderRadius: 10,
      borderWidth: 2,
      borderColor: 'black',
  
    },
    image1: {
      width: wp('15%'),
      height: hp('12%'),
      marginTop: hp('1%'),
      marginBottom: hp('4%'),    
      alignSelf: 'center'
    },
    button4: {
      alignSelf: 'center',
      alignItems: 'center',
      width: wp('40%'),
      height: hp('8%'),
      backgroundColor: "rgba(0,0,0,1)",
      elevation: 150,
      borderRadius: 19,
      borderColor: "#000000",
      borderWidth: 0,
      shadowOffset: {
        height: 5,
        width: 5
      },
      shadowColor: "rgba(0,0,0,1)",
      shadowOpacity: 0.01,
      shadowRadius: 50,
      flexDirection: "row"
    },
    icon5Row: {
      flexDirection: "row",
      flex: 1,
      alignSelf: 'center',
      justifyContent: 'space-around',
      paddingHorizontal: wp('2%')
    },
     icon5: {
      color: "rgba(255,255,255,1)",
      fontSize: hp('4%'), 
    },
    selectHere: {
      color: "rgba(255,255,255,1)",
      fontFamily: "fugaz-one-regular",
      alignSelf: "center",
      fontSize: hp('3%'), 
    },
    rect111:{
      height: hp('50%'),
      backgroundColor: "grey",
      marginRight: hp('2%'),
      marginLeft: hp('2%'),
      borderRadius: 17,
      marginBottom: hp('3%'),
      marginTop: hp('2%'),
    },
  
    button3Row: {
      flexDirection: "row",
      flex: 1,
      alignItems: 'center',
      alignSelf: 'center',
     
    },
    button3: {
      width: wp('30%'),
      height: hp('8%'),
      backgroundColor: "rgba(218,218,218,1)",
      borderRadius: 17,
      alignSelf: 'center',
    },
    button2: {
      width: wp('30%'),
      height: hp('8%'),
      backgroundColor: "rgba(218,218,218,1)",
      borderRadius: 17,
      alignSelf: 'center', 
   },
    going2: {
      color: "black",
      fontSize: hp('5%'),
      fontFamily: "FuturaBT-Medium",
      // letterSpacing: -3,
      alignSelf: 'center',
      flex: 1
    },
    not2: {
      color: "rgba(255,255,255,1)",
      fontSize: hp('5%'),
      fontFamily: "FuturaBT-Medium",
      // letterSpacing: -3,
      alignSelf: 'center',
      flex: 1
    },
    
    rect6: {
      height: hp('10%'),
      backgroundColor: "rgba(230, 230, 230,1)",
      alignSelf: 'center',
      flex: 1,
      marginTop: hp('4%'),
      borderRadius: 7,
      marginTop: hp('2%'),
    },
  
    rect2: {
      width: wp('1%'),
      height: hp('1%'),
      backgroundColor: "rgba(0,0,0,1)",
      borderColor: "rgba(0,0,0,1)",
      borderWidth: 0,
      alignSelf: 'center',
      alignItems: 'center'
    },
    icon2Row: {
      flexDirection: "row",
      flex: 1,
      height: hp('9%'),
      alignSelf: 'center',
      justifyContent: 'space-around'
    }, 
    icons: {
      height: hp('9%'),
      flexDirection: "row",
      alignSelf: 'center',
      flex: 1,
    },
    icon2: {
      color: "rgba(255,255,255,1)",
      fontSize: hp('5%'),
    },
    icon3: {
      color: "rgba(0,0,0,1)",
      fontSize: hp('5%'),
    },
    icon6: {
      color: "rgba(0,0,0,1)",
      fontSize: hp('5%'),
    },
   
  });

export default Tabbar
