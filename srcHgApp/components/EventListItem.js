import React, { Component } from "react";

import { View, Text, ScrollView, StyleSheet } from 'react-native'

const EventListItem = () => {
    return (
        <View style={styles.container}>
        <View style={styles.scrollArea3}>
            <Text style={styles.eventName}>atrium sale</Text>
        </View>
      </View>
    );
  }


export default EventListItem
  
const styles = StyleSheet.create({
    container: {
      marginTop: 20,
    //   width: 417,
      height: 156
    },
    eventName: {
        textAlign: 'center',
        color: "#121212",
        opacity: 0.95,
        fontSize: 20,
        fontFamily: "Futura-Medium",
      },
    scrollArea3: {
        // flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    //   width: 415,
      height: 52,
      backgroundColor: "rgba(255,255,255,1)",
      elevation: 150,
      borderRadius: 17,
      shadowOffset: {
        height: 5,
        width: 5
      },
      shadowColor: "rgba(0,0,0,1)",
      shadowOpacity: 0.24,
      shadowRadius: 50,
    //   marginTop: 104
    },
   
  });

