import React from "react";
import { StyleSheet, View, ScrollView, Text, TouchableOpacity } from "react-native";

import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen'
import ProgressiveImage from "./ProgressiveImage";



// function to test for even or odd
const isOdd = (x) =>  x & 1

var date_diff_indays = (date1, date2) => {
  dt1 = new Date(date1);
  dt2 = new Date(date2);
  return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
}
  
export default function CountdownItem({ navigation, item,  index }) {
  const date = new Date(item.date)
  const days_to_go = date_diff_indays(Date.now(), date)
  const text_to_display = (days_to_go > 1 ? 'days' : (days_to_go == 0 ? 'today' : 'day'))
  const thumb = 'https://img.imageboss.me/width/200/quality:40/'

  return (
    <TouchableOpacity onPress={()=>navigation.navigate('EventDetail', {event: item})}y>
    <View   style={[isOdd(index) ? styles.rect111 : styles.rect2]}>
      <Text style={styles.countdownText}>{`${days_to_go} ${text_to_display}`}</Text>    
      <ProgressiveImage 
        thumbnailSource = {{uri: thumb + item.images[0].image}}
        style={{ height: hp('15%'), resizeMode: "cover", justifyContent:'center', borderRadius: 17 }}
        source = {{uri: item.images[0].image}}
      />
    <Text style={styles.countdownText}>{days_to_go === 0 ? "" : days_to_go} {text_to_display}</Text>   
   </View>
   </TouchableOpacity>
  )
}



const styles = StyleSheet.create({
    rect111:{
      height: hp('15%'),
      backgroundColor: "rgba(255,255,255,1)",
      marginRight: hp('-2%'),
      marginLeft: hp('2%'),
      borderRadius: 17,
      // marginBottom: hp('3%'),
      marginTop: hp('2%'),
      elevation: 10,
      borderRadius: 17,
      shadowRadius: 20,
      shadowOpacity: 0.5 
    },

    rect2:{
      height: hp('15%'),
      backgroundColor: "rgba(255,255,255,1)",
      marginRight: hp('2%'),
      marginLeft: hp('-2%'),
      borderRadius: 17,
      // marginBottom: hp('3%'),
      marginTop: hp('2%'),
      elevation: 10,
      borderRadius: 17,
      shadowRadius: 20,
      shadowOpacity: 0.5 
    },
    countdownText: {
      elevation: 10,
      shadowRadius: 20,
      shadowOpacity: 0.5,
      position: 'absolute',
      top: 0,
      bottom: 0,
      left: 0,
      right : 0,
      padding: hp('5.5%'),
      fontSize: hp('4%'),
      fontFamily: "FuturaBT-Medium",
      color: 'white',
      backgroundColor: 'black',
      opacity : 0.8,
      // padding: 20,
      
    },
    item: {
      padding: hp('5.5%'),
      // marginTop: 16,
      // justifyContent: 'center',
      // alignSelf: 'center'
    }
  });