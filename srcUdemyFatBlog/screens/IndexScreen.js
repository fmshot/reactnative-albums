import React, { Component, useContext } from "react";
import { StyleSheet, View, Text, FlatList, Button, TouchableOpacity, Image, ScrollView, TextInput } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
// import ResultsDetail from './ResultDetail';
import { Context as BlogContext } from '../context/BlogContext';


// import { withNavigation } from 'react-navigation';


const IndexScreen = () => {

    // const blogPosts = useContext(BlogContext);

        const { state, addBlogPost } = useContext(BlogContext);

   
    return(
        <View>
            <Text style={styles.title}>dsdss</Text>
            <Button title="Add Post" onPress={addBlogPost}/>
            <FlatList
            // data={blogPosts}
            data={state}
            keyExtractor={blogPost => blogPost.title}
            renderItem={({ item }) => {
                return <Text>{item.title}</Text>
            }}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    title: {
        fontSize: 18,
        fontWeight: 'bold'
    },
});

export default IndexScreen;