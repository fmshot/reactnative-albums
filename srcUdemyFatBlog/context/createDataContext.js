import React, { useReducer } from 'react';

export default (reducer, actions, initialState) => {
    const BlogContext =  React.createContext();

    const Provider = ({ children }) => {
        const [state, dispatch] = useReducer(reducer, initialState);

        // actions === { addBlogPost: {dispatch} => { return } () => {} }

        const boundActions = {};
        for (let key in actions ) {
            // key === 'addBlogPost'
            boundActions[key] = actions[key](dispatch);
        }

        return (
        <BlogContext.Provider value={{ state, ...boundActions }}>
        {children}
        </BlogContext.Provider>
        )
    };

    return ( BlogContext, Provider);
};