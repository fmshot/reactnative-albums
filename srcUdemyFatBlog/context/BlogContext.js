// for use state

// import React, { Component, useState } from "react";
// import { StyleSheet, View, Text, FlatList, TouchableOpacity, Image, ScrollView, TextInput } from "react-native";


// const BlogContext = React.createContext();

//  export const BlogProvider = ({ children }) => {
//     //  const blogPosts = [
//     //      { title: 'Blog Post #1'},
//     //      { title: 'Blog Post #2'},
//     //  ];

//     const [blogPosts, setBlogPosts] = useState([]);

//     const addBlogPost = () => {
//         setBlogPosts([
//             ...blogPosts,
//             { title: `Blog Post #${blogPosts.length + 1}` }
//         ]);
//     };

//     return  <BlogContext.Provider value={{ data: blogPosts, addBlogPost }}>{children}</BlogContext.Provider>
// };

// export default BlogContext;

// End for useState



// for useReducer

import createDataContext from './createDataContext';


const blogReducer = (state, action) => {
    switch (action.type) {
        case 'add_blogpost':
          return [
            ...state,
            { title: `Blog Post #${state.length + 1}` }
            ];

        default:
          return state;
      }
};

const addBlogPost = (dispatch) => {
    return () => {
        dispatch({ type: 'add_blogpost' });
    };
};

export const { BlogContext, Provider } = createDataContext( 
    blogReducer,
     { addBlogPost },
     [],
    );