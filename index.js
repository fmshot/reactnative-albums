// // /**
// //  * @format
// //  */

// // import {AppRegistry} from 'react-native';
// // import App from './App';
// // import {name as appName} from './app.json';

// // AppRegistry.registerComponent(appName, () => App);

// // import {AppRegistry} from 'react-native';





//                 // INDEX FOR AYAHAY APP


// // Import a library to help create a component
// import React from "react"; 
// import {AppRegistry, View, style } from 'react-native';
// import {
//   heightPercentageToDP as hp,
//   widthPercentageToDP as wp,
// } from 'react-native-responsive-screen'


// import Header from './src/components/Header';
// import AlbumlList from './src/components/AlbumList';
// // import Navigator from './HomeStack';
// // import Navigator from './AppAyahay';
// // import Navigator from './AppHgGUIDE';
// // import Navigator from './AppMotorfxr';
// // import Navigator from './AppUdemyRestuarant';
// // import Navigator from './AppUdemyBlog';
// import { BlogProvider } from './AppUdemyBlog'


// // import Navigator from './HomestackAahay';

// // Create a component 
// const App = () =>  (
//   <View style={{ flex: 1 }}>
//         {/* <Header headerText={'My Albums'} />
//         <AlbumlList /> */}
//         <BlogProvider />
//   </View>
//     );

// // Render it to the device
// AppRegistry.registerComponent('albums', () => App)




//                                 // END INDEX FOR AYAHAY APP



//                                 // INDEX FOR HG APP
// // Import a library to help create a component
// import React from 'react'; 
// import {AppRegistry, View } from 'react-native';
// import Header from './src/components/Header';
// import AlbumlList from './src/components/AlbumList';
// // import Navigator from './HomeStack';
// // import Navigator from './AppAyahay';
// import Navigator from './AppHgGUIDE';
// // import Navigator from './HomestackAahay';



// // Create a component 
// const App = () =>  (
//   <View style={{ flex: 1 }}>
//         {/* <Header headerText={'My Albums'} />
//         <AlbumlList /> */}
//         <Navigator />
//   </View>
//     );


// // Render it to the device
// AppRegistry.registerComponent('albums', () => App)



          // END INDEX FOR HG APP





// Responsive Tutorials




// import React from 'react'
// import { AppRegistry, Image, SafeAreaView, ScrollView, StyleSheet, Text } from 'react-native'
// import {
//   heightPercentageToDP as hp,
//   widthPercentageToDP as wp,
// } from 'react-native-responsive-screen'

// const chats = [
//   { name: 'Prem', text: 'My God in heaven, you look like an angel' },
//   { name: 'Pooja', text: 'Are you for real?' },
//   { name: 'Prem', text: 'You have the best pics I have ever seen' },
//   { name: 'Prem', text: 'Everything about you is perfect' },
//   { name: 'Pooja', text: 'OMG ahaha, thank you so so much 🙌' },
//   { name: 'Prem', text: 'I dont want your thank you & all' },
//   { name: 'Pooja', text: 'So what do you want?' },
//   { name: 'Prem', text: 'Plz just teach me how to edit pics 🙏' },
//   { name: 'Pooja', text: 'Go to hell 😡' },
//   { name: 'Prem', text: 'Are you there?' },
//   { name: 'Prem', text: 'Did you just block me? 😅' },
// ]

// const App = () => {
//   return (
//     <SafeAreaView style={styles.container}>
//       <ScrollView>
//         <Image
//           source={require('./assets/pikachu_heart_eyes.png')}
//           style={styles.img}
//         />
//         {chats.map((chat, i) => (
//           <Text
//             key={i}
//             style={[
//               styles.commonChat,
//               chat.name === 'Prem' ? styles.rightChat : styles.leftChat,
//             ]}
//           >
//             <Text style={styles.username}>@{chat.name}: </Text>
//             {chat.text}
//           </Text>
//         ))}
//       </ScrollView>
//     </SafeAreaView>
//   )
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: 'black',
//   },
//   img: {
//     width: wp('70%'),
//     height: hp('30%'),
//     alignSelf: 'flex-end',
//     resizeMode: 'contain',
//   },
//   commonChat: {
//     color: 'white',
//     borderRadius: 10,
//     borderWidth: 1,
//     borderColor: 'white',
//     overflow: 'hidden',
//     padding: 10,
//     margin: 10,
//     fontSize: hp('1.6%'),
//   },
//   leftChat: {
//     backgroundColor: '#c83660',
//     alignSelf: 'flex-start',
//   },
//   rightChat: {
//     backgroundColor: 'rebeccapurple',
//     alignSelf: 'flex-end',
//   },
//   username: {
//     fontWeight: 'bold',
//   },
// })


// // Render it to the device
// AppRegistry.registerComponent('albums', () => App)













import React, { useState } from "react";
import {AppRegistry, View } from 'react-native';

import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createDrawerNavigator } from "react-navigation-drawer";
// import { AppLoading } from "expo";
// import * as Font from "expo-font";
// import Untitled from "./srcAyahay/screens/Untitled";
import IndexScreen from "./srcUdemyFatBlog/screens/IndexScreen";
// import ResultsShowScreen from "./srcUdemyFat/screens/ResultsShowScreen";
import { Provider } from './srcUdemyFatBlog/context/BlogContext';

// const DrawerNavigation = createDrawerNavigator({
//   // Untitled: Untitled,
//   SearchScreen: SearchScreen
// });

const navigator = createStackNavigator(
  {
    // DrawerNavigation: {
    //   screen: DrawerNavigation
    // },
    // Untitled: Untitled,
    Index: IndexScreen,
    // ResultsShow: ResultsShowScreen
  },
//   {
//     headerMode: "none"
//   }
  {
    // headerMode: "none",
    initialRouteName: "Index",
    defaultNavigationOptions: {
      title: "Blogs"
    }
  }
);

const App2 =  createAppContainer(navigator);

 export default () => {
     return (
        <Provider>
         <App2 />
        </Provider>
     )
 }


 // // Render it to the device
AppRegistry.registerComponent('albums', () => App2)