
import React from 'react';
import { StyleSheet, View, Image, Text, FlatList, TouchableOpacity } from 'react-native';
import Svg, { Path } from "react-native-svg";

const MechanicTabBar = ({ navigation }) => {

  return (
    <View style={styles.container}>
      <View style={styles.group7}>
        <View style={styles.menu}>
          <View style={styles.menu1}>
            <View style={styles.tabBarStack}>
              <View style={styles.tabBar}>
                <View style={styles.tabBarItems}>
                  <View style={styles.barStack}>
                    <Svg
                      viewBox="-0 -0 375 82.50000000000001"
                      style={styles.bar}
                    >
                      <Path
                        strokeWidth={0}
                        fill="rgba(255,255,255,1)"
                        fillOpacity={1}
                        strokeOpacity={1}
                        d="M0.00 25.00 L0.00 57.50 C0.00 57.50 0.00 82.50 0.00 82.50 C0.00 82.50 0.00 82.50 0.00 82.50 L375.00 82.50 L25.00 -0.00 C25.00 -0.00 0.00 0.00 0.00 25.00 Z"
                      ></Path>
                    </Svg>
                    <View style={styles.indicator}></View>
                  </View>
                </View>
              </View>
              <View style={styles.group}>
                <View style={styles.group2Stack}>
                  <View style={styles.group2}>
                    <View style={styles.group5Row}>
                      <View style={styles.group5}>
                        <View style={styles.home1}>
                          <Svg viewBox="-0 -0 23 22" style={styles.path1}>
                            <Path
                              strokeWidth={0}
                              fill="rgba(0,0,0,1)"
                              fillOpacity={1}
                              strokeOpacity={1}
                              d="M3.87 6.86 L10.52 0.39 C10.52 0.39 11.08 -0.01 11.52 0.00 C11.87 0.01 12.46 0.39 12.46 0.39 L22.83 10.49 C22.83 10.49 22.98 10.96 22.85 11.10 C22.69 11.28 22.38 11.29 22.18 11.12 C21.22 10.18 20.25 9.24 19.29 8.30 C19.30 12.05 19.32 15.80 19.29 19.56 C19.27 20.83 18.14 21.96 16.80 21.98 C13.26 22.01 9.72 22.01 6.18 21.98 C4.87 21.96 3.68 19.56 3.68 19.56 L3.68 8.31 L0.79 11.12 L0.14 10.49 L3.84 6.89 L3.87 6.86 Z M11.80 1.02 C11.63 0.86 11.16 1.03 11.16 1.03 L4.60 7.42 C4.60 7.42 4.52 15.51 4.60 19.56 C4.63 20.36 5.34 21.06 6.17 21.09 C7.13 21.11 8.10 21.12 9.07 21.12 C9.07 19.50 9.06 17.88 9.07 16.25 C9.09 15.01 10.16 13.93 11.51 13.94 C12.75 13.96 13.89 16.25 13.89 16.25 L13.89 21.11 C13.89 21.11 15.82 21.10 16.79 21.09 C17.62 21.07 18.37 19.52 18.37 19.52 L18.37 7.39 C18.37 7.39 14.02 3.12 11.80 1.02 Z M9.99 16.28 L9.99 21.09 L12.97 21.09 C12.97 21.09 13.00 17.86 12.97 16.25 C12.95 15.49 11.46 14.83 11.46 14.83 L9.99 16.28 Z"
                            ></Path>
                          </Svg>
                        </View>
                        <Text style={styles.home}>Home</Text>
                      </View>
                      <View style={styles.group4}>
                        <View style={styles.bell}></View>
                        <Text style={styles.notification}>Notification</Text>
                      </View>
                      <View style={styles.group3}>
                        <View style={styles.cog}>
                          <View style={styles.cog1Stack}>
                            <View style={styles.cog1}></View>
                            <Svg
                              viewBox="-0 -0 22.09232609054021 22.09173055141374"
                              style={styles.path}
                            >
                              <Path
                                strokeWidth={0}
                                fill="rgba(0,0,0,1)"
                                fillOpacity={1}
                                strokeOpacity={1}
                                d="M4.81 2.13 C4.91 2.18 4.92 2.20 4.95 2.22 C5.39 2.65 5.85 3.06 6.31 3.46 C6.31 3.46 6.15 3.62 6.62 3.34 C7.36 2.92 8.99 2.40 8.99 2.40 L9.03 2.39 C9.03 2.39 9.11 1.13 9.12 0.49 C9.12 0.49 9.18 0.15 9.55 0.10 C9.70 0.08 9.85 0.06 10.00 0.05 C10.86 -0.03 11.74 -0.01 12.60 0.11 C12.60 0.11 12.95 0.16 12.98 0.49 C12.99 1.11 13.01 1.72 13.06 2.34 C13.06 2.34 12.84 2.33 13.36 2.47 C14.19 2.69 15.71 3.48 15.71 3.48 L15.74 3.50 C15.74 3.50 16.69 2.66 17.14 2.22 C17.14 2.22 17.41 1.98 18.00 2.46 C18.68 3.01 19.29 3.64 19.82 4.33 C19.93 4.48 20.05 4.74 19.87 4.95 C19.44 5.39 19.03 5.84 18.63 6.31 C18.63 6.31 18.48 6.15 18.75 6.62 C19.17 7.36 19.69 8.99 19.69 8.99 L19.70 9.03 C19.70 9.03 20.96 9.11 21.60 9.12 C21.60 9.12 21.93 9.09 22.00 9.65 C22.12 10.59 22.12 11.55 22.00 12.50 C21.97 12.69 21.87 12.95 21.60 12.98 C21.51 12.98 21.42 12.98 21.32 12.98 C20.80 12.99 20.28 13.02 19.76 13.06 C19.76 13.06 19.76 12.84 19.62 13.36 C19.40 14.19 18.61 15.71 18.61 15.71 L18.59 15.74 C18.59 15.74 19.43 16.69 19.87 17.14 C19.87 17.14 20.11 17.40 19.63 18.00 C19.06 18.70 18.41 19.33 17.68 19.88 C17.29 20.17 16.86 19.59 16.40 19.17 C16.13 18.93 15.74 18.59 15.74 18.59 C15.70 18.62 15.66 18.64 15.61 18.67 C14.83 19.13 13.10 19.69 13.10 19.69 L13.07 19.70 C13.07 19.70 12.99 20.96 12.98 21.60 C12.98 21.60 12.92 21.94 12.55 21.99 C12.40 22.01 12.25 22.03 12.10 22.04 C11.23 22.12 10.36 22.10 9.50 21.98 C9.50 21.98 9.14 21.93 9.12 21.60 C9.11 21.51 9.11 21.42 9.11 21.32 C9.10 20.80 9.07 20.28 9.03 19.76 C9.03 19.76 9.26 19.76 8.73 19.62 C7.90 19.40 6.39 18.61 6.39 18.61 L6.36 18.59 C6.36 18.59 5.41 19.43 4.95 19.87 C4.95 19.87 4.66 20.07 4.37 19.85 C3.56 19.23 2.83 18.50 2.22 17.68 C2.22 17.68 2.00 17.40 2.22 17.14 C2.65 16.70 3.06 16.25 3.46 15.78 C3.46 15.78 3.62 15.94 3.34 15.47 C2.92 14.73 2.40 13.10 2.40 13.10 L2.39 13.07 C2.39 13.07 1.13 12.98 0.50 12.98 C0.50 12.98 0.14 12.96 0.06 12.20 C-0.03 11.30 -0.02 10.40 0.11 9.50 C0.18 9.01 0.89 9.11 1.52 9.08 C1.88 9.07 2.39 9.03 2.39 9.03 C2.41 8.98 2.42 8.93 2.43 8.89 C2.65 8.00 3.48 6.39 3.48 6.39 L3.50 6.36 C3.50 6.36 2.66 5.40 2.22 4.95 C2.22 4.95 2.02 4.66 2.25 4.37 C2.85 3.57 3.57 2.86 4.37 2.25 C4.37 2.25 4.52 2.05 4.81 2.13 Z M9.93 0.84 L9.89 0.84 C9.89 0.84 9.88 1.04 9.88 1.14 C9.86 1.69 9.89 2.26 9.77 2.80 C9.69 3.14 9.12 3.16 8.69 3.29 C7.93 3.52 7.21 3.86 6.54 4.30 C6.54 4.30 6.31 4.45 6.08 4.28 C5.59 3.87 4.66 3.02 4.66 3.02 L4.65 3.01 L3.07 4.58 L3.01 4.65 C3.01 4.65 3.97 5.57 4.31 6.12 C4.49 6.42 4.10 6.83 3.89 7.22 C3.52 7.93 3.25 8.68 3.09 9.46 C3.09 9.46 3.03 9.73 2.75 9.77 C2.12 9.83 0.86 9.88 0.86 9.88 L0.84 9.88 L0.83 12.12 L0.84 12.21 C0.84 12.21 2.17 12.18 2.80 12.33 C3.13 12.40 3.15 12.96 3.27 13.35 C3.50 14.13 3.85 14.87 4.30 15.55 C4.30 15.55 4.45 15.78 4.28 16.01 C3.87 16.50 3.02 17.43 3.02 17.43 L3.01 17.44 L4.58 19.03 L4.65 19.08 C4.65 19.08 5.57 18.12 6.12 17.78 C6.42 17.60 6.83 17.99 7.22 18.20 C7.93 18.57 8.68 18.84 9.47 19.00 C9.47 19.00 9.73 19.06 9.78 19.34 C9.83 19.97 9.88 21.24 9.88 21.24 L9.89 21.25 L12.12 21.26 L12.21 21.25 C12.21 21.25 12.18 19.92 12.33 19.29 C12.41 18.95 12.98 18.94 13.40 18.81 C14.16 18.57 14.89 18.23 15.55 17.79 C15.55 17.79 15.78 17.64 16.02 17.81 C16.50 18.22 17.43 19.07 17.43 19.07 L17.44 19.08 L19.03 17.51 L19.08 17.44 C19.08 17.44 18.12 16.52 17.78 15.97 C17.60 15.67 17.99 15.26 18.20 14.87 C18.57 14.17 18.84 13.41 19.00 12.63 C19.00 12.63 19.06 12.36 19.35 12.32 C19.97 12.26 21.24 12.21 21.24 12.21 L21.25 12.21 L21.26 9.98 L21.25 9.88 C21.25 9.88 19.92 9.92 19.29 9.77 C18.94 9.68 18.93 9.08 18.79 8.64 C18.69 8.33 18.58 8.02 18.44 7.71 C18.26 7.31 18.04 6.91 17.79 6.54 C17.79 6.54 17.64 6.31 17.81 6.08 C18.22 5.59 19.07 4.66 19.07 4.66 L19.08 4.65 L17.51 3.07 L17.44 3.01 C17.44 3.01 16.52 3.97 15.97 4.31 C15.68 4.49 15.28 4.12 14.91 3.92 C14.61 3.75 14.29 3.60 13.97 3.48 C13.53 3.31 13.08 3.18 12.63 3.09 C12.63 3.09 12.37 3.03 12.32 2.75 C12.31 2.65 12.30 2.56 12.29 2.47 C12.25 1.93 12.22 1.39 12.21 0.85 C12.21 0.85 11.00 0.77 11.00 0.77 L9.93 0.84 Z M11.08 6.39 C13.20 6.42 15.21 8.05 15.61 10.17 C16.01 12.24 14.81 14.51 12.86 15.33 C10.69 16.25 7.89 15.21 6.85 13.06 C5.53 10.32 7.57 6.47 10.96 6.40 C11.00 6.39 11.04 6.39 11.08 6.39 Z M11.02 7.17 C8.83 7.19 6.90 9.35 7.21 11.59 C7.47 13.42 9.21 14.93 11.07 14.92 C13.34 14.91 15.32 12.57 14.84 10.26 C14.49 8.54 12.85 7.16 11.02 7.17 Z"
                              ></Path>
                            </Svg>
                          </View>
                        </View>
                        <Text style={styles.settings}>Settings</Text>
                      </View>
                    </View>
                  </View>
                  <View style={styles.bell2}>
                    <Svg
                      viewBox="-0 -0 19.32184191899989 21.58455154804878"
                      style={styles.path2}
                    >
                      <Path
                        strokeWidth={0}
                        fill="rgba(0,0,0,1)"
                        fillOpacity={1}
                        strokeOpacity={1}
                        d="M6.97 19.40 C5.28 19.40 3.59 19.39 1.90 19.38 C0.65 19.36 -0.37 17.94 0.13 16.71 C0.42 16.00 1.92 15.50 1.92 15.50 L3.09 15.50 C3.09 15.50 3.08 11.58 3.09 9.62 C3.11 7.13 6.94 3.68 6.94 3.68 L6.96 3.67 C6.96 3.67 6.93 2.30 7.13 1.77 C7.52 0.72 8.55 -0.01 9.71 0.00 C11.12 0.03 12.39 2.71 12.39 2.71 L12.39 3.67 C12.39 3.67 12.48 3.71 12.52 3.74 C14.75 4.80 16.27 9.66 16.27 9.66 L16.27 15.50 C16.27 15.50 17.62 15.41 18.20 15.67 C19.32 16.15 19.72 17.83 18.85 18.76 C18.49 19.14 17.98 19.37 17.45 19.38 C15.77 19.39 14.09 19.40 12.41 19.40 C12.39 19.67 12.27 19.98 12.17 20.20 C11.79 21.01 10.94 21.56 10.02 21.58 C9.51 21.59 9.00 21.61 8.54 21.44 C7.69 21.13 7.07 20.31 6.97 19.40 Z M11.57 19.57 L7.79 19.57 C7.79 19.57 8.61 20.79 9.34 20.81 C9.75 20.81 10.15 20.85 10.53 20.72 C11.04 20.54 11.57 19.57 11.57 19.57 Z M9.65 0.78 C8.65 0.79 7.74 2.71 7.74 2.71 L7.74 3.93 C7.74 3.93 7.61 4.24 7.29 4.37 C5.28 5.28 3.88 7.36 3.86 9.62 C3.86 9.62 3.86 15.89 3.86 15.89 C3.86 15.89 3.78 16.26 3.48 16.28 C2.42 16.28 1.18 16.12 0.84 17.03 C0.58 17.71 1.13 18.58 1.89 18.60 C7.08 18.70 12.27 18.70 17.46 18.60 C18.33 18.58 18.95 17.35 18.27 16.64 C18.05 16.41 17.43 16.28 17.43 16.28 L15.88 16.28 C15.88 16.28 15.51 16.17 15.49 15.89 C15.49 13.80 15.50 11.71 15.49 9.62 C15.47 7.35 14.02 5.16 11.86 4.29 C11.86 4.29 11.63 4.20 11.61 3.93 C11.61 3.04 11.69 2.10 11.16 1.47 C10.79 1.02 10.27 0.77 9.65 0.78 Z"
                      ></Path>
                    </Svg>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: 375,
    height: 83,
    opacity: 1
  },
  group7: {
    width: 375,
    height: 83,
    opacity: 1
  },
  menu: {
    width: 375,
    height: 83,
    opacity: 1
  },
  menu1: {
    width: 375,
    height: 83,
    opacity: 1
  },
  tabBar: {
    top: 0,
    left: 0,
    width: 375,
    height: 83,
    position: "absolute",
    opacity: 1
  },
  tabBarItems: {
    width: 375,
    height: 83,
    opacity: 1
  },
  bar: {
    top: 0,
    left: 0,
    width: 375,
    height: 82,
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  indicator: {
    left: 121,
    width: 134,
    height: 5,
    backgroundColor: "rgba(57,57,57,1)",
    position: "absolute",
    bottom: 9,
    opacity: 0.15,
    borderRadius: 100
  },
  barStack: {
    width: 375,
    height: 82
  },
  group: {
    top: 15,
    left: 73,
    width: 228,
    height: 40,
    position: "absolute",
    opacity: 1
  },
  group2: {
    top: 0,
    left: 0,
    width: 228,
    height: 40,
    position: "absolute",
    opacity: 1,
    flexDirection: "row"
  },
  group5: {
    width: 53,
    height: 40,
    opacity: 1
  },
  home1: {
    width: 24,
    height: 23,
    opacity: 1,
    marginLeft: 15
  },
  path1: {
    width: 23,
    height: 22,
    backgroundColor: "transparent",
    borderColor: "transparent",
    marginTop: 1
  },
  home: {
    width: 53,
    height: 14,
    backgroundColor: "transparent",
    color: "rgba(51,51,51,1)",
    opacity: 1,
    fontSize: 10,
    fontFamily: "Avenir-Heavy",
    letterSpacing: 0.12,
    textAlign: "center",
    marginTop: 3
  },
  group4: {
    width: 61,
    height: 40,
    opacity: 1,
    marginLeft: 36
  },
  bell: {
    width: 25,
    height: 25,
    backgroundColor: "transparent",
    opacity: 1,
    marginLeft: 18
  },
  notification: {
    width: 61,
    height: 14,
    backgroundColor: "transparent",
    color: "rgba(51,51,51,1)",
    opacity: 1,
    fontSize: 10,
    fontFamily: "Avenir-Heavy",
    letterSpacing: 0.12,
    textAlign: "center",
    marginTop: 1
  },
  group3: {
    width: 48,
    height: 40,
    opacity: 1,
    marginLeft: 30
  },
  cog: {
    width: 26,
    height: 25,
    opacity: 1,
    marginLeft: 11
  },
  cog1: {
    top: 0,
    left: 1,
    width: 25,
    height: 25,
    backgroundColor: "transparent",
    position: "absolute",
    opacity: 1
  },
  path: {
    top: 1,
    left: 0,
    width: 22,
    height: 22,
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  cog1Stack: {
    width: 26,
    height: 25
  },
  settings: {
    width: 48,
    height: 14,
    backgroundColor: "transparent",
    color: "rgba(51,51,51,1)",
    opacity: 1,
    fontSize: 10,
    fontFamily: "Avenir-Heavy",
    letterSpacing: 0.12,
    textAlign: "center",
    marginTop: 1
  },
  group5Row: {
    height: 40,
    flexDirection: "row",
    flex: 1
  },
  bell2: {
    top: 0,
    left: 107,
    width: 25,
    height: 25,
    position: "absolute",
    opacity: 1
  },
  path2: {
    width: 19,
    height: 22,
    backgroundColor: "transparent",
    borderColor: "transparent",
    marginTop: 2,
    marginLeft: 3
  },
  group2Stack: {
    width: 228,
    height: 40
  },
  tabBarStack: {
    width: 375,
    height: 83
  }
});

export default MechanicTabBar;

