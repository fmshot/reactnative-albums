import React from 'react';
import { StyleSheet, View, Image, Text, TextInput, TouchableOpacity } from 'react-native';

const FormTextInput = ({ inputstyle, navigation, placeholder, label }) => {

  return (
    <>
      <View style={styles.shippingdetailsView}>
        <View style={styles.citystackView}>
            <Text style={styles.citylabelText}>{label}</Text>
            <View pointerEvents="box-none"
                style={{
                    flex: 1,
                    marginLeft: 1,
                    marginTop: 8,
                }}>
                <View style={styles.dividerTwoView}/>
                <TextInput
                    autoCorrect={false}
                    placeholder={placeholder}
                    style={{...styles.cityTextInput,...inputstyle}}/>
            </View>
        </View>
    </View>
    </>
  );
};

const styles = StyleSheet.create({
    shippingdetailsView: {
		marginLeft: 30,
        marginRight: 31,
	},
    citystackView: {
		backgroundColor: "transparent",
		height: 46,
		marginLeft: 2,
		marginRight: 2,
		marginTop: 26,
	},
	citylabelText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.28,
		alignSelf: "flex-start",
	},
	dividerTwoView: {
		backgroundColor: "#dbdbde",
		position: "absolute",
		left: 0,
		right: 0,
		top: 21,
		height: 1,
	},
	cityTextInput: {
		backgroundColor: "transparent",
		padding: 0,
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Medium",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		position: "absolute",
		left: 4,
		// width: 96,
		bottom: 0,
		height: 22,
	},
});

export default FormTextInput;

