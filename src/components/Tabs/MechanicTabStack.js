import React , {useEffect} from 'react';
import {
    createAppContainer,
    createStackNavigator,
    createBottomTabNavigator,
    createSwitchNavigator
  } from 'react-navigation';
import MechanicTabBar from '../MechanicTabBar'
import Homepage from "../../screens/Homepage"

const homeFlow = createStackNavigator({
    home : Homepage,
})

const MechanicTabStack = createBottomTabNavigator({
    home: {screen: homeFlow },
}, { tabBarComponent: MechanicTabBar, })



export default MechanicTabStack