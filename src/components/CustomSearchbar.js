import { View, StyleSheet,Image, TextInput, } from "react-native"
import React, {useState} from "react"
import RecentTextPopUp from "./RecentTextPopUp"
import searchicon from '../../src/Common/Images'


const CustomSearchbar = ({navigation, showHistory, placeholder}) => {
	const [showRecents, setShowRecents] = useState(false)

    return <>
        <View style={styles.searchbarView}>
            <Image
                source={searchicon}
                style={styles.iconSearchImage}/>
            <View
                pointerEvents="box-none"
                style={{
                    flex: 1,
                    height: 29,
                    marginLeft: 16,
                    marginRight: 16,
                    marginTop: 10,
                }}>
                <TextInput
                    onFocus={()=>setShowRecents(showHistory? !showRecents : showHistory)}
                    onBlur={()=>setShowRecents(showHistory ? !showRecents : showHistory)}
                    returnKeyType="search"
                    clearButtonMode="always"
                    autoCorrect={false}
                    placeholder={placeholder? placeholder : "Search keyword, category or brand " }
                    style={styles.placeholderTextInput}/>

                
                
            </View>
        </View>
        { showRecents?  <RecentTextPopUp style={{marginTop: 30}} /> : null}
        </>

}

export default CustomSearchbar

CustomSearchbar.navigationOptions =  {
	header: null
}



const styles = StyleSheet.create({
    searchbarView: {
		backgroundColor: "white",
		borderRadius: 24,
		shadowColor: "rgba(0, 0, 0, 0.08)",
		shadowRadius: 8,
		shadowOpacity: 1,
		height: 48,
		marginLeft: 24,
		marginRight: 24,
		marginTop: 15,
		marginBottom: 10,
		flexDirection: "row",
		alignItems: "flex-start",
		elevation: 1,
	},
	iconSearchImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		height: 24,
		marginLeft: 20,
		marginRight: 10,
		marginTop: 12,
	},
	placeholderTextInput: {
		backgroundColor: "transparent",
		opacity: 1,
		padding: 0,
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		position: "absolute",
		left: 0,
		right: 0,
		top: 5,
		height: 20,
	},
})