import React, { Component } from "react";
import { StyleSheet, View, Image } from "react-native";
import Svg, { Ellipse } from "react-native-svg";

function MapsCloseButton(props) {
  return (
    <View style={[styles.container, props.style]}>
      <View style={styles.ellipseStack}>
        <Svg viewBox="0 0 56.18 56.18" style={styles.ellipse}>
          <Ellipse
            strokeWidth={1}
            fill="rgba(255,255,255,1)"
            stroke="rgba(230, 230, 230,1)"
            cx={28}
            cy={28}
            rx={28}
            ry={28}
          ></Ellipse>
        </Svg>
        <Image
          source={require("../../../assets/images/close.png")}
          resizeMode="contain"
          style={styles.image}
        ></Image>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  ellipse: {
    width: 56,
    height: 56,
    position: "absolute",
    left: 0,
    top: 0
  },
  image: {
    top: 17,
    left: 16,
    width: 24,
    height: 24,
    position: "absolute"
  },
  ellipseStack: {
    width: 56,
    height: 56
  }
});

export default MapsCloseButton;
