import React , {useContext}from 'react';
import { StyleSheet, View, Image, Text, FlatList, TouchableOpacity, Dimensions, ScrollView } from 'react-native';
import {Context as CartContext} from "../context/CartContext"


const CartLineItem = ({ navigation ,lineitem}) => {
	const {state,  deleteCartItem } = useContext(CartContext)
	
  return  <TouchableOpacity onPress={()=> {navigation.navigate("ProductDetail", {product: lineitem.product})}} style={styles.container}>
      <View style={styles.group4Stack}>
        <View style={styles.group1}>
          <View style={styles.rectangle2}>
            <Image source={{uri: lineitem.product.productImages[0].image}} style={styles.placeholdersVectorImage70Px} />
          </View>
          <Text style={styles.productName}> {lineitem.product.name} </Text>
          <Text style={styles.style}>₦{lineitem.product.price  * lineitem.quantity} </Text>
        </View>
        <View style={styles.group3}>
          <View style={styles.group41}>
            <View style={styles.backgroundStack}>
              <View style={styles.background}>
                <Text style={styles.numbers}>{lineitem.product.condition.name}</Text>
              </View>
              <Text style={styles.numbers1}>{lineitem.product.condition.name}</Text>
            </View>
          </View>
        </View>
        <Text style={styles.quantity2}>Quantity: {lineitem.quantity}</Text>
        <Text style={styles.style1}>₦{lineitem.product.price}</Text>
        <Text style={styles.conditionBelgium}>Condition: {lineitem.product.condition.name}</Text>
      </View>
    </TouchableOpacity>
  
}

export default CartLineItem

const styles = StyleSheet.create({
  container: {
  width: Dimensions.get("window").width-30,
	height: 90,
	marginBottom: 10,
  marginLeft: 15,
  marginRight: 15,
	justifyContent: 'space-evenly',
  flex: 1,
  },
  group4: {
    top: 0,
    left: 0,
    position: "absolute",
    opacity: 1,
    right: 0,
    bottom: 0
  },
  itemListLine: {
    width: 345,
    height: 90,
    opacity: 1
  },
  itemListLine1: {
    width: 345,
    height: 90,
    opacity: 1
  },
  itemListLine2: {
    width: 345,
    height: 90,
    opacity: 1
  },
  itemList: {
    width: 345,
    height: 90,
    opacity: 1
  },
  itemList1: {
    width: 345,
    height: 90,
    opacity: 1
  },
  group1: {
    // width: 345,
    height: 90,
    opacity: 1,
    flexDirection: "row"
  },
  image: {
    width: 90,
    height: 90,
    opacity: 1
  },
  rectangle2: {
    width: 90,
    height: 90,
    backgroundColor: "rgba(242,242,242,1)",
    borderRadius: 12
  },
  placeholdersVectorImage70Px: {
    width: 90,
    height: 90,
    backgroundColor: "transparent",
    opacity: 1,
    // marginTop: 10,
    // marginLeft: 10,
    borderRadius: 12,
  },
  productName: {
    backgroundColor: "transparent",
    color: "rgba(6,6,6,1)",
    opacity: 1,
    fontSize: 14,
    fontFamily: "Avenir-Heavy",
    lineHeight: 20,
    marginLeft: 15,
    marginTop: 6
  },
  style: {
    backgroundColor: "transparent",
    color: "rgba(6,6,6,1)",
    opacity: 1,
    fontSize: 14,
    fontFamily: "Avenir-Heavy",
    lineHeight: 20,
    textAlign: "right",
    justifyContent: "flex-end",
    // marginLeft: 79,
    flex:  1,
    marginTop: 66,
    // width: Dimensions.get("window").width,

  },
  imageRow: {
    height: 90,
    flexDirection: "row",
    flex: 1
  },
  group3: {
    top: 6,
    // left: 279,
    alignSelf: "flex-end",
    width: 66,
    height: 22,
    position: "absolute",
    opacity: 1,
    elevation: 2,
    shadowOffset: {
      height: 1,
      width: 0
    },
    shadowColor: "rgba(0,0,0,0.1)",
    shadowOpacity: 1,
    shadowRadius: 4,
    elevation: 1,
  },
  group41: {
    width: 66,
    height: 22,
    opacity: 1
  },
  background: {
    top: 0,
    left: 0,
    width: 66,
    height: 22,
    backgroundColor: "rgba(255,255,255,1)",
    position: "absolute",
    opacity: 1,
    borderRadius: 6,
    overflow: "hidden",
    elevation: 2,
  },
  numbers: {
    width: 44,
    height: 12,
    backgroundColor: "transparent",
    color: "rgba(6,6,6,1)",
    opacity: 1,
    fontSize: 9,
    lineHeight: 12,
    letterSpacing: 0.15,
    marginTop: 5,
    marginLeft: 11
  },
  numbers1: {
    top: 5,
    left: 11,
    width: 44,
    height: 12,
    backgroundColor: "transparent",
    color: "rgba(6,6,6,1)",
    position: "absolute",
    opacity: 1,
    fontSize: 9,
    lineHeight: 12,
    letterSpacing: 0.15
  },
  backgroundStack: {
    elevation: 2,
    // width: 66,
    // height: 22
  },
  quantity2: {
    top: 49,
    left: 106,
    backgroundColor: "transparent",
    color: "rgba(51,51,51,1)",
    position: "absolute",
    opacity: 1,
    fontSize: 11,
    letterSpacing: 0.25
  },
  style1: {
    top: 31,
    left: 105,
    backgroundColor: "transparent",
    color: "rgba(51,51,51,1)",
    position: "absolute",
    opacity: 1,
    fontSize: 11,
    // fontFamily: "LucidaGrande",
    letterSpacing: 0.25
  },
  conditionBelgium: {
    top: 67,
    left: 106,
    backgroundColor: "transparent",
    color: "rgba(51,51,51,1)",
    position: "absolute",
    opacity: 1,
    fontSize: 11,
    letterSpacing: 0.25,

  },
  group4Stack: {
    // flex: 1
  }
})


{/*   
  	<View style={styles.cartlineitemstackView}>
        <View pointerEvents="box-none"
            style={{
                // position: "absolute",
                // left: 0,
                // right: 0,
                // top: 0,
                // bottom: 0,
            }}>
            <View
                pointerEvents="box-none"
                style={{
                    // position: "absolute",
                    // left: 0,
                    // right: 0,
                    // top: 0,
                    // bottom: 0,
                    // justifyContent: "center",
                }}>
                <View
                    style={styles.cartLineItemView}>
                    <View style={styles.group4View}>
                        <View style={styles.groupView}>
                            <View style={styles.imageView}>
                                <View pointerEvents="box-none"
                                    style={{
                                        // position: "absolute",
                                        // left: 0,
                                        // right: 0,
                                        // top: 0,
                                        // bottom: 0,
                                        // justifyContent: "center",
                                    }}>
                                    <Image
                                        source={require("./../../assets/images/rectangle-2.png")}
                                        style={styles.rectangle2Image}/>
                                </View>
                                <View
                                    pointerEvents="box-none"
                                    style={{
                                        position: "absolute",
                                        left: 0,
                                        right: 0,
                                        top: 0,
                                        bottom: 0,
                                        justifyContent: "center",
                                    }}>
                                    <Image
                                        source={require("./../../assets/images/profile.png")}
                                        style={styles.profileImage}/>
                                </View>
                            </View>
                            <Text style={styles.alternatorBeltText}>{lineitem.product.name}</Text>
                            <View
                                style={{
                                    flex: 1,
                                }}/>
                            <Text
                                style={styles.textText}>₦{lineitem.product.price}</Text>
                        </View>
                    </View>
                </View>
            </View>
            <View
                pointerEvents="box-none"
                style={{
                    position: "absolute",
                    left: 120,
                    right: 15,
                    top: 46,
                    bottom: 18,
                    alignItems: "flex-start",
                }}>
                <Text
                    style={styles.textTwoText}>₦</Text>
                <Text
                    style={styles.conditionBelgiumText}>Condition: {lineitem.product.condition.name}</Text>
                <View
                    style={{
                        flex: 1,
                    }}/>
                <View
                    pointerEvents="box-none"
                    style={{
                        alignSelf: "stretch",
                        height: 15,
                        flexDirection: "row",
                        alignItems: "flex-end",
                    }}>
                    <Text
                        style={styles.editText}>Edit</Text>
                    <View
                        style={{
                            flex: 1,
                        }}/>
                    <Text
                        style={styles.saveForLaterText}>Save for later</Text>

                    <Text  style={styles.removeText}>Remove</Text>
                </View>
            </View>
        </View>
        <View
            pointerEvents="box-none"
            style={{
                position: "absolute",
                left: 0,
                top: 0,
                bottom: 0,
                justifyContent: "center",
            }}>
            <Text
                style={styles.quantity2Text}>Quantity: {lineitem.quantity}</Text>
        </View>
    </View>

    </>

}

export default CartLineItem

const styles = StyleSheet.create({
	reviewOrderView: {
		backgroundColor: "white",
		flex: 1,
	},
	group5TwoView: {
		backgroundColor: "transparent",
		alignSelf: "center",
		width: 223,
		height: 17,
		marginTop: 191,
	},
	ovalView: {
		backgroundColor: "rgb(19, 20, 22)",
		borderRadius: 8.5,
		width: 17,
		height: 17,
	},
	iconImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: null,
		height: 7,
		marginLeft: 4,
		marginRight: 4,
	},
	lineView: {
		backgroundColor: "white",
		width: 87,
		height: 1,
	},
	iconTwoImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		flex: 1,
		height: 7,
		marginLeft: 3,
		marginRight: 4,
	},
	lineTwoView: {
		backgroundColor: "white",
		width: 87,
		height: 1,
		marginRight: 16,
	},
	ovalThreeView: {
		backgroundColor: "transparent",
		borderRadius: 8.5,
		borderWidth: 0.5,
		borderColor: "white",
		borderStyle: "solid",
		width: 17,
		height: 17,
	},
	ovalTwoView: {
		backgroundColor: "rgb(19, 20, 22)",
		borderRadius: 8.5,
		width: 17,
		height: 17,
	},
	cartlineitemstackView: {
		backgroundColor: "transparent",
		height: 143,
		marginTop: 31,
	},
	cartLineItemView: {
		backgroundColor: "transparent",
		height: 143,
		justifyContent: "center",
	},
	group4View: {
		backgroundColor: "white",
		shadowColor: "rgba(0, 0, 0, 0.03)",
		shadowRadius: 8,
		shadowOpacity: 1,
		height: 143,
	},
	groupView: {
		backgroundColor: "transparent",
		height: 90,
		marginLeft: 15,
		marginRight: 15,
		marginTop: 15,
		flexDirection: "row",
		alignItems: "flex-start",
	},
	imageView: {
		backgroundColor: "transparent",
		alignSelf: "center",
		width: 90,
		height: 90,
	},
	rectangle2Image: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: null,
		height: 90,
	},
	profileImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: null,
		height: 70,
		marginLeft: 10,
		marginRight: 10,
	},
	alternatorBeltText: {
		backgroundColor: "transparent",
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		lineHeight: 20,
		marginLeft: 15,
		marginTop: 6,
	},
	textText: {
		backgroundColor: "transparent",
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Roman",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "right",
		lineHeight: 20,
		marginTop: 6,
	},
	textTwoText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
	},
	conditionBelgiumText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
		marginTop: 21,
	},
	editText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
	},
	saveForLaterText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
		marginRight: 54,
	},
	removeText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
		backgroundColor: "transparent",
	},
	quantity2Text: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
		marginLeft: 121,
	},
	group6View: {
		backgroundColor: "transparent",
		alignSelf: "flex-end",
		width: 322,
		height: 71,
		marginRight: 21,
		marginTop: 41,
		alignItems: "flex-start",
	},
	subtotal6000Text: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Roman",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
		marginLeft: 24,
	},
	shippingTbdText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Roman",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
		marginLeft: 24,
		marginTop: 8,
	},
	line2View: {
		backgroundColor: "white",
		alignSelf: "stretch",
		height: 1,
		marginTop: 7,
	},
	total6000Text: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		letterSpacing: 0.25,
		marginLeft: 24,
	},
	group5View: {
		backgroundColor: "rgb(19, 20, 22)",
		borderRadius: 30,
		height: 60,
		marginLeft: 33,
		marginRight: 32,
		marginTop: 50,
		justifyContent: "center",
		alignItems: "center",
	},
	completeOrderText: {
		backgroundColor: "transparent",
		color: "white",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		letterSpacing: 0.94,
	},


}); */}
