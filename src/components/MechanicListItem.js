import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import Svg, { Path } from "react-native-svg";
import {withNavigation} from "react-navigation"

const MechanicListItem = ({navigation}) => {
  return (
    <TouchableOpacity onPress={()=> navigation.navigate("MechanicDetails")} style={[styles.root]}>
      <View style={styles.container1}>
        <View style={styles.rectangle2Row}>
          <View style={styles.rectangle2}></View>
          <View style={styles.titleColumn}>
            <Text style={styles.title}>Chucka Motors</Text>
            <View style={styles.statusRow}>
              <Text style={styles.status}>4.6 •</Text>
              <Text style={styles.type}>Gudu Mechanic Village</Text>
            </View>
            <View style={styles.iconDirectionRow}>
              <View style={styles.iconDirection}>
                <View style={styles.bound2}>
                  <Svg
                    viewBox="-0 -0 7.999999999999981 9.999999999999972"
                    style={styles.path2}
                  >
                    <Path
                      strokeWidth={0}
                      fill="rgba(34,34,34,1)"
                      fillOpacity={1}
                      strokeOpacity={1}
                      d="M0.07 9.81 L3.93 0.19 C3.96 0.09 4.04 0.09 4.07 0.19 L7.93 9.81 C7.96 9.91 7.91 9.96 7.82 9.91 L4.18 8.09 C4.09 8.04 4.00 8.00 4.00 8.00 L-0.07 10.19 Z"
                    ></Path>
                  </Svg>
                </View>
              </View>
              <Text style={styles.distance}>200 m</Text>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1
  },
  container1: {
    width: 311,
    height: 96,
    backgroundColor: "transparent"
  },
  rectangle2: {
    width: 83,
    height: 75,
    backgroundColor: "rgba(230,230,230,1)",
    opacity: 1
  },
  title: {
    width: 195,
    height: 18,
    backgroundColor: "transparent",
    color: "rgba(74,74,74,1)",
    opacity: 1,
    fontSize: 14,
    lineHeight: 18,
    letterSpacing: 0.2,
    marginLeft: 3
  },
  status: {
    backgroundColor: "transparent",
    color: "rgba(195,195,195,1)",
    opacity: 1,
    fontSize: 12,
    fontFamily: "Avenir-Medium"
  },
  type: {
    width: 160,
    height: 14,
    backgroundColor: "transparent",
    color: "rgba(34,34,34,1)",
    opacity: 0.5,
    fontSize: 11,
    lineHeight: 14,
    letterSpacing: 1,
    marginLeft: 9
  },
  statusRow: {
    height: 14,
    flexDirection: "row",
    marginTop: 6,
    marginLeft: 3
  },
  iconDirection: {
    width: 16,
    height: 16,
    opacity: 1
  },
  bound2: {
    width: 16,
    height: 16,
    backgroundColor: "transparent"
  },
  path2: {
    width: 8,
    height: 10,
    backgroundColor: "transparent",
    transform: [
      {
        rotate: "45deg"
      }
    ],
    borderColor: "transparent",
    marginTop: 2,
    marginLeft: 5
  },
  distance: {
    backgroundColor: "transparent",
    color: "rgba(74,74,74,1)",
    opacity: 1,
    fontSize: 12,
    letterSpacing: 0.2,
    marginLeft: 4,
    marginTop: 1
  },
  iconDirectionRow: {
    height: 16,
    flexDirection: "row",
    marginTop: 13,
    marginRight: 140
  },
  titleColumn: {
    width: 198,
    marginLeft: 14,
    marginTop: 1,
    marginBottom: 7
  },
  rectangle2Row: {
    height: 75,
    flexDirection: "row",
    marginTop: 11,
    marginLeft: 13,
    marginRight: 3
  }
});

export default withNavigation(MechanicListItem);
