import React from 'react';
import { StyleSheet, View, Image, Text, FlatList, TouchableOpacity } from 'react-native';

const PaymentMethod = ({ navigation, name }) => {

  return (
    <>
      <TouchableOpacity onPress={()=>{navigation.navigate("CartPayment")}}
      style={styles.viewView}>
        <Image source={require("../../assets/images/rectangle-5.png")} style={styles.rectangleThreeImage}/>
        <Text style={styles.localDebitText}>{name}</Text>
        <View style={{ flex: 1, }}/>
        <Image source={require("../../assets/images/path-2.png")}  style={styles.pathTwoImage}/>
    </TouchableOpacity>
    </>
  );
};

PaymentMethod.navigationOptions = {
//   title: 'Tracks'
};

const styles = StyleSheet.create({
    viewView: {
		backgroundColor: "transparent",
		height: 33,
		marginLeft: 12,
		marginRight: 10,
		marginTop: 13,
		flexDirection: "row",
		alignItems: "flex-start",
    },
    rectangleThreeImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: 32,
		height: 33,
    },
    pathTwoImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: 10,
		height: 10,
		marginTop: 15,
    },
    localDebitText: {
		backgroundColor: "transparent",
		color: "rgb(58, 44, 60)",
		fontFamily: "Avenir-Book",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		marginLeft: 41,
		marginTop: 6,
	},
});

export default PaymentMethod;
