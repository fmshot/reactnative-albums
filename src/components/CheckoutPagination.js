import React from 'react';
import { StyleSheet, View, Image, Text, FlatList, TouchableOpacity, Dimensions, ScrollView } from 'react-native';


const CheckoutPagination = ({ navigation }) => {

  return <> 
  	<View
					style={styles.group5TwoView}>
					<View
						pointerEvents="box-none"
						style={{
							// position: "absolute",
							// left: 0,
							// right: 0,
							// top: 0,
							// bottom: 0,
							// justifyContent: "center",
						}}>
						<View
							pointerEvents="box-none"
							style={{
								height: 17,
								flexDirection: "row",
								alignItems: "center",
							}}>
							<View
								pointerEvents="box-none"
								style={{
									flex: 1,
									height: 17,
								}}>
								<View
									pointerEvents="box-none"
									style={{
										position: "absolute",
										left: 0,
										top: 0,
										bottom: 0,
										justifyContent: "center",
									}}>
									<View
										style={styles.ovalView}/>
								</View>
								<View
									pointerEvents="box-none"
									style={{
										position: "absolute",
										left: 0,
										right: 0,
										top: 0,
										bottom: 0,
										justifyContent: "center",
									}}>
									<Image
										source={require("./../../assets/images/icon-7.png")}
										style={styles.iconImage}/>
								</View>
							</View>
							<View
								style={styles.lineView}/>
							<Image
								source={require("./../../assets/images/icon-7.png")}
								style={styles.iconTwoImage}/>
							<View
								pointerEvents="box-none"
								style={{
									width: 103,
									height: 17,
								}}>
								<View
									pointerEvents="box-none"
									style={{
										position: "absolute",
										right: 0,
										top: 0,
										bottom: 0,
										justifyContent: "center",
									}}>
									<View
										style={styles.lineTwoView}/>
								</View>
								<View
									pointerEvents="box-none"
									style={{
										position: "absolute",
										right: 0,
										top: 0,
										bottom: 0,
										justifyContent: "center",
									}}>
									<View
										style={styles.ovalThreeView}/>
								</View>
							</View>
						</View>
					</View>
					<View
						pointerEvents="box-none"
						style={{
							position: "absolute",
							alignSelf: "center",
							top: 0,
							bottom: 0,
							justifyContent: "center",
						}}>
						<View
							style={styles.ovalTwoView}/>
					</View>
				</View>

    </>

}

export default CheckoutPagination

const styles = StyleSheet.create({
	reviewOrderView: {
		backgroundColor: "white",
		// flex: 1,
	},
	group5TwoView: {
		backgroundColor: "transparent",
		alignSelf: "center",
		width: 223,
		height: 17,
	},
	ovalView: {
		backgroundColor: "rgb(19, 20, 22)",
		borderRadius: 8.5,
		width: 17,
		height: 17,
	},
	iconImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: null,
		height: 7,
		marginLeft: 4,
		marginRight: 4,
	},
	lineView: {
		backgroundColor: "#dbdbde",
		width: 87,
		height: 1,
	},
	iconTwoImage: {
		resizeMode: "center",
		flex: 1,
		height: 7,
		marginLeft: 3,
		marginRight: 4,
	},
	lineTwoView: {
		backgroundColor: "#dbdbde",
		width: 87,
		height: 1,
		marginRight: 16,
	},
	ovalThreeView: {
		borderRadius: 8.5,
		width: 17,
		height: 17,
		borderStyle: "solid",
		borderWidth: 0.5,
		borderColor: "#979797"

	},
	ovalTwoView: {
		backgroundColor: "rgb(19, 20, 22)",
		borderRadius: 8.5,
		width: 17,
		height: 17,
	},
	cartlineitemstackView: {
		backgroundColor: "transparent",
		height: 143,
		marginTop: 31,
	},
	cartLineItemView: {
		backgroundColor: "transparent",
		height: 143,
		justifyContent: "center",
	},
	group4View: {
		backgroundColor: "white",
		shadowColor: "rgba(0, 0, 0, 0.03)",
		shadowRadius: 8,
		shadowOpacity: 1,
		height: 143,
	},
	groupView: {
		backgroundColor: "transparent",
		height: 90,
		marginLeft: 15,
		marginRight: 15,
		marginTop: 15,
		flexDirection: "row",
		alignItems: "flex-start",
	},
	imageView: {
		backgroundColor: "transparent",
		alignSelf: "center",
		width: 90,
		height: 90,
	},
	rectangle2Image: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: null,
		height: 90,
	},
	profileImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: null,
		height: 70,
		marginLeft: 10,
		marginRight: 10,
	},
	alternatorBeltText: {
		backgroundColor: "transparent",
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		lineHeight: 20,
		marginLeft: 15,
		marginTop: 6,
	},
	textText: {
		backgroundColor: "transparent",
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Roman",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "right",
		lineHeight: 20,
		marginTop: 6,
	},
	textTwoText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
	},
	conditionBelgiumText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
		marginTop: 21,
	},
	editText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
	},
	saveForLaterText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
		marginRight: 54,
	},
	removeText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
		backgroundColor: "transparent",
	},
	quantity2Text: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
		marginLeft: 121,
	},
	group6View: {
		backgroundColor: "transparent",
		alignSelf: "flex-end",
		width: 322,
		height: 71,
		marginRight: 21,
		marginTop: 41,
		alignItems: "flex-start",
	},
	subtotal6000Text: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Roman",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
		marginLeft: 24,
	},
	shippingTbdText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Roman",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.25,
		marginLeft: 24,
		marginTop: 8,
	},
	line2View: {
		backgroundColor: "white",
		alignSelf: "stretch",
		height: 1,
		marginTop: 7,
	},
	total6000Text: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 11,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		letterSpacing: 0.25,
		marginLeft: 24,
	},
	group5View: {
		backgroundColor: "rgb(19, 20, 22)",
		borderRadius: 30,
		height: 60,
		marginLeft: 33,
		marginRight: 32,
		marginTop: 50,
		justifyContent: "center",
		alignItems: "center",
	},
	completeOrderText: {
		backgroundColor: "transparent",
		color: "white",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		letterSpacing: 0.94,
	},


});
