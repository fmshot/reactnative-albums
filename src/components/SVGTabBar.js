import React, {useContext, useEffect, useState} from "react"
import { StyleSheet, View, TouchableOpacity, Text, Image, Dimensions, Platform } from "react-native";
import {Context as CartContext} from './../context/CartContext'
import {Context as TrackContext} from './../context/TrackContext'


function Menu({navigation}) {
    const {state, getCartItems} = useContext(CartContext)
    useEffect(() => { getCartItems() },[])
  return (
    <View style={styles.container}>
    <View style={{...styles.root, }}>
      <View style={styles.rect}>

        {/* <View style={styles.homeTabRow}> */}
          <TouchableOpacity onPress={() => navigation.navigate("Homepage")} style={styles.homeTab}>
          <Image source={require("./../../assets/images/home.png")} style={styles.homeImage}/>
            <Text style={styles.home}>Home</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => navigation.navigate("Wishlist")} style={styles.wishtlistTab}>
            <View style={styles.wiishlistStack}>
              <Text style={styles.wishlist}>Wishlist</Text>
              <Image
                source={require("./../../assets/images/star.png")}
                resizeMode="cover"
                style={styles.star}
              ></Image>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => navigation.navigate("Cart")} style={styles.cartTab}>
          {
              
              state.cartItems ? <View style={styles.cartCounter}><Text style={styles.cartqtynum}>{state.cartItems.reduce((total, product)=>total + product.quantity, 0)}</Text></View> : null
          }
          
            <View style={styles.path2Stack}>
                
            <Image source={require("./../../assets/images/cart-2-2.png")} style={styles.cart2Image}/>
            <View style={styles.rect2}></View>
            </View>
            <Text style={styles.cart}>Cart</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => navigation.navigate("NearbyMechanic")} style={styles.nearbyTab}>
            <View style={styles.pin}>
                <Image source={require("./../../assets/images/pin.png")} style={styles.pinImage}/>
            </View>
            <Text style={styles.nearby}>Nearby</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => navigation.navigate("MoreScreen")} style={styles.moreTab}>
            <Image
              source={require("./../../assets/images/menu.png")}
              resizeMode="cover"
              style={styles.menu1}
            ></Image>
            <Text style={styles.more}>More</Text>
          </TouchableOpacity>
        </View>
      {/* </View> */}
    </View>
    </View>
  );
}

const styles = StyleSheet.create({
    cartqtynum: {
		color: "rgb(251, 252, 254)",
		fontFamily: "Avenir-Heavy",
		fontSize: 9,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "center",
		letterSpacing: 0.13,
        // backgroundColor: "transparent",
        marginTop: 1,
		// marginLeft: 4,
		// marginRight: 5,
	},
cartCounter: {
    position: "absolute",
    width: 15,
  height: 15,
  backgroundColor: "#333333",
//   marginLeft : 10,
  top:0,
  right:10,
  bottom: 10,
  borderRadius: 8,
},
container: {
  elevation : 1,
  borderTopLeftRadius: 25,
  borderTopRightRadius: 25,
  width: Dimensions.get("window").width,
  height: Dimensions.get("window").height == 812|| Dimensions.get("window").height == 896 ? 83 : 63,
  backgroundColor: "#fff",
  //TODO:Android doesnt have shadow even after code below
  ...Platform.select({
    ios: {
      shadowColor: "#2d2d2d",
      shadowOffset: {
        width: 0,
        height: -3
        },
      shadowRadius: 20,
      shadowOpacity: 1  
    },
    android: {
      elevation : 1,
    },
  }),
},
menu: {
    backgroundColor: "#2d2d2d",
    opacity: 1,
    flex: 1
},  
  root: {
    flex: 1,
    // backgroundColor: "rgba(230, 230, 230,1)",
    
  },
  rect: {
    // width: 186,
    opacity: 1,
    flexDirection: "row",
    // alignItems: "center",
    marginTop: 10,
    flex: 1,
    justifyContent: "space-evenly",
    marginLeft: 30,
    marginRight: 30,
    
    // marginLeft: 39
  },
  homeTab: {
    width: 53,
    height: 39,
    // marginLeft: -10,
    marginTop: 4
  },
  path3: {
    width: 23,
    height: 23,
    backgroundColor: "transparent",
    borderColor: "transparent",
    marginLeft: 13
  },
  home: {
    backgroundColor: "transparent",
    color: "rgba(51,51,51,1)",
    opacity: 1,
    fontSize: 10,
    fontFamily: "Avenir-Heavy",
    letterSpacing: 0.12,
    // textAlign: "center",
    marginTop: 2
  },
  wishtlistTab: {
    width: 48,
    height: 43,
    marginLeft: 18
  },
  wishlist: {
    top: 29,
    left: 0,
    width: 48,
    height: 14,
    backgroundColor: "transparent",
    color: "rgba(51,51,51,1)",
    position: "absolute",
    opacity: 1,
    fontSize: 10,
    fontFamily: "Avenir-Heavy",
    letterSpacing: 0.12,
    textAlign: "center"
  },
  star: {
    top: 0,
    left: 8,
    width: 30,
    height: 30,
    backgroundColor: "transparent",
    position: "absolute",
    opacity: 1
  },
  wishlistStack: {
    width: 48,
    height: 43
  },
  cartTab: {
    width: 48,
    height: 37,
    marginLeft: 25,
    marginTop: 0
  },
  path2: {
    top: 1,
    left: 0,
    width: 24,
    height: 20,
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  rect2: {
    top: 0,
    left: 0,
    width: 24,
    height: 21,
    position: "absolute",
    opacity: 1
  },
  path2Stack: {
    width: 24,
    height: 21,
    marginTop: 6,
    marginLeft: 8
  },
  cart: {
    width: 48,
    height: 14,
    backgroundColor: "transparent",
    color: "rgba(51,51,51,1)",
    opacity: 1,
    fontSize: 10,
    fontFamily: "Avenir-Heavy",
    letterSpacing: 0.12,
    textAlign: "center",
    marginTop: 3
  },
  nearbyTab: {
    width: 53,
    height: 38,
    marginLeft: 20,
    marginTop: 5
  },
  pin: {
    width: 19,
    height: 22,
    opacity: 1,
    marginLeft: 16
  },
  path4: {
    width: 18,
    height: 21,
    backgroundColor: "transparent",
    borderColor: "transparent",
    marginLeft: 1
  },
  nearby: {
    width: 53,
    height: 14,
    backgroundColor: "transparent",
    color: "rgba(51,51,51,1)",
    opacity: 1,
    fontSize: 10,
    fontFamily: "Avenir-Heavy",
    letterSpacing: 0.12,
    textAlign: "center",
    marginTop: 2
  },
  moreTab: {
    width: 48,
    height: 35,
    marginLeft: 25,
    marginTop: 8
  },
  menu1: {
    width: 23,
    height: 18,
    backgroundColor: "transparent",
    opacity: 1,
    marginLeft: 12
  },
  more: {
    width: 48,
    height: 14,
    backgroundColor: "transparent",
    color: "rgba(51,51,51,1)",
    opacity: 1,
    fontSize: 10,
    fontFamily: "Avenir-Heavy",
    letterSpacing: 0.12,
    textAlign: "center",
    marginTop: 3
  },
  homeTabRow: {
    backgroundColor: "pink",
    height: 43,
    flexDirection: "row",
    flex: 1,
    // marginRight: -182,
    // marginLeft: 30,
    marginTop: 12
  }
});

export default Menu;
