import React, { useReducer, useState } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';

const COLOR_INCREMENT = 1;


const reducer = (state, action) => {
  switch (action.type) {
    case 'increment':
      return { ...state, count: state.count + action.payload };
    case 'decrement':
      return { ...state, count: state.count - action.payload };
    default:
      return state;
  }
};

const CounterScreen = () => {
// const [ counter, setCounter] = useState(0);

const [state, dispatch] = useReducer(reducer, { count: 0 });

    return (
      <View>
          <Button title="Increase" onPress={() => {
              // counter++;
              // console.log(counter);
              // setCounter(counter + 1);
              dispatch({ type: 'increment', payload: COLOR_INCREMENT })
          }}
/>
          <Button title="Decrease" onPress={() => {
              dispatch({ type: 'decrement', payload: COLOR_INCREMENT })
            }}/>
        <Text style={styles.text}>Current Count: {counter}</Text>
      </View>
    );
};

const styles = StyleSheet.create({
    text: {
      fontSize: 20
    }
  });
  
  export default CounterScreen;