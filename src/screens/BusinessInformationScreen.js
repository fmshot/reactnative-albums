import React from 'react';
import { StyleSheet, View, Image, Text, ScrollView, Dimensions } from 'react-native';
import LogoTitle from "../components/Logotitle"
import MotorFixrButton from "../components/MotorFixrButton"
import FixrInput from "../components/FixrInput"
import {SafeAreaView} from "react-navigation"

const BusinessInformationScreen = ({ navigation }) => {
  return (
    <ScrollView contentContainerStyle={styles.container}>
        <FixrInput label='Business Information' placeholder="Business Name" />
        <FixrInput label={null} placeholder="Address Line 1" />
        <FixrInput label={null} placeholder="Address Line 1" />
        <FixrInput label={null} placeholder="Address Line 2" />
        <FixrInput label={null} placeholder="City" />
        <FixrInput label={null} placeholder="State" />
        <MotorFixrButton navigation={navigation} text="Continue" destination="EnterMobileNumber"/>
    </ScrollView>
);
}

export default BusinessInformationScreen;

BusinessInformationScreen.navigationOptions = {
    headerTitle: <LogoTitle />,
    headerTintColor: 'white',
    headerStyle: {
      backgroundColor: '#131416',
      height : 120,
    }
}

const styles = StyleSheet.create({
container: {
  width: Dimensions.get("window").width,
  height: 800,
//   flex:1,
  justifyContent: "center",
},

});



