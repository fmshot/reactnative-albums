import React from 'react';
import { StyleSheet, View, ScrollView, Text, FlatList, TouchableOpacity } from 'react-native';
import ChosenCarHeader from "../../components/ChosenCarHeader"
import BreadcrumbsNav from "../../components/BreadcrumbsNav"
import FormTextInput from "../../components/FormTextInput"
import MotorFixrButton from "../../components/MotorFixrButton"


const CartPaymentScreen = ({ navigation }) => {

  return (
    <>
    <ScrollView>
      <BreadcrumbsNav withbackArrow={true} navigation={navigation} screenName="Card Details"/>
      <FormTextInput placeholder="1234 5678 9120 8900" label="Card Number"/>
      <FormTextInput placeholder="Bello Oluwa Chukwu" label="Name on Card"/>
      <View style={{flexDirection: "row", }}>
          <FormTextInput inputstyle={{}} label="Expiry Date" placeholder="1120" />
          <FormTextInput inputstyle={{width: 197.8,}} label="CVV" placeholder="123" />
      </View>
      <MotorFixrButton navigation={navigation} text="Pay Now" destination="OrderPlaced" />
    </ScrollView>
      
    </>
  );
};

CartPaymentScreen.navigationOptions = () => {
  return{
    header: <ChosenCarHeader/>,
    tabBarVisible: false,
  }
   
}


const styles = StyleSheet.create({});

export default CartPaymentScreen;
