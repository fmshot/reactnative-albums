//
//  CreateUsername
//  Project
//
//  Created by girlProg.
//  Copyright © 2019 YediTech. All rights reserved.
//

import { TextInput, View, TouchableOpacity, Image, StyleSheet, Text, StatusBar } from "react-native"
import React from "react"
import LogoTitle from "../components/Logotitle"


const CreateUsernameScreen = ({navigation}) => {


    return <View style={styles.createUsernameView}>
		<StatusBar backgroundColor="blue" barStyle="light-content" />
            <Text style={styles.createAUsernameText}>Create a username</Text>
            <View style={styles.eMailView}>
                <TextInput
                    clearButtonMode="always"
                    autoCorrect={false}
                    placeholder="Username"
                    style={styles.usernameTextInput}/>
            </View>
            <View style={{
                    height: 60,
                    marginLeft: 30,
                    marginRight: 30,
                    marginTop: 20,
                }}>
                <Text style={styles.getStartedText}>Get Started</Text>
                <TouchableOpacity
                    onPress={()=> navigation.navigate('CreatePassword')}
                    style={styles.gobuttonButton}>
                    <Text style={styles.gobuttonButtonText}>Continue</Text>
                </TouchableOpacity>
            </View>
            <TouchableOpacity
                onPress={()=>navigation.navigate('Signin')}
                style={styles.signInButton}>
                <Text
                    style={styles.signInButtonText}>Already have an account? Sign in </Text>
            </TouchableOpacity>
        </View>
}

export default CreateUsernameScreen
CreateUsernameScreen.navigationOptions = {
    headerTitle: <LogoTitle />,
    headerTintColor: 'white',
    headerStyle: {
      backgroundColor: '#131416',
      height : 120,
    }
}

const styles = StyleSheet.create({
	createUsernameView: {
		backgroundColor: "rgb(251, 252, 254)",
		flex: 1,
	},
	headerBgView: {
		backgroundColor: "transparent",
		position: "absolute",
		left: 0,
		right: 0,
		top: 0,
		height: 116,
		justifyContent: "center",
	},
	headerBgImage: {
		resizeMode: "cover",
		backgroundColor: "transparent",
		width: null,
		height: 116,
	},
	logoView: {
		backgroundColor: "transparent",
		position: "absolute",
		left: 110,
		right: 109,
		top: 50,
		height: 36,
		flexDirection: "row",
		alignItems: "flex-start",
	},
	group2Image: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: 27,
		height: 36,
	},
	motorfixrText: {
		backgroundColor: "transparent",
		color: "white",
		// fontFamily: ".AppleSystemUIFont",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 3,
		position: "absolute",
		right: 0,
		top: 0,
	},
	fixingAllYourCarText: {
		color: "white",
		fontFamily: "Avenir-Book",
		fontSize: 9,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		lineHeight: 16,
		backgroundColor: "transparent",
		position: "absolute",
		right: 24,
		top: 17,
	},
	createAUsernameText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 21,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		marginLeft: 99,
		marginRight: 99,
		marginTop: 99,
	},
	eMailView: {
		backgroundColor: "transparent",
		borderRadius: 30,
		borderWidth: 0.5,
		borderColor: "#dbd7ce",
		borderStyle: "solid",
		height: 60,
		marginLeft: 30,
		marginRight: 30,
		marginTop: 25,
	},
	usernameTextInput: {
		color: "rgb(136, 136, 136)",
		fontFamily: "Avenir-Medium",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		backgroundColor: "transparent",
		padding: 0,
		height: 39,
		marginLeft: 18,
		marginRight: 18,
		marginTop: 12,
	},
	getStartedText: {
		color: "white",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "center",
		letterSpacing: 0.32,
		backgroundColor: "transparent",
		position: "absolute",
		left: 111,
		right: 123,
		top: 15,
	},
	gobuttonButton: {
		backgroundColor: "rgb(23, 24, 26)",
		borderRadius: 30,
		shadowColor: "rgba(0, 0, 0, 0.08)",
		shadowRadius: 8,
		shadowOpacity: 1,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		position: "absolute",
		alignSelf: "center",
		width: 315,
		top: 0,
		height: 60,
	},
	gobuttonButtonText: {
		color: "white",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "center",
	},
	gobuttonButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	signInButtonText: {
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Book",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "center",
	},
	signInButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	signInButton: {
		backgroundColor: "transparent",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		height: 19,
		marginLeft: 78,
		marginRight: 79,
		marginTop: 17,
	},
})
