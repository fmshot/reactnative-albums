//
//  Item
//  Project
//
//  Created by girlProg.
//  Copyright © 2018 YediTech. All rights reserved.
//

import { Image, View, StyleSheet, TextInput, Text, TouchableOpacity, ScrollView, Dimensions } from "react-native"
import React, {useContext, useEffect} from "react"
import ChosenCarHeader from "../components/ChosenCarHeader"
import BreadcrumbsNav from "../components/BreadcrumbsNav"
import { SafeAreaView, withNavigationFocus } from 'react-navigation'; 
import { SliderBox } from 'react-native-image-slider-box';
import {Context as CartContext} from './../context/CartContext'
import {Context as ProductContext} from './../context/ProductContext'


const ProductDetail = ({navigation}) => {

	const {state, addToCart, } = useContext(CartContext)
	// const {state2, getProduct, } = useContext(ProductContext) 
	const product = navigation.getParam("product")
	// alert(JSON.stringify(product.condition.name))
	const images = product.productImages.map(imageObj=>imageObj.image)
	
    const iamages = [ 'https://source.unsplash.com/1024x768/?nature',
        'https://source.unsplash.com/1024x768/?water',
        'https://source.unsplash.com/1024x768/?girl',
        'https://source.unsplash.com/1024x768/?tree'
      ]
	
		return <SafeAreaView style={styles.itemView}>
            <BreadcrumbsNav navigation={navigation} screenName="Product Detail"/>
				<ScrollView pointerEvents="box-none"
					style={{
						// position: "absolute",
						// left: 0,
						// right: 0,
						// top: 0,
						// bottom: -253,
					}}>
					
					<View
						pointerEvents="box-none"
						style={{
							height: 230,
							marginLeft: 15,
							marginRight: 25,
							marginTop: 20,
						}}>
						<View style={styles.picView}>
                            <SliderBox images={images} />
						</View>
						<View style={styles.belgiumtagView}>
							<View style={styles.belgiumtagvView}>
									<Image source={require("./../../assets/images/-background-11.png")}
										style={styles.belgiumImage}/>
								<View
									pointerEvents="box-none"
									style={{
										position: "absolute",
										left: 0,
										right: 0,
										top: 0,
										bottom: 0,
										justifyContent: "center",
									}}>
									<Text style={styles.belgiumText}>{product.condition.name}</Text>
								</View>
							</View>
						</View>
					</View>
					{/* <Image source={require("./../../assets/images/dots.png")} style={styles.dotsImage}/> */}
					<Text style={styles.productnameText}>{product.name}</Text>
					<View
						pointerEvents="box-none"
						style={{
							alignSelf: "center",
							width: 281,
							height: 135,
                            marginTop: 8,
                            // marginBottom: 30
						}}>
						<Text style={styles.productpriceText}>₦{product.price}</Text>
						<Text style={styles.newverifiedpartText}>New Verfied Part{"\n"}</Text>
						<Text style={styles.newverifiedpartText}>{"\n"}{product.shortDescription}</Text>
					</View>
					<View
						pointerEvents="box-none"
						style={{
							height: 50,
							marginLeft: 28,
                            marginRight: 30,
                            marginBottom: 30,
							flexDirection: "row",
						}}>
						<View style={styles.quantityView}>
							<View
								pointerEvents="box-none"
								style={{
									width: 42,
									height: 33,
									marginLeft: 10,
									marginTop: 5,
									alignItems: "flex-start",
								}}>
								<Text style={styles.quantityText}>QUANTITY</Text>
								<TextInput
                                    keyboardType="numeric"
									autoCorrect={false}
									placeholder="1"
									style={styles.quantityTextInput}/>
							</View> 
                            <TouchableOpacity>
							    <Image source={require("./../../assets/images/right-arrow.png")} style={styles.rightArrowImage}/>
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity onPress={()=>{addToCart(product)}} style={styles.addtocartView}>
                            <Text style={styles.addToCartText}>Add to Cart</Text>
                        </TouchableOpacity>
					</View> 

					<TouchableOpacity
						onPress={()=>{navigation.navigate("MoreProductInfo", {screenName: "About This Product", })}}
						style={styles.productdetailsbuttonButton}>
						<Text style={styles.productdetailsbuttonButtonText}>PRODUCT DETAILS  </Text>
						<Image source={require("./../../assets/images/icon-group-3.png")} style={styles.productdetailsbuttonButtonImage}/>
					</TouchableOpacity>
					<TouchableOpacity 
						onPress={()=>{navigation.navigate("MoreProductInfo", {screenName: "Delivery and Returns"})}}
						style={styles.delverynreturnsButton}>
						<Text style={styles.delverynreturnsButtonText}>DELIVERY & RETURNS  </Text>
						<Image source={require("./../../assets/images/icon-group-3.png")} style={styles.delverynreturnsButtonImage}/>
					</TouchableOpacity>
					<TouchableOpacity
						onPress={()=>{navigation.navigate("ProductReviews", {screenName: "Reviews", sellerid: product.seller.id})}}
						style={styles.reviewsButton}>
						<Text style={styles.reviewsButtonText}>REVIEWS  </Text>
						<Image source={require("./../../assets/images/icon-group-3.png")}
							style={styles.reviewsButtonImage}/>
					</TouchableOpacity>
                    
					<View style={styles.relatedproductsView}>
						<Text style={styles.relatedProductsHeadingText}>RELATED PRODUCTS</Text>
						<View style={{ 
								flexDirection: "row",
                                alignItems: "flex-start",
                                justifyContent: "space-evenly"
							}}>
							<TouchableOpacity
								onPress={this.onRelatedproductPressed}
								style={styles.relatedproductButton}>
								<Image source={require("./../../assets/images/rectangle-4.png")} style={styles.relatedproductButtonImage}/>
								<Text style={styles.relatedproductSellerNameText}>Adam Motors  </Text>
                                <Text style={styles.relatedproductnameText}>FM Transmittor</Text>
							</TouchableOpacity>
							<TouchableOpacity
								onPress={this.onRelatedproductPressed}
								style={styles.relatedproductButton}>
								<Image
									source={require("./../../assets/images/rectangle-4.png")}
									style={styles.relatedproductButtonImage}/>
								<Text style={styles.relatedproductSellerNameText}>Adam Motors  </Text>
                                <Text style={styles.relatedproductnameText}>FM Transmittor</Text>
							</TouchableOpacity>
						</View>
					</View>
				</ScrollView>
				
			</SafeAreaView>
	
}

export default ProductDetail

ProductDetail.navigationOptions =  {
	header: <ChosenCarHeader/>,
}


const styles = StyleSheet.create({
	itemView: {
		backgroundColor: "white",
		flex: 1,
	},
	headerBgView: {
		backgroundColor: "transparent",
		position: "absolute",
		left: 0,
		right: 0,
		top: 0,
		height: 116,
		justifyContent: "center",
	},
	headerBgImage: {
		backgroundColor: "transparent",
		resizeMode: "cover",
		width: null,
		height: 116,
	},
	addCarView: {
		backgroundColor: "transparent",
		position: "absolute",
		left: 0,
		right: 0,
		top: 0,
		height: 101,
		justifyContent: "flex-end",
	},
	group2View: {
		backgroundColor: "transparent",
		height: 43,
		marginLeft: 30,
		marginRight: 176,
		flexDirection: "row",
	},
	ovalView: {
		backgroundColor: "white",
		borderRadius: 21.5,
		width: 43,
		height: 43,
	},
	bitmapImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		position: "absolute",
		left: 7,
		width: 30,
		top: 9,
		height: 30,
	},
	toyotaCamryText: {
		backgroundColor: "transparent",
		color: "white",
		fontFamily: "Avenir-Book",
		fontSize: 18,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.41,
		position: "absolute",
		right: 0,
		top: 0,
	},
	hp20lText: {
		color: "white",
		fontFamily: "Avenir-Book",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.28,
		backgroundColor: "transparent",
		position: "absolute",
		right: 40,
		bottom: 0,
	},
	backIconImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: null,
		height: 16,
		marginLeft: 25,
		marginRight: 328,
		marginTop: 22,
	},
	picView: {
		backgroundColor: "transparent",
		position: "absolute",
		left: 0,
		right: 0,
		top: 0,
		height: 230,
	},
	rectangle13Image: {
		resizeMode: "cover",
		backgroundColor: "transparent",
		width: null,
		height: 230,
	},
	picImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: null,
		height: 100,
		marginLeft: 123,
		marginRight: 122,
	},
	belgiumtagView: {
		backgroundColor: "transparent",
		shadowColor: "rgba(0, 0, 0, 0.1)",
		shadowRadius: 4,
		shadowOpacity: 1,
		position: "absolute",
		right: 17,
		width: 66,
		top: 14,
		height: 22,
		justifyContent: "center",
		elevation: 2,
	},
	belgiumtagvView: {
		backgroundColor: "transparent",
		height: 22,
	},
	belgiumImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: null,
		height: 22,
	},
	belgiumText: {
		color: "rgb(6, 6, 6)",
		// fontFamily: ".AppleSystemUIFont",
		fontSize: 9,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		lineHeight: 12,
		letterSpacing: 0.15,
		backgroundColor: "transparent",
		marginLeft: 11,
		marginRight: 12,
	},
	// dotsImage: {
	// 	backgroundColor: "transparent",
	// 	resizeMode: "center",
	// 	alignSelf: "flex-start",
	// 	width: 56,
	// 	height: 8,
	// 	marginLeft: 30,
	// 	marginTop: 18,
	// },
	productnameText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "center",
		letterSpacing: 0.37,
		marginLeft: 19,
		marginRight: 18,
		marginTop: 22,
	},
	productpriceText: {
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Black",
		fontSize: 24,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		letterSpacing: 0.34,
		backgroundColor: "transparent",
		position: "absolute",
		alignSelf: "center",
        top: 0,
        
	},
	newverifiedpartText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "center",
		letterSpacing: 0.28,
		backgroundColor: "transparent",
		position: "absolute",
		alignSelf: "center",
        top: 32,
        marginBottom: 100,
	},
	quantityView: {
		backgroundColor: "white",
		borderRadius: 5,
		shadowColor: "rgba(0, 0, 0, 0.08)",
		shadowRadius: 8,
		shadowOpacity: 1,
		width: 80,
		height: 43,
		flexDirection: "row",
		alignItems: "flex-start",
		elevation: 1,
	},
	quantityText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Roman",
		fontSize: 8,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.18,
		backgroundColor: "transparent",
	},
	quantityTextInput: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		backgroundColor: "transparent",
		padding: 0,
		width: 30,
		height: 22,
		marginLeft: 2,
	},
	rightArrowImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: 22,
		height: 22,
		marginRight: 6,
		marginTop: 13,
	},
	addtocartView: {
		backgroundColor: "rgb(19, 20, 22)",
		borderRadius: 25,
		flex: 1,
		height: 50,
		marginLeft: 45,
		justifyContent: "center",
		alignItems: "center",
	},
	addToCartText: {
		color: "white",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		letterSpacing: 0.94,
		backgroundColor: "transparent",
	},
	productdetailsbuttonButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	productdetailsbuttonButton: {
		backgroundColor: "transparent",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		alignSelf: "flex-start",
		width: 130,
		height: 16,
		marginLeft: 28,
		marginBottom: 11,
	},
	productdetailsbuttonButtonText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "center",
	},
	delverynreturnsButton: {
		backgroundColor: "transparent",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		alignSelf: "flex-start",
		width: 146,
		height: 16,
		marginLeft: 27,
		marginBottom: 11,
	},
	delverynreturnsButtonText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "center",
	},
	delverynreturnsButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	reviewsButtonText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "center",
	},
	reviewsButton: {
		backgroundColor: "transparent",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		alignSelf: "flex-start",
		width: 71,
		height: 16,
		marginLeft: 28,
		marginBottom: 53,
	},
	reviewsButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	relatedproductsView: {
		backgroundColor: "transparent",
		height: 224,
		marginLeft: 15,
		marginRight: 14,
		justifyContent: "flex-end",
	},
	relatedProductsHeadingText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "center",
		letterSpacing: 0.28,
		alignSelf: "flex-start",
		marginLeft: 8,
		marginBottom: 18,
	},
	relatedproductView: {
		backgroundColor: "transparent",
		flexDirection: "column",
		alignItems: "flex-start",
		justifyContent: "flex-start",
		padding: 0,
		width: Dimensions.get('window').width/3,
	},
	relatedproductnameText: {
        width: 165,
        height: 18,
        fontFamily: "Avenir",
        fontSize: 14,
        fontWeight: "900",
        fontStyle: "normal",
        lineHeight: 18,
        letterSpacing: 0.2,
        color: "#060606"
	},
	relatedproductButton: {
		backgroundColor: "transparent",
		flexDirection: "column",
		alignItems: "flex-start",
		justifyContent: "space-evenly",
		// padding: 0,
		// width: Dimensions.get('window').width/3,
        height: 190,
        width: 167,
	},
	relatedproductSellerNameText: {
		width: 165,
        height: 16,
        fontFamily: "Avenir",
        fontSize: 12,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: 16,
        letterSpacing: -0.2,
        color: "#ababab"
	},
	relatedproductButtonImage: {
		resizeMode: "contain",
        marginBottom: 28,
        borderRadius: 12,
	},
	
})
