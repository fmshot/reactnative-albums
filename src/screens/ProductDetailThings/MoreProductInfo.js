import { StyleSheet, View, Text } from "react-native"
import React from "react"
import ChosenCarHeader from "../../components/ChosenCarHeader"
import BreadcrumbsNav from "../../components/BreadcrumbsNav"


const MoreProductInfoScreen = ({navigation}) => {
    return <>
        <BreadcrumbsNav navigation={navigation} screenName={navigation.getParam("ScreenName")}/>
        <View style={styles.productDetailsView}>
			<Text>{navigation.getParam("screenName")}</Text>
				<Text style={styles.descriptionText}>Many studies 
                have been done to research the effects of motivation and mental health. 
                As the implications of helping those with negative self-esteem, 
                depression and anxiety are immense.{"\n"}{"\n"}This is certainly an area 
                of research that deserves a great deal of attention. 
                Psychology Online reports on a study investigating the differences.
                 {"\n"}{"\n"}The report states that “Although our society is largely 
                 extrinsically-motivated by external rewards such as money,
                  fame and power.{"\n"}{"\n"}Research has indicated those who are i
                  ntrinsically-motivated by inner desires for creativity, 
                  fulfillment and inner satisfaction are psychologically healthier 
                  and happier.</Text>
			</View> 
            </>
}

export default MoreProductInfoScreen

MoreProductInfoScreen.navigationOptions =  {
	header: <ChosenCarHeader/>,
}

const styles = StyleSheet.create({
	productDetailsView: {
		backgroundColor: "white",
		flex: 1,
	},
	descriptionText: {
		backgroundColor: "transparent",
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Book",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.2,
		marginLeft: 30,
		marginRight: 30,
		// marginTop: 191,
    }
})