//
//  PickAService
//  
//
//  Created by girlProg.
//  Copyright © 2018 YediTech. All rights reserved.
//

import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView, Dimensions } from "react-native"
import React from "react"


const PickAServiceScreen = ({navigation}) => {

		return <ScrollView><View style={styles.pickAServiceView}>

<View style={{ height: Dimensions.get('window').height/2,backgroundColor: 'red' }}>
					<Image
						source={require("./../../assets/images/photo-1487101588220-01e039029925-2.png")}
						style={styles.photo148710158822001e039029925Image}/>
					<View style={styles.rectangleView}/>
						<TouchableOpacity
							onPress={()=>{navigation.navigate("Homepage")}}
							style={styles.skipButton}>
							<Text style={styles.skipButtonText}>Skip</Text>
						</TouchableOpacity>
						<View style={styles.logoView}>
								<Image
									source={require("./../../assets/images/logo.png")}
									style={styles.logoImage}/>
								{/* <View style={{ flex: 1, }}/> */}
						</View>
				</View>

						<View style={styles.group3View}>
                            <TouchableOpacity style={styles.mask}>
									<Image
										source={require("./../../assets/images/bitmap-7.png")}
										style={styles.bitmapImage}/>
								
								<Text style={styles.findAMechanicText}>Find a Mechanic</Text>
                            </TouchableOpacity >
                            <TouchableOpacity style={styles.mask}>
								<Image
									source={require("./../../assets/images/bitmap-3.png")}
									style={styles.bitmapTwoImage}/>
								<Text style={styles.shopSparePartsText}>Shop Spare Parts</Text>
                            </TouchableOpacity>
						</View>

				<View style={{ flex: 1, }}/>

                <TouchableOpacity onPress={()=>{navigation.navigate("CreateUsername")}}>
                    <View style={styles.buttonView}>
                        <Text style={styles.signInTwoText}>Sign up</Text>
                    </View>
                </TouchableOpacity>
                <View style={styles.signuptext2}>
                    <Text style={styles.alreadyHaveAnAccoText}>Already have an account? </Text>
                    <TouchableOpacity onPress={()=>{navigation.navigate("Signin")}}>
                        <Text style={styles.signInText}>Sign in</Text>
                    </TouchableOpacity>
                </View>
			</View>
			</ScrollView>
	}



PickAServiceScreen.navigationOptions = {
    header: null
}

const styles = StyleSheet.create({
	pickAServiceView: {
		// backgroundColor: "white",
		flex: 1,
	},
	skipButtonText: {
		color: "white",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "bold",
		marginTop: 50,
		marginRight: 20,
	},
	skipButton: {
		backgroundColor: "transparent",
		padding: 0,
		alignSelf: "flex-end",
	},
	logoView: {
		alignSelf: "center",
		backgroundColor: "transparent",
		width: 200,
		height: 206,
		marginTop: Dimensions.get('window').width/5,
	},
	logoImage: {
		resizeMode: "contain",
		backgroundColor: "transparent",
		width: null,
		height: Dimensions.get('window').width/3,
	},
	photo148710158822001e039029925Image: {
		backgroundColor: "transparent",
		position: "absolute",
		left: 0,
		right: 0,
		top: 0,
		width: Dimensions.get('window').width,
		alignSelf: "stretch",
		height: Dimensions.get('window').height/2,
		marginRight: 75,
	},
	rectangleView: {
		backgroundColor: "rgb(19, 20, 22)",
		opacity: 0.9,
		position: "absolute",
		width: Dimensions.get('window').width,
		height: Dimensions.get('window').height/2,
	},
	opacityBackground: {
		backgroundColor: "rgb(19, 20, 22)",
		opacity: 0.9,
		// position: "absolute",
		// left: 54,
		// right: 59,
		// top: 167,
        height: 450,
        width : "100%"
	},
	skipText: {
		backgroundColor: "transparent",
		color: "white",
		fontFamily: "Avenir-Heavy",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		alignSelf: "flex-end",
		marginRight: 25,
	},
	group2Image: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: null,
		// height: 52,
		marginLeft: 74,
		marginRight: 67,
	},
	
	group3View: {
		backgroundColor: "transparent",
        // alignSelf: "stretch",
        justifyContent : "space-evenly",
		// height: 164,
		marginTop: -45,
        flexDirection: "row",
        width : "100%"
		// alignItems: "flex-end",
	},
	bitmapImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: 169,
		height: 164,
	},
	findAMechanicText: {
		zIndex : 1,
		backgroundColor: "transparent",
		color: "rgb(34, 34, 34)",
		fontFamily: "Avenir-Heavy",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		position: "absolute",
		left: 24,
		bottom: 7,
	},
	bitmapTwoImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: 169,
		height: 164,
	},
	shopSparePartsText: {
		backgroundColor: "transparent",
		color: "rgb(34, 34, 34)",
		fontFamily: "Avenir-Heavy",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		position: "absolute",
		right: 20,
		bottom: 7,
	},
	buttonView: {
		backgroundColor: "rgb(19, 20, 22)",
		borderRadius: 30,
		shadowColor: "rgba(0, 0, 0, 0.08)",
		shadowRadius: 8,
		shadowOpacity: 1,
		height: 60,
		marginLeft: 24,
		marginRight: 24,
		marginBottom: 13,
		justifyContent: "center",
		alignItems: "center",
	},
	signInTwoText: {
		color: "white",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "center",
		letterSpacing: 0.32,
		backgroundColor: "transparent",
	},
	alreadyHaveAnAccoText: {
		color: "rgb(136, 136, 136)",
		fontFamily: "Avenir-Medium",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.4,
		backgroundColor: "transparent",
		// marginLeft: 61,
		// marginRight: 10,
	},
	signInText: {
		color: "black",
		fontFamily: "Avenir-Heavy",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "bold",
		// textAlign: "left",
		backgroundColor: "transparent",
		alignSelf: "center",
		marginBottom: 43,
    },
    signuptext2:{
        flexDirection: "row",
        justifyContent :  "center"
	},
	mask : {
		// width: Dimensions.get('window').width/3,
		// height: Dimensions.get('window').height/7,
		width: 169,
		height: 164,
		borderRadius: 10,
		backgroundColor: "#ffffff",
		shadowColor: "rgba(0, 0, 0, 0.16)",
		shadowOffset: {
		  width: 0,
		  height: 0
		},
		shadowRadius: 20,
		shadowOpacity: 1,
		marginBottom: 20,
	  }
})


export default PickAServiceScreen;