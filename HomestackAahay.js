import React, { useState } from "react";
import {AppRegistry, View } from 'react-native';

import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
// import { createDrawerNavigator } from "react-navigation-drawer";
// import { AppLoading } from "expo";
// import * as Font from "expo-font";
// import Untitled from "./srcAyahay/screens/Untitled";
import SplashScreen from "./srcAyahay/screens/SplashScreen";
import StudentListScreen from "./srcAyahay/screens/StudentListScreen";
import LogoScreen from "./srcAyahay/screens/LogoScreen";
import StudentReviewScreen from "./srcAyahay/screens/StudentReviewScreen";
import LogClassDataScreen from "./srcAyahay/screens/LogClassDataScreen";
import MenuScreen from "./srcAyahay/screens/MenuScreen";

// const DrawerNavigation = createDrawerNavigator({
//   Untitled: Untitled,
//   SplashScreen: SplashScreen,
//   StudentListScreen: StudentListScreen,
//   LogoScreen: LogoScreen,
//   StudentReviewScreen: StudentReviewScreen,
//   LogClassDataScreen: LogClassDataScreen,
//   MenuScreen: MenuScreen
// });

const navigator = createStackNavigator(
  {
    // DrawerNavigation: {
    //   screen: DrawerNavigation
    // },
    // Untitled: Untitled,
    SplashScreen: SplashScreen,
    StudentListScreen: StudentListScreen,
    LogoScreen: LogoScreen,
    StudentReviewScreen: StudentReviewScreen,
    LogClassDataScreen: LogClassDataScreen,
    MenuScreen: MenuScreen
  },
//   {
//     headerMode: "none"
//   }
  {
    initialRouteName: "SplashScreen",
    defaultNavigationOptions: {
      title: "App"
    }
  }
);

export default createAppContainer(navigator);
// const AppContainer = createAppContainer(StackNavigation);

// function App() {
//   const [isLoadingComplete, setLoadingComplete] = useState(false);
//   if (!isLoadingComplete) {
//     return (
//       <AppLoading
//         startAsync={loadResourcesAsync}
//         onError={handleLoadingError}
//         onFinish={() => handleFinishLoading(setLoadingComplete)}
//       />
//     );
//   } else {
//     return isLoadingComplete ? <AppContainer /> : <AppLoading />;
//   }
// }
// async function loadResourcesAsync() {
//   await Promise.all([
//     Font.loadAsync({
//       "roboto-700": require("./srcAyahay/assets/fonts/roboto-700.ttf"),
//       "roboto-regular": require("./srcAyahay/assets/fonts/roboto-regular.ttf")
//     })
//   ]);
// }
// function handleLoadingError(error) {
//   console.warn(error);
// }

// function handleFinishLoading(setLoadingComplete) {
//   setLoadingComplete(true);
// }


// Create a component 
// const App = () =>  (
//   <View style={{ flex: 1 }}>
//         {/* <Header headerText={'My Albums'} />
//         <AlbumlList /> */}
//         {/* <Navigator /> */}
//         <AppContainer />
//   </View>
//     );
// export default AppContainer;

// export default App;
